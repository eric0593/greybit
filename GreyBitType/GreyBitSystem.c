#include "GreyBitType.h"
#include "GreyBitSystem.h"

///////////////////////////////////////////////////////////////////////////////
// Memory Management
///////////////////////////////////////////////////////////////////////////////
extern void *GreyBit_Malloc_Sys(GB_INT32 size);
extern void *GreyBit_Realloc_Sys(void *p, GB_INT32 newsize);
extern void  GreyBit_Free_Sys(void *p);

//-----------------------------------------------------------------------------
GB_Memory GreyBit_Memory_New(void)
{
    GB_Memory mem = (GB_Memory)GreyBit_Malloc_Sys(sizeof(GB_MemoryRec));
    if(mem)
    {
        mem->malloc  = GreyBit_Malloc_Sys;
        mem->realloc = GreyBit_Realloc_Sys;
        mem->free    = GreyBit_Free_Sys;
    }
    return mem;
}

//-----------------------------------------------------------------------------
void *GreyBit_Malloc(GB_Memory mem, GB_INT32 size)
{
    return mem->malloc(size);
}

//-----------------------------------------------------------------------------
void *GreyBit_Realloc(GB_Memory mem, void *p, GB_INT32 newsize)
{
    return mem->realloc(p,newsize);
}

//-----------------------------------------------------------------------------
void  GreyBit_Free(GB_Memory mem, void *p)
{
    mem->free(p);
}

//-----------------------------------------------------------------------------
void  GreyBit_Memory_Done(GB_Memory mem)
{
    GreyBit_Free_Sys(mem);
}

///////////////////////////////////////////////////////////////////////////////
// Memory Stream
///////////////////////////////////////////////////////////////////////////////
typedef struct _GB_MemStreamRec{
    const GB_BYTE *pData;
    GB_INT32       size;
    GB_INT32       pos;
}GB_MemStreamRec,*GB_MemStream;

GB_IOHandler GreyBit_Open_Mem(const void *p, GB_INT32 size)
{
    GB_MemStream memstream = (GB_MemStream)GreyBit_Malloc_Sys(sizeof(GB_MemStreamRec));
    if(memstream)
    {
        memstream->pData = (const GB_BYTE *)p;
        memstream->size  = size;
        memstream->pos   = 0;
    }
    return memstream;
}

GB_INT32 GreyBit_Read_Mem(GB_IOHandler f, GB_BYTE *p, GB_INT32 size)
{
    GB_MemStream memstream = (GB_MemStream)f;
    GB_INT32 i,j; 

    if(memstream->pos+size > memstream->size)
    {
        size = memstream->size - memstream->pos;
    }
    for(i=0,j=memstream->pos;i<size;i++,j++)
    {
        p[i] = memstream->pData[j];
    }
    memstream->pos = j;
    return size;
}

GB_INT32 GreyBit_Seek_Mem(GB_IOHandler f, GB_INT32 pos)
{
    GB_MemStream memstream = (GB_MemStream)f;
    if(pos < memstream->size)
    {
        memstream->pos = pos;
    }
    else
    {
        return 0;
    }
    return memstream->pos;
}

void GreyBit_Close_Mem(GB_IOHandler f)
{
    GB_MemStream memstream = (GB_MemStream)f;
    GreyBit_Free_Sys(memstream);
}

///////////////////////////////////////////////////////////////////////////////
// Stream IO
///////////////////////////////////////////////////////////////////////////////
extern GB_IOHandler GreyBit_Open_Sys(const GB_CHAR *p, GB_BOOL bcreate);
extern GB_INT32 GreyBit_Read_Sys(GB_IOHandler f, GB_BYTE *p, GB_INT32 size);
extern GB_INT32 GreyBit_Write_Sys(GB_IOHandler f, GB_BYTE *p, GB_INT32 size);
extern GB_INT32 GreyBit_Seek_Sys(GB_IOHandler f, GB_INT32 pos);
extern GB_INT32 GreyBit_GetSize_Sys(GB_IOHandler f);
extern void     GreyBit_Close_Sys(GB_IOHandler f);
GB_Stream GreyBit_Stream_New(const GB_CHAR* filepathname, GB_BOOL bcreate)
{
    GB_IOHandler f = GreyBit_Open_Sys(filepathname, bcreate);
    if(f != 0)
    {
        GB_Stream stream = (GB_Stream)GreyBit_Malloc_Sys(sizeof(GB_StreamRec));
        if(stream)
        {
            stream->parent  = 0;
            stream->read    = GreyBit_Read_Sys;
            stream->write   = GreyBit_Write_Sys;
            stream->seek    = GreyBit_Seek_Sys;
            stream->close   = GreyBit_Close_Sys;
            stream->handler = f;
            stream->size    = GreyBit_GetSize_Sys(stream->handler);
            stream->offset  = 0;
            stream->refcnt  = 1;
#ifdef ENABLE_ENCODER
            stream->pfilename = (char *)GreyBit_Malloc_Sys(GreyBit_Strlen_Sys(filepathname)+1);
            GreyBit_Strcpy_Sys(stream->pfilename, filepathname);
#endif
        }
        return stream;
    }
    return 0;
}

//-----------------------------------------------------------------------------
GB_Stream GreyBit_Stream_New_Memory(const void *pBuf, GB_INT32 nBufSize)
{
    GB_IOHandler f = GreyBit_Open_Mem(pBuf, nBufSize);
    if(f != 0)
    {
        GB_Stream stream = (GB_Stream)GreyBit_Malloc_Sys(sizeof(GB_StreamRec));
        if(stream)
        {
            stream->parent  = 0;
            stream->read    = GreyBit_Read_Mem;
            stream->write   = 0;
            stream->seek    = GreyBit_Seek_Mem;
            stream->close   = GreyBit_Close_Mem;
            stream->handler = f;
            stream->size    = nBufSize;
            stream->offset  = 0;
            stream->refcnt  = 1;
#ifdef ENABLE_ENCODER
            stream->pfilename = 0;
#endif
        }
        return stream;
    }
    return 0;
}

GB_Stream GreyBit_Stream_New_Child(GB_Stream parent)
{
    if(parent)
    {
        GB_Stream stream = (GB_Stream)GreyBit_Malloc_Sys(sizeof(GB_StreamRec));
        if(stream)
        {
            parent->refcnt++;
            stream->parent  = parent;
            stream->read    = GreyBit_Read_Sys;
            stream->write   = GreyBit_Write_Sys;
            stream->seek    = GreyBit_Seek_Sys;
            stream->close   = GreyBit_Close_Sys;
            stream->handler = parent->handler;
            stream->size    = parent->size;
            stream->offset  = parent->offset;
            stream->refcnt  = 1;
#ifdef ENABLE_ENCODER
            stream->pfilename = (char *)GreyBit_Malloc_Sys(GreyBit_Strlen_Sys(parent->pfilename)+1);
            GreyBit_Strcpy_Sys(stream->pfilename, parent->pfilename);
#endif
        }
        return stream;
    }
    return 0;
}

//-----------------------------------------------------------------------------
GB_INT32 GreyBit_Stream_Read(GB_Stream stream, GB_BYTE *p, GB_INT32 size)
{
    if(stream->read)
    {
        return stream->read(stream->handler, p, size);
    }
    return 0;
}

//-----------------------------------------------------------------------------
GB_INT32 GreyBit_Stream_Write(GB_Stream stream, GB_BYTE *p, GB_INT32 size)
{
    if(stream->write)
    {
        return stream->write(stream->handler, p, size);
    }
    return 0;
}

//-----------------------------------------------------------------------------
GB_INT32 GreyBit_Stream_Seek(GB_Stream stream, GB_INT32 pos)
{
    if(stream->seek)
    {
        return stream->seek(stream->handler, stream->offset+pos);
    }
    return 0;
}

GB_INT32 GreyBit_Stream_Offset(GB_Stream stream, GB_INT32 offset, GB_INT32 size)
{
    if(stream->parent)
    {
        stream->offset = stream->parent->offset+offset;
        if(size)
        {
            stream->size = size;
        }
        else
        {
            stream->size = stream->parent->size - stream->offset;
        }
    }
    else
    {
        stream->offset = offset;
    }
    
    if(stream->seek)
    {
        return stream->seek(stream->handler, stream->offset);
    }
    return 0;
}

//-----------------------------------------------------------------------------
void GreyBit_Stream_Done(GB_Stream stream)
{
    stream->refcnt--;
    if(stream->refcnt > 0)
    {
        return;
    }
    
    if(!stream->parent)
    {
        if(stream->close)
        {
            stream->close(stream->handler);
        }
    }
    else
    {
        GreyBit_Stream_Done(stream->parent);
    }
    
#ifdef ENABLE_ENCODER
    if(stream->pfilename)
    {
        GreyBit_Free_Sys(stream->pfilename);
    }
#endif
    GreyBit_Free_Sys(stream);
}

