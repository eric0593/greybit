#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"

//-----------------------------------------------------------------------------
// Local Functions Declare
//-----------------------------------------------------------------------------
static GB_Decoder  GreyBitType_Loader_Probe(GB_Library library, GB_Loader loader);

//-----------------------------------------------------------------------------
// Loader
//-----------------------------------------------------------------------------
GBHANDLE GreyBitType_Loader_New(GBHANDLE library, const char* filepathname)
{
	GB_Library gbLib = (GB_Library)library;
	GB_Loader loader = (GB_Loader)GreyBit_Malloc(gbLib->gbMem,sizeof(GB_LoaderRec));
	if(loader)
	{
		loader->gbLibrary = gbLib;
		loader->gbMem    = gbLib->gbMem;
		loader->gbStream = GreyBit_Stream_New(filepathname, 0);
		if(loader->gbStream)
		{
			loader->gbDecoder  = GreyBitType_Loader_Probe(gbLib,loader);
		}
		
		if(loader->gbStream == 0 || loader->gbDecoder == 0)
		{
			GreyBitType_Loader_Done(loader);
			return 0;
		}
	}
	return loader;
}

GBHANDLE GreyBitType_Loader_New_Stream(GBHANDLE library, GBHANDLE stream, GB_INT32 size)
{
    GB_Library gbLib = (GB_Library)library;
	GB_Loader loader = (GB_Loader)GreyBit_Malloc(gbLib->gbMem,sizeof(GB_LoaderRec));
	if(loader)
	{
		loader->gbLibrary = gbLib;
		loader->gbMem    = gbLib->gbMem;
		loader->gbStream = GreyBit_Stream_New_Child(stream);
		if(loader->gbStream)
		{
            GreyBit_Stream_Offset(loader->gbStream, 0, size);
			loader->gbDecoder  = GreyBitType_Loader_Probe(gbLib,loader);
		}
		
		if(loader->gbStream == 0 || loader->gbDecoder == 0)
		{
			GreyBitType_Loader_Done(loader);
			return 0;
		}
	}
	return loader;
}

GBHANDLE GreyBitType_Loader_New_Memory(GBHANDLE library, void *pBuf, GB_INT32 nBufSize)
{
	GB_Library gbLib = (GB_Library)library;
	GB_Loader loader = (GB_Loader)GreyBit_Malloc(gbLib->gbMem,sizeof(GB_LoaderRec));
	if(loader)
	{
		loader->gbLibrary = gbLib;
		loader->gbMem    = gbLib->gbMem;
		loader->gbStream = GreyBit_Stream_New_Memory(pBuf, nBufSize);
		if(loader->gbStream)
		{
			loader->gbDecoder  = GreyBitType_Loader_Probe(gbLib,loader);
		}
		
		if(loader->gbStream == 0 || loader->gbDecoder == 0)
		{
			GreyBitType_Loader_Done(loader);
			return 0;
		}
	}
	return loader;
}

GB_INT32 GreyBitType_Loader_GetHeight(GBHANDLE loader)
{
    GB_Loader me = (GB_Loader)loader;
	return GreyBit_Decoder_GetHeight(me->gbDecoder);
}

GB_INT32 GreyBitType_Loader_GetCount(GBHANDLE loader)
{
    GB_Loader me = (GB_Loader)loader;
	return GreyBit_Decoder_GetCount(me->gbDecoder);
}

int GreyBitType_Loader_SetParam(GBHANDLE loader, GB_Param nParam, GB_UINT32 dwParam)
{
	GB_Loader me = (GB_Loader)loader;
	return GreyBit_Decoder_SetParam(me->gbDecoder, nParam, dwParam);
}

GB_BOOL GreyBitType_Loader_IsExist(GBHANDLE loader, GB_UINT32 nCode)
{
    GB_Loader me = (GB_Loader)loader;
	if(GreyBit_Decoder_GetWidth(me->gbDecoder, nCode, 100) == GB_WIDTH_DEFAULT)
    {
        return GB_FALSE;
    }
    else
    {
        return GB_TRUE;
    }
}

void GreyBitType_Loader_Done(GBHANDLE loader)
{
    GB_Loader me = (GB_Loader)loader;
	if(me->gbDecoder)
	{
		GreyBit_Decoder_Done(me->gbDecoder);
	}
	if(me->gbStream)
	{
		GreyBit_Stream_Done(me->gbStream);
	}
	GreyBit_Free(me->gbMem, me);
}
//-----------------------------------------------------------------------------
// Local Functions
//-----------------------------------------------------------------------------
static GB_Decoder GreyBitType_Loader_Probe(GB_Library library, GB_Loader loader)
{
	GB_Format format = library->gbFormatHeader;
	GB_Decoder decoder  = 0;
	while(format)
	{
		if(GreyBit_Format_Probe(format,loader->gbStream))
		{
			decoder = GreyBit_Format_DecoderNew(format, loader, loader->gbStream);
			break;
		}
		format = format->next;
	}
	return decoder;
}
