#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"

#ifdef ENABLE_GREYBITFILE
#ifdef ENABLE_ENCODER
#include "GreyBitFile.h"

/*************************************************************************/
/*                                                                       */
/*                              Struct                                   */
/*                                                                       */
/*************************************************************************/
// Compress Function
static int GreyBitFile_Encoder_Compress(GB_BYTE *pOutData, GB_INT32 *pnInOutLen, GB_BYTE *pInData, GB_INT32 nInDataLen);

static int GreyBitFile_Encoder_Init(GBF_Encoder me);
static int GreyBitFile_Encoder_BuildAll(GBF_Encoder me);
static int GreyBitFile_Encoder_WriteAll(GBF_Encoder me);
static int GreyBitFile_Encoder_InfoInit(GBF_Encoder me, GB_INT16 nHeight,GB_INT16 nBitCount, GB_INT16 bCompress);

static void GreyBitFile_Encoder_ClearCache(GBF_Encoder me);

//-----------------------------------------------------------------------------
// Encoder
//-----------------------------------------------------------------------------
GB_Encoder GreyBitFile_Encoder_New(GB_Creator creator, GB_Stream stream)
{
    GBF_Encoder codec = (GBF_Encoder)GreyBit_Malloc(creator->gbMem,sizeof(GBF_EncoderRec));
    if(codec)
    {
        codec->gbEncoder.getcount       = GreyBitFile_Encoder_GetCount;
        codec->gbEncoder.setparam       = GreyBitFile_Encoder_SetParam;
        codec->gbEncoder.remove         = GreyBitFile_Encoder_Delete;
        codec->gbEncoder.encode         = GreyBitFile_Encoder_Encode;
        codec->gbEncoder.flush          = GreyBitFile_Encoder_Flush;
        codec->gbEncoder.done           = GreyBitFile_Encoder_Done;
        codec->gbLibrary                = creator->gbLibrary;
        codec->gbMem                    = creator->gbMem;
        codec->gbStream                 = stream;
        codec->nCacheItem	            = 0;
        codec->nItemCount               = 0;
        codec->gbBitCount               = 8;
        codec->gbCompress               = GB_FALSE;
        codec->gbHeight                 = 20;
        codec->gbOffDataBits            = sizeof(GREYBITFILEHEADER)+sizeof(GREYBITINFOHEADER);
        GreyBitFile_Encoder_Init(codec);
    }
    return (GB_Encoder)codec;
}

GB_INT32 GreyBitFile_Encoder_GetCount(GB_Encoder codec)
{
    GBF_Encoder me = (GBF_Encoder)codec;
    return me->nItemCount;
}

int	GreyBitFile_Encoder_SetParam(GB_Encoder codec, GB_Param nParam, GB_UINT32 dwParam)
{
    GBF_Encoder me = (GBF_Encoder)codec;

    switch(nParam){
    case GB_PARAM_BITCOUNT:
        me->gbBitCount = (GB_BYTE)dwParam;
        break;
        
    case GB_PARAM_COMPRESS:
        me->gbCompress = (GB_BOOL)dwParam;
        break;
        
    case GB_PARAM_HEIGHT:
        me->gbHeight   = (GB_UINT16)dwParam;
        break;
        
    default:
        return -1;
    }
    
    return GreyBitFile_Encoder_InfoInit(me, me->gbHeight, me->gbBitCount, me->gbCompress);
}

int	GreyBitFile_Encoder_Delete(GB_Encoder codec, GB_UINT32 nCode)
{
    GBF_Encoder me = (GBF_Encoder)codec;

    me->gbOffsetTable[nCode] = 0;
    me->gbWidthTable[nCode] = GB_WIDTH_DEFAULT;
    me->gbHoriOffTable[nCode] = GB_HORIOFF_DEFAULT;
    if(me->gpGreyBits[nCode])
    {
        GreyBit_Free(me->gbMem,me->gpGreyBits[nCode]);
        me->gpGreyBits[nCode] = 0;
    }
    me->pnGreySize[nCode] = 0;
    return 0;
}

int	GreyBitFile_Encoder_Encode(GB_Encoder codec, GB_UINT32 nCode, GB_Data pData)
{
    GBF_Encoder me = (GBF_Encoder)codec;
    GB_Bitmap bitmap;
    GB_INT32 nInDataLen, nOutLen;
    GB_BYTE *pByteData;
    
    if(!pData || pData->format != GB_FORMAT_BITMAP)
    {
        return -1;
    }
    
    bitmap = (GB_Bitmap)pData->data;

    if(bitmap->bitcount != me->gbInfoHeader.gbiBitCount
        || bitmap->height != me->gbInfoHeader.gbiHeight
        || bitmap->width > 3*bitmap->height)
    {
        return -1;
    }
    
    if(me->gbInfoHeader.gbiWidth<bitmap->width)
    {
        me->gbInfoHeader.gbiWidth = bitmap->width;
    }

    nInDataLen = bitmap->height*bitmap->pitch;
    if(me->gbInfoHeader.gbiCompression)
    {
        GreyBitFile_Encoder_Compress(0, &nOutLen, bitmap->buffer, nInDataLen);
        pByteData = (GB_BYTE *)GreyBit_Malloc(me->gbMem, nOutLen);
        GreyBitFile_Encoder_Compress(pByteData, &nOutLen, bitmap->buffer, nInDataLen);
    }
    else
    {
        nOutLen = nInDataLen;
        pByteData = (GB_BYTE *)GreyBit_Malloc(me->gbMem, nOutLen);
        GB_MEMCPY(pByteData, bitmap->buffer, nInDataLen);
    }
    if(me->gpGreyBits[nCode])
    {
        GreyBit_Free(me->gbMem,me->gpGreyBits[nCode]);
    }

    me->gpGreyBits[nCode]     = pByteData;
    me->pnGreySize[nCode]     = (GBF_Lenght)nOutLen;
    me->gbOffsetTable[nCode]  = SET_RAM(nCode);
    me->gbWidthTable[nCode]   = (GBF_Width)bitmap->width;
    me->gbHoriOffTable[nCode] = (GBF_HoriOff)pData->horioff;
    return 0;
}

int	GreyBitFile_Encoder_Flush(GB_Encoder codec)
{
    GBF_Encoder me = (GBF_Encoder)codec;

    GreyBitFile_Encoder_BuildAll(me);
    GreyBitFile_Encoder_WriteAll(me);
    return 0;
}

void GreyBitFile_Encoder_Done(GB_Encoder codec)
{
    GBF_Encoder me = (GBF_Encoder)codec;

    GreyBitFile_Encoder_ClearCache(me);
    GreyBit_Free(me->gbMem,codec);
}

//-----------------------------------------------------------------------------
// Local function
//-----------------------------------------------------------------------------
// Compress Function
static int GreyBitFile_Encoder_Compress(GB_BYTE *pOutData, GB_INT32 *pnInOutLen, GB_BYTE *pInData, GB_INT32 nInDataLen)
{
    GB_BYTE nLen, nData, nNextData;
    GB_INT32 i;
    GB_INT32 nOutlLen = *pnInOutLen;
    GB_INT32 nCompressLen = 0;
    
    nLen  = 0;
    nData = pInData[0]>>1;
    if(pOutData)
    {
        for(i=1;i<nInDataLen;i++)
        {
            nNextData = pInData[i]>>1;
            if(nData == nNextData && nLen < MAX_LEN())
            {
                nLen++;
            }
            else
            {
                if(nLen == 0)
                {
                    pOutData[nCompressLen] = nData;
                    nCompressLen++;
                }
                else
                {
                    pOutData[nCompressLen] = SET_LEN(nLen);
                    nCompressLen++;
                    pOutData[nCompressLen] = nData;
                    nCompressLen++;
                    nLen = 0;
                }
            }
            nData = nNextData;
        }

        if(nLen == 0)
        {
            pOutData[nCompressLen] = nData;
            nCompressLen++;
        }
        else
        {
            pOutData[nCompressLen] = SET_LEN(nLen);
            nCompressLen++;
            pOutData[nCompressLen] = nData;
            nCompressLen++;
        }
        //pOutData[nCompressLen] = SET_END();
        //nCompressLen++;
    }
    else
    {
        for(i=1;i<nInDataLen;i++)
        {
            nNextData = pInData[i]>>1;
            if(nData == nNextData && nLen < MAX_LEN())
            {
                nLen++;
            }
            else
            {
                if(nLen == 0)
                {
                    nCompressLen++;
                }
                else
                {
                    nCompressLen += 2;
                    nLen = 0;
                }
            }
            nData = nNextData;
        }

        if(nLen == 0)
        {
            nCompressLen++;
        }
        else
        {
            nCompressLen += 2;
        }

        // End Char
        //nCompressLen++;
    }
    *pnInOutLen = nCompressLen;
    return 0;
}

static void GreyBitFile_Encoder_ClearCache(GBF_Encoder me)
{
    if(me->gbWidthTable)
    {
        GreyBit_Free(me->gbMem,me->gbWidthTable);
    }
    
    if(me->gbHoriOffTable)
    {
        GreyBit_Free(me->gbMem,me->gbHoriOffTable);
    }

    if(me->gbOffsetTable)
    {
        GreyBit_Free(me->gbMem,me->gbOffsetTable);
    }

    if(me->pnGreySize)
    {
        GreyBit_Free(me->gbMem,me->pnGreySize);
    }

    if(me->gpGreyBits)
    {
        GB_INT32 i;
        for(i=0;i<me->nCacheItem;i++)
        {
            if(me->gpGreyBits[i])
            {
                GreyBit_Free(me->gbMem, me->gpGreyBits[i]);
            }
        }
        GreyBit_Free(me->gbMem,me->gpGreyBits);
    }
}

static int GreyBitFile_Encoder_Init(GBF_Encoder me)
{
    me->gbWidthTable   = (GBF_Width *)GreyBit_Malloc(me->gbMem, MAX_COUNT*sizeof(GBF_Width));
    me->gbHoriOffTable = (GBF_HoriOff *)GreyBit_Malloc(me->gbMem, MAX_COUNT*sizeof(GBF_HoriOff));
    me->gbOffsetTable  = (GBF_Offset *)GreyBit_Malloc(me->gbMem, MAX_COUNT*sizeof(GBF_Offset));
    me->gpGreyBits     = (GB_BYTE **)GreyBit_Malloc(me->gbMem, MAX_COUNT*sizeof(GB_BYTE *));
    me->pnGreySize     = (GBF_Lenght *)GreyBit_Malloc(me->gbMem, MAX_COUNT*sizeof(GBF_Lenght));
    me->nCacheItem     = MAX_COUNT;
    GB_MEMSET(me->gbWidthTable,GB_WIDTH_DEFAULT,MAX_COUNT*sizeof(GBF_Width));
    GB_MEMSET(me->gbHoriOffTable,GB_HORIOFF_DEFAULT,MAX_COUNT*sizeof(GBF_HoriOff));
    return 0;
}

static int GreyBitFile_Encoder_BuildAll(GBF_Encoder me)
{
    GB_INT32 nSection, nSectionLen;
    GB_UINT16 nMaxCode, nMinCode; 
    GB_INT32 nCode;
    GB_UINT32 nWidthTableSize = 0, nHoriOffTableSize = 0, nOffSetTableSize = 0, nGreyBitSize = 0, nCount = 0;
    GBF_Lenght nSize;
    // Width and Index Info
    for(nSection = 0; nSection < UNICODE_SECTION_NUM; nSection++)
    {
        UnicodeSection_GetSectionInfo(nSection, &nMinCode, &nMaxCode);
        nSectionLen = (nMaxCode-nMinCode+1);
        for(nCode = nMinCode; nCode <= nMaxCode; nCode++)
        {
            if(me->gbWidthTable[nCode] != GB_WIDTH_DEFAULT)
            {
                me->gbInfoHeader.gbiWidthSection.gbSectionOff[nSection] = (GBF_Index)(nWidthTableSize/sizeof(GBF_Width))+1;
                me->gbInfoHeader.gbiIndexSection.gbSectionOff[nSection] = (GBF_Index)(nOffSetTableSize/sizeof(GBF_Offset))+1;
                nWidthTableSize += nSectionLen*sizeof(GBF_Width);
                nHoriOffTableSize += nSectionLen*sizeof(GBF_HoriOff);
                nOffSetTableSize += nSectionLen*sizeof(GBF_Offset);
                break;
            }
        }
    }
    if(me->gbInfoHeader.gbiCompression && me->gbInfoHeader.gbiBitCount == 8)
    {
        for(nCode = 0; nCode<MAX_COUNT; nCode++)
        {
            nSize = me->pnGreySize[nCode];
            if(nSize)
            {
                me->gbOffsetTable[nCode] = nGreyBitSize;
                nGreyBitSize += nSize+sizeof(GBF_Lenght);
                nCount++;
            }
        }
    }
    else
    {
        for(nCode = 0; nCode<MAX_COUNT; nCode++)
        {
            nSize = me->pnGreySize[nCode];
            if(nSize)
            {
                me->gbOffsetTable[nCode] = nGreyBitSize;
                nGreyBitSize += nSize;
                nCount++;
            }
        }
    }
    
    me->gbInfoHeader.gbiCount = nCount;
    me->gbInfoHeader.gbiOffGreyBits  = nWidthTableSize+nHoriOffTableSize+nOffSetTableSize;
    me->gbInfoHeader.gbiOffsetTabOff = nWidthTableSize+nHoriOffTableSize;
    me->gbInfoHeader.gbiHoriOffTabOff= nWidthTableSize;
    me->gbInfoHeader.gbiWidthTabOff  = 0;
    me->gbInfoHeader.gbiSize         = sizeof(GREYBITINFOHEADER);
    GB_STRNCPY(me->gbFileHeader.gbfTag, "gbtf",GB_STRLEN("gbtf"));
    return 0;
}

static int GreyBitFile_Encoder_WriteAll(GBF_Encoder me)
{
    GB_INT32 nSection, nSectionLen;
    GB_UINT16 nMaxCode, nMinCode; 
    GB_INT32 nCode;
    GB_BYTE *pData;
    GB_INT32 nDataSize;

    GreyBit_Stream_Seek(me->gbStream, 0);
    GreyBit_Stream_Write(me->gbStream, (GB_BYTE *)&me->gbFileHeader, sizeof(GREYBITFILEHEADER));
    GreyBit_Stream_Write(me->gbStream, (GB_BYTE *)&me->gbInfoHeader, sizeof(GREYBITINFOHEADER));

    // Width Info
    for(nSection = 0; nSection < UNICODE_SECTION_NUM; nSection++)
    {
        UnicodeSection_GetSectionInfo(nSection, &nMinCode, &nMaxCode);
        nSectionLen = (nMaxCode-nMinCode+1);
        
        if(me->gbInfoHeader.gbiWidthSection.gbSectionOff[nSection] != 0)
        {
            pData = &me->gbWidthTable[nMinCode];
            nDataSize = (GBF_Lenght)nSectionLen*sizeof(GBF_Width);
            GreyBit_Stream_Write(me->gbStream, pData, nDataSize);
        }
    }
    
    // Hori Offset Info
    for(nSection = 0; nSection < UNICODE_SECTION_NUM; nSection++)
    {
        UnicodeSection_GetSectionInfo(nSection, &nMinCode, &nMaxCode);
        nSectionLen = (nMaxCode-nMinCode+1);
        
        if(me->gbInfoHeader.gbiWidthSection.gbSectionOff[nSection] != 0)
        {
            pData = &me->gbHoriOffTable[nMinCode];
            nDataSize = (GBF_Lenght)nSectionLen*sizeof(GBF_HoriOff);
            GreyBit_Stream_Write(me->gbStream, pData, nDataSize);
        }
    }
    
    // Index Info
    for(nSection = 0; nSection < UNICODE_SECTION_NUM; nSection++)
    {
        UnicodeSection_GetSectionInfo(nSection, &nMinCode, &nMaxCode);
        nSectionLen = (nMaxCode-nMinCode+1);
        if(me->gbInfoHeader.gbiIndexSection.gbSectionOff[nSection] != 0)
        {
            pData = (GB_BYTE *)&me->gbOffsetTable[nMinCode];
            nDataSize = (GBF_Lenght)nSectionLen*sizeof(GBF_Offset);
            GreyBit_Stream_Write(me->gbStream, pData, nDataSize);
        }
    }
    
    // GeryBits
    if(me->gbInfoHeader.gbiCompression && me->gbInfoHeader.gbiBitCount == 8)
    {
        for(nCode = 0; nCode<me->nCacheItem; nCode++)
        {
            nDataSize = me->pnGreySize[nCode];
            if(nDataSize)
            {
                // 写入压缩数据的长度
                GreyBit_Stream_Write(me->gbStream, (GB_BYTE *)&nDataSize, sizeof(GBF_Lenght));
                pData = me->gpGreyBits[nCode];
                GreyBit_Stream_Write(me->gbStream, pData, nDataSize);
            }
        }
    }
    else
    {
        for(nCode = 0; nCode<me->nCacheItem; nCode++)
        {
            nDataSize = me->pnGreySize[nCode];
            if(nDataSize)
            {
                pData = me->gpGreyBits[nCode];
                GreyBit_Stream_Write(me->gbStream, pData, nDataSize);
            }
        }
    }
    return 0;
}

static int GreyBitFile_Encoder_InfoInit(GBF_Encoder me, GB_INT16 nHeight,GB_INT16 nBitCount, GB_INT16 bCompress)
{
    me->gbInfoHeader.gbiBitCount  = nBitCount;
    me->gbInfoHeader.gbiHeight = nHeight;
    if(nBitCount ==  8)
    {
        me->gbInfoHeader.gbiCompression = bCompress;
    }
    else
    {
        me->gbInfoHeader.gbiCompression = 0;
    }
    return 0;
}
#endif
#endif //#ifdef ENABLE_GREYBITFILE