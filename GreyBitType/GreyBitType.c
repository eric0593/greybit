#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"

static GB_Format GreyBitType_Format_Init(GB_Library library);
static void      GreyBitType_Format_Done(GB_Library library);
static void      GreyBitType_Format_Insert(GB_Library library,GB_Format format);

extern void         GreyBit_Free_Sys(void *p);

//-----------------------------------------------------------------------------
// Library
//-----------------------------------------------------------------------------
GBHANDLE GreyBitType_Init(void)
{
    GB_Library gbLib = (GB_Library)GreyBit_Malloc_Sys(sizeof(GB_LibraryRec));
    if(gbLib)
    {
        gbLib->gbMem = GreyBit_Memory_New();
        gbLib->gbFormatHeader = GreyBitType_Format_Init(gbLib);
    }
    return gbLib;
}

void GreyBitType_Done(GBHANDLE library)
{
    GB_Library gbLib = (GB_Library)library;
    GreyBitType_Format_Done(gbLib);
    GreyBit_Memory_Done(gbLib->gbMem);
    GreyBit_Free_Sys(gbLib);
}
#ifdef ENABLE_GREYCOMBINEFILE
extern GB_Format GreyCombineFile_Format_New(GB_Library library);
#endif
#ifdef ENABLE_GREYBITFILE
extern GB_Format GreyBitFile_Format_New(GB_Library library);
#endif
#ifdef ENABLE_TRUETYPEFILE
extern GB_Format TrueTypeFile_Format_New(GB_Library library);
#endif
#ifdef ENABLE_GREYVECTORFILE
extern GB_Format GreyVectorFile_Format_New(GB_Library library);
#endif
static GB_Format GreyBitType_Format_Init(GB_Library library)
{
    GB_Format format = 0;
#ifdef ENABLE_GREYCOMBINEFILE
    format = GreyCombineFile_Format_New(library);
    GreyBitType_Format_Insert(library, format);
#endif
#ifdef ENABLE_GREYBITFILE
    format = GreyBitFile_Format_New(library);
    GreyBitType_Format_Insert(library, format);
#endif
#ifdef ENABLE_TRUETYPEFILE
    format = TrueTypeFile_Format_New(library);
    GreyBitType_Format_Insert(library, format);
#endif
#ifdef ENABLE_GREYVECTORFILE
    format = GreyVectorFile_Format_New(library);
    GreyBitType_Format_Insert(library, format);
#endif
    return library->gbFormatHeader;
}

static void GreyBitType_Format_Done(GB_Library library)
{
    GB_Format next;
    while(library->gbFormatHeader)
    {
        next = library->gbFormatHeader->next;
        GreyBit_Format_Done(library->gbFormatHeader, library->gbMem);
        library->gbFormatHeader = next;
    }
}

static void GreyBitType_Format_Insert(GB_Library library,GB_Format format)
{
    if(format)
    {
        GB_Format curr = library->gbFormatHeader;
        library->gbFormatHeader = format;
        format->next = curr;
    }
}
//-----------------------------------------------------------------------------
// Bitmap
//-----------------------------------------------------------------------------
GB_Bitmap GreyBitType_Bitmap_New(GBHANDLE library, GB_INT16 nWidth, GB_INT16 nHeight, GB_INT16 bitcount, GB_BYTE *pInitBuf)
{
    GB_Library gbLib = (GB_Library)library;
    GB_BitmapRec currBmp;
    GB_Bitmap bitmap;
    GB_INT32 nBufSize;
    
    currBmp.width       = nWidth;
    currBmp.height      = nHeight;
    currBmp.bitcount    = bitcount;
    currBmp.pitch       = (((nWidth<<3)*bitcount)+63)>>6;
    nBufSize = currBmp.pitch*currBmp.height;
    
    bitmap = (GB_Bitmap)GreyBit_Malloc(gbLib->gbMem,sizeof(GB_BitmapRec));
    if(bitmap)
    {
        bitmap->width       = nWidth;
        bitmap->height      = nHeight;
        bitmap->bitcount    = bitcount;
        bitmap->pitch       = currBmp.pitch;
        bitmap->buffer      = (GB_BYTE *)GreyBit_Malloc(gbLib->gbMem, nBufSize);
        if(!bitmap->buffer)
        {
            GreyBit_Free(gbLib->gbMem,bitmap);
            return 0;
        }
        
        if(pInitBuf)
        {
            GB_MEMCPY(bitmap->buffer, pInitBuf, nBufSize);
        }
    }
    return bitmap;
}

void GreyBitType_Bitmap_Done(GBHANDLE library, GB_Bitmap bitmap)
{
    GB_Library gbLib = (GB_Library)library;
    GreyBit_Free(gbLib->gbMem,bitmap->buffer);
    GreyBit_Free(gbLib->gbMem,bitmap);
}

//-----------------------------------------------------------------------------
// Outline
//-----------------------------------------------------------------------------
GB_Outline GreyBitType_Outline_New(GBHANDLE library, GB_INT16 n_contours, GB_INT16 n_points)
{
    GB_Library gbLib = (GB_Library)library;
    GB_INT32 nSize = sizeof(GB_OutlineRec)+n_contours*sizeof(GB_INT16)+n_points*(sizeof(GB_PointRec)+sizeof(GB_BYTE));
    GB_Outline outline = (GB_Outline)GreyBit_Malloc(gbLib->gbMem, nSize);
    if(outline)
    {
        GB_BYTE *pBase = (GB_BYTE *)outline;

        outline->n_contours = n_contours;
        outline->n_points   = n_points;

        pBase += sizeof(GB_OutlineRec);
        outline->contours   = (GB_INT16 *)pBase;

        pBase += n_contours*sizeof(GB_INT16);
        outline->points     = (GB_Point)pBase;

        pBase += n_points*sizeof(GB_PointRec);
        outline->tags       = pBase;
    }
    return outline;
}

GB_Outline GreyBitType_Outline_Clone(GBHANDLE library, GB_Outline source)
{
    GB_Outline outline = GreyBitType_Outline_New(library, source->n_contours, source->n_points);
    if(outline)
    {
        int i;
        for(i=0;i<outline->n_contours;i++)
        {
            outline->contours[i] = source->contours[i];
        }

        for(i=0;i<outline->n_points;i++)
        {
            outline->points[i].x = source->points[i].x;
            outline->points[i].y = source->points[i].y;
            outline->tags[i]     = source->tags[i];
        }
    }
    return outline;
}

int GreyBitType_Outline_Remove(GB_Outline outline, GB_INT16 idx)
{
    int i;
    int inc = 0;
    int idxinc = 0;
    
    if(idx > outline->n_points || idx < 0)
    {
        return -1;
    }
    
    for(i=0; i<outline->n_contours; i++)
    {
        GB_INT16 curr = outline->contours[i];
        if(curr >= idx)
        {
            inc = 1;
        }
        
        curr -= inc;
        if(curr<0)
        {
            // Remove contour
            idxinc++;
        }
        else
        {
            outline->contours[i-idxinc] = curr;
        }
    }
    outline->n_contours -= idxinc;
    
    outline->n_points--;
    for(i=idx;i<outline->n_points;i++)
    {
        int next = i+1;
        outline->points[i].x = outline->points[next].x;
        outline->points[i].y = outline->points[next].y;
        outline->tags[i]     = outline->tags[next];
    }
    return 0;
}

GB_INT32 GreyBitType_Outline_GetSize(GB_Outline outline)
{
    return GreyBitType_Outline_GetSizeEx(outline->n_contours, outline->n_points);
}

GB_INT32 GreyBitType_Outline_GetSizeEx(GB_INT16 n_contours, GB_INT16 n_points)
{
    return (sizeof(GB_OutlineRec)+n_contours*sizeof(GB_INT16)+n_points*(sizeof(GB_PointRec)+sizeof(GB_BYTE)));
}

int GreyBitType_Outline_Transform(GB_Outline outline, GB_Outline source, GB_INT16 tosize, GB_INT16 fromsize)
{
    int i;
    
    outline->n_contours = source->n_contours;
    outline->n_points   = source->n_points;
    for(i=0;i<outline->n_contours;i++)
    {
        outline->contours[i] = source->contours[i];
    }

    for(i=0;i<outline->n_points;i++)
    {
        outline->points[i].x = source->points[i].x*tosize/fromsize;
        outline->points[i].y = source->points[i].y*tosize/fromsize;
        outline->tags[i]     = source->tags[i];
    }
    return 0;
}

void GreyBitType_Outline_Done(GBHANDLE library, GB_Outline outline)
{
    GB_Library gbLib = (GB_Library)library;
    //GreyBit_Free(gbLib->gbMem,outline->contours);
    //GreyBit_Free(gbLib->gbMem,outline->points);
    //GreyBit_Free(gbLib->gbMem,outline->tags);
    GreyBit_Free(gbLib->gbMem,outline);
}

//-----------------------------------------------------------------------------
// Format
//-----------------------------------------------------------------------------
GB_Decoder GreyBit_Format_DecoderNew(GB_Format format, GB_Loader loader, GB_Stream stream)
{
    if(format->decodernew)
    {
        return format->decodernew(loader, stream);
    }
    return 0;
}

#ifdef ENABLE_ENCODER
GB_Encoder GreyBit_Format_EncoderNew(GB_Format format, GB_Creator creator, GB_Stream stream)
{
    if(format->encodernew)
    {
        return format->encodernew(creator, stream);
    }
    return 0;
}
#endif

int GreyBit_Format_Probe(GB_Format format, GB_Stream stream)
{
    if(format->probe)
    {
        return format->probe(stream);
    }
    return 0;
}

void GreyBit_Format_Done(GB_Format format, GB_Memory mem)
{
    GreyBit_Free(mem,format);
}

