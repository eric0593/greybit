#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"

//-----------------------------------------------------------------------------
// Decoder
//-----------------------------------------------------------------------------
int GreyBit_Decoder_SetParam(GB_Decoder decoder, GB_Param nParam, GB_UINT32 dwParam)
{
    //if(decoder->setparam)
    {
        return decoder->setparam(decoder, nParam, dwParam);
    }
    //return -1;
}

GB_INT32 GreyBit_Decoder_GetCount(GB_Decoder decoder)
{
    //if(decoder->getcount)
    {
        return decoder->getcount(decoder);
    }
    //return 0;
}

GB_INT32 GreyBit_Decoder_GetHeight(GB_Decoder decoder)
{
    //if(decoder->getheight)
    {
        return decoder->getheight(decoder);
    }
    //return 0;
}

GB_INT32 GreyBit_Decoder_GetWidth(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize)
{
    //if(decoder->getwidth)
    {
        return decoder->getwidth(decoder, nCode, nSize);
    }
    //return 0;
}

GB_INT32 GreyBit_Decoder_GetAdvance(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize)
{
    //if(decoder->getwidth)
    {
        return decoder->getadvance(decoder, nCode, nSize);
    }
    //return 0;
}

int GreyBit_Decoder_Decode(GB_Decoder decoder, GB_UINT32 nCode, GB_Data pData, GB_INT16 nSize)
{
    //if(decoder->decode)
    {
        return decoder->decode(decoder, nCode, pData, nSize);
    }
    //return -1;
}

void GreyBit_Decoder_Done(GB_Decoder decoder)
{
    //if(decoder->done)
    {
        decoder->done(decoder);
    }
}

#ifdef ENABLE_ENCODER
//-----------------------------------------------------------------------------
// Encoder
//-----------------------------------------------------------------------------
GB_INT32 GreyBit_Encoder_GetCount(GB_Encoder encoder)
{
    //if(encoder->getcount)
    {
        return encoder->getcount(encoder);
    }
    //return 0;
}

int GreyBit_Encoder_SetParam(GB_Encoder encoder, GB_Param nParam, GB_UINT32 dwParam)
{
    //if(encoder->setparam)
    {
        return encoder->setparam(encoder, nParam, dwParam);
    }
    //return -1;
}

int GreyBit_Encoder_Delete(GB_Encoder encoder, GB_UINT32 nCode)
{
    //if(encoder->remove)
    {
        return encoder->remove(encoder, nCode);
    }
    //return -1;
}

int GreyBit_Encoder_Encode(GB_Encoder encoder, GB_UINT32 nCode, GB_Data pData)
{
    //if(encoder->encode)
    {
        return encoder->encode(encoder, nCode, pData);
    }
    //return -1;
}

int GreyBit_Encoder_Flush(GB_Encoder encoder)
{
    //if(encoder->flush)
    {
        return encoder->flush(encoder);
    }
    //return -1;
}

void GreyBit_Encoder_Done(GB_Encoder encoder)
{
    //if(encoder->done)
    {
        encoder->done(encoder);
    }
}
#endif

