#ifndef GREYBITRASTER_H_
#define GREYBITRASTER_H_
#include "GreyBitType.h"

#define DEFAULT_POOL_SIZE   (16384)

typedef struct  GB_BBox_
{
    GB_Pos  xMin, yMin;
    GB_Pos  xMax, yMax;
} GB_BBox;

#ifdef __cplusplus
  extern "C" {
#endif

#define GB_Vector   GB_PointRec
typedef struct  _RST_Vector
{
    GB_INT32 x;
    GB_INT32 y;
} RST_Vector;

  /*************************************************************************/
  /*                                                                       */
  /* <FuncType>                                                            */
  /*    GB_Outline_MoveToFunc                                              */
  /*                                                                       */
  /* <Description>                                                         */
  /*    A function pointer type used to describe the signature of a `move  */
  /*    to' function during outline walking/decomposition.                 */
  /*                                                                       */
  /*    A `move to' is emitted to start a new contour in an outline.       */
  /*                                                                       */
  /* <Input>                                                               */
  /*    to   :: A pointer to the target point of the `move to'.            */
  /*                                                                       */
  /*    user :: A typeless pointer which is passed from the caller of the  */
  /*            decomposition function.                                    */
  /*                                                                       */
  /* <Return>                                                              */
  /*    Error code.  0~means success.                                      */
  /*                                                                       */
  typedef int
  (*GB_Outline_MoveToFunc)( const RST_Vector*  to,
                            void*             user );

#define GB_Outline_MoveTo_Func  GB_Outline_MoveToFunc


  /*************************************************************************/
  /*                                                                       */
  /* <FuncType>                                                            */
  /*    GB_Outline_LineToFunc                                              */
  /*                                                                       */
  /* <Description>                                                         */
  /*    A function pointer type used to describe the signature of a `line  */
  /*    to' function during outline walking/decomposition.                 */
  /*                                                                       */
  /*    A `line to' is emitted to indicate a segment in the outline.       */
  /*                                                                       */
  /* <Input>                                                               */
  /*    to   :: A pointer to the target point of the `line to'.            */
  /*                                                                       */
  /*    user :: A typeless pointer which is passed from the caller of the  */
  /*            decomposition function.                                    */
  /*                                                                       */
  /* <Return>                                                              */
  /*    Error code.  0~means success.                                      */
  /*                                                                       */
  typedef int
  (*GB_Outline_LineToFunc)( const RST_Vector*  to,
                            void*             user );

#define GB_Outline_LineTo_Func  GB_Outline_LineToFunc


  /*************************************************************************/
  /*                                                                       */
  /* <FuncType>                                                            */
  /*    GB_Outline_ConicToFunc                                             */
  /*                                                                       */
  /* <Description>                                                         */
  /*    A function pointer type used to describe the signature of a `conic */
  /*    to' function during outline walking or decomposition.              */
  /*                                                                       */
  /*    A `conic to' is emitted to indicate a second-order B��zier arc in   */
  /*    the outline.                                                       */
  /*                                                                       */
  /* <Input>                                                               */
  /*    control :: An intermediate control point between the last position */
  /*               and the new target in `to'.                             */
  /*                                                                       */
  /*    to      :: A pointer to the target end point of the conic arc.     */
  /*                                                                       */
  /*    user    :: A typeless pointer which is passed from the caller of   */
  /*               the decomposition function.                             */
  /*                                                                       */
  /* <Return>                                                              */
  /*    Error code.  0~means success.                                      */
  /*                                                                       */
  typedef int
  (*GB_Outline_ConicToFunc)( const RST_Vector*  control,
                             const RST_Vector*  to,
                             void*             user );

#define GB_Outline_ConicTo_Func  GB_Outline_ConicToFunc


  /*************************************************************************/
  /*                                                                       */
  /* <FuncType>                                                            */
  /*    GB_Outline_CubicToFunc                                             */
  /*                                                                       */
  /* <Description>                                                         */
  /*    A function pointer type used to describe the signature of a `cubic */
  /*    to' function during outline walking or decomposition.              */
  /*                                                                       */
  /*    A `cubic to' is emitted to indicate a third-order B��zier arc.      */
  /*                                                                       */
  /* <Input>                                                               */
  /*    control1 :: A pointer to the first B��zier control point.           */
  /*                                                                       */
  /*    control2 :: A pointer to the second B��zier control point.          */
  /*                                                                       */
  /*    to       :: A pointer to the target end point.                     */
  /*                                                                       */
  /*    user     :: A typeless pointer which is passed from the caller of  */
  /*                the decomposition function.                            */
  /*                                                                       */
  /* <Return>                                                              */
  /*    Error code.  0~means success.                                      */
  /*                                                                       */
  typedef int
  (*GB_Outline_CubicToFunc)( const RST_Vector*  control1,
                             const RST_Vector*  control2,
                             const RST_Vector*  to,
                             void*             user );

#define GB_Outline_CubicTo_Func  GB_Outline_CubicToFunc


  /*************************************************************************/
  /*                                                                       */
  /* <Struct>                                                              */
  /*    GB_Outline_Funcs                                                   */
  /*                                                                       */
  /* <Description>                                                         */
  /*    A structure to hold various function pointers used during outline  */
  /*    decomposition in order to emit segments, conic, and cubic B��ziers. */
  /*                                                                       */
  /* <Fields>                                                              */
  /*    move_to  :: The `move to' emitter.                                 */
  /*                                                                       */
  /*    line_to  :: The segment emitter.                                   */
  /*                                                                       */
  /*    conic_to :: The second-order B��zier arc emitter.                   */
  /*                                                                       */
  /*    cubic_to :: The third-order B��zier arc emitter.                    */
  /*                                                                       */
  /*    shift    :: The shift that is applied to coordinates before they   */
  /*                are sent to the emitter.                               */
  /*                                                                       */
  /*    delta    :: The delta that is applied to coordinates before they   */
  /*                are sent to the emitter, but after the shift.          */
  /*                                                                       */
  /* <Note>                                                                */
  /*    The point coordinates sent to the emitters are the transformed     */
  /*    version of the original coordinates (this is important for high    */
  /*    accuracy during scan-conversion).  The transformation is simple:   */
  /*                                                                       */
  /*    {                                                                  */
  /*      x' = (x << shift) - delta                                        */
  /*      y' = (x << shift) - delta                                        */
  /*    }                                                                  */
  /*                                                                       */
  /*    Set the values of `shift' and `delta' to~0 to get the original     */
  /*    point coordinates.                                                 */
  /*                                                                       */
typedef struct  GB_Outline_Funcs_
{
    GB_Outline_MoveToFunc   move_to;
    GB_Outline_LineToFunc   line_to;
    GB_Outline_ConicToFunc  conic_to;
    GB_Outline_CubicToFunc  cubic_to;

    int                     shift;
    GB_Pos                  delta;
} GB_Outline_Funcs;

/*************************************************************************/
  /*                                                                       */
  /* <Struct>                                                              */
  /*    GB_Span                                                            */
  /*                                                                       */
  /* <Description>                                                         */
  /*    A structure used to model a single span of gray (or black) pixels  */
  /*    when rendering a monochrome or anti-aliased bitmap.                */
  /*                                                                       */
  /* <Fields>                                                              */
  /*    x        :: The span's horizontal start position.                  */
  /*                                                                       */
  /*    len      :: The span's length in pixels.                           */
  /*                                                                       */
  /*    coverage :: The span color/coverage, ranging from 0 (background)   */
  /*                to 255 (foreground).  Only used for anti-aliased       */
  /*                rendering.                                             */
  /*                                                                       */
  /* <Note>                                                                */
  /*    This structure is used by the span drawing callback type named     */
  /*    @GB_SpanFunc which takes the y~coordinate of the span as a         */
  /*    a parameter.                                                       */
  /*                                                                       */
  /*    The coverage value is always between 0 and 255.  If you want less  */
  /*    gray values, the callback function has to reduce them.             */
  /*                                                                       */
  typedef struct  GB_Span_
  {
    GB_INT16           x;
    GB_UINT16  len;
    GB_BYTE   coverage;

  } GB_Span;


  /*************************************************************************/
  /*                                                                       */
  /* <FuncType>                                                            */
  /*    GB_SpanFunc                                                        */
  /*                                                                       */
  /* <Description>                                                         */
  /*    A function used as a call-back by the anti-aliased renderer in     */
  /*    order to let client applications draw themselves the gray pixel    */
  /*    spans on each scan line.                                           */
  /*                                                                       */
  /* <Input>                                                               */
  /*    y     :: The scanline's y~coordinate.                              */
  /*                                                                       */
  /*    count :: The number of spans to draw on this scanline.             */
  /*                                                                       */
  /*    spans :: A table of `count' spans to draw on the scanline.         */
  /*                                                                       */
  /*    user  :: User-supplied data that is passed to the callback.        */
  /*                                                                       */
  /* <Note>                                                                */
  /*    This callback allows client applications to directly render the    */
  /*    gray spans of the anti-aliased bitmap to any kind of surfaces.     */
  /*                                                                       */
  /*    This can be used to write anti-aliased outlines directly to a      */
  /*    given background bitmap, and even perform translucency.            */
  /*                                                                       */
  /*    Note that the `count' field cannot be greater than a fixed value   */
  /*    defined by the `GB_MAX_GRAY_SPANS' configuration macro in          */
  /*    `ftoption.h'.  By default, this value is set to~32, which means    */
  /*    that if there are more than 32~spans on a given scanline, the      */
  /*    callback is called several times with the same `y' parameter in    */
  /*    order to draw all callbacks.                                       */
  /*                                                                       */
  /*    Otherwise, the callback is only called once per scan-line, and     */
  /*    only for those scanlines that do have `gray' pixels on them.       */
  /*                                                                       */
  typedef void
  (*GB_SpanFunc)( int             y,
                  int             count,
                  const GB_Span*  spans,
                  void*           user );

#define GB_Raster_Span_Func  GB_SpanFunc

extern GBHANDLE GreyBit_Raster_New(GBHANDLE library, int nPoolSize);
extern int      GreyBit_Raster_Render(GBHANDLE raster, GB_Bitmap tobitmap, GB_Outline fromoutline);
extern void     GreyBit_Raster_Done(GBHANDLE raster);

#ifdef __cplusplus
  }
#endif
#endif


