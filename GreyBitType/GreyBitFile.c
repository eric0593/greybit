#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"
#ifdef ENABLE_GREYBITFILE
#include "GreyBitFile.h"

//-----------------------------------------------------------------------------
// Format
//-----------------------------------------------------------------------------
GB_BOOL GreyBitFile_Probe(GB_Stream stream)
{
    GREYBITFILEHEADER fileHeader;
    GreyBit_Stream_Seek(stream, 0);
    if(GreyBit_Stream_Read(stream, (GB_BYTE *)&fileHeader, sizeof(GREYBITFILEHEADER)) == sizeof(GREYBITFILEHEADER))
    {
        if( fileHeader.gbfTag[0] == 'g' 
          &&fileHeader.gbfTag[1] == 'b'
          &&fileHeader.gbfTag[2] == 't' 
          &&fileHeader.gbfTag[3] == 'f' )
        {
            return GB_TRUE;
        }
    }
    return GB_FALSE;
}

GB_Format GreyBitFile_Format_New(GB_Library library)
{
    GB_Format format = (GB_Format)GreyBit_Malloc(library->gbMem,sizeof(GB_FormatRec));
    if(format)
    {
        format->next        = 0;
        format->tag[0]      = 'g';
        format->tag[1]      = 'b';
        format->tag[2]      = 'f';
        format->probe       = GreyBitFile_Probe;
        format->decodernew  = GreyBitFile_Decoder_New;
#ifdef ENABLE_ENCODER
        format->encodernew  = GreyBitFile_Encoder_New;
#endif
    }
    return format;
}
#endif

