#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"

#ifdef ENABLE_GREYVECTORFILE
#include "GreyVectorFile.h"

/*************************************************************************/
/*                                                                       */
/*                              Struct                                   */
/*                                                                       */
/*************************************************************************/
static int GreyVectorFile_Decoder_Init(GVF_Decoder me);
static int GreyVectorFile_Decoder_ReadHeader(GVF_Decoder me);
static int GreyVectorFile_Decoder_InfoInit(GVF_Decoder me, GB_INT16 nMaxWidth, GB_INT16 nHeight, GB_INT16 nMaxPoints, GB_INT16 nMaxContours);

static GVF_Offset GreyVectorFile_Decoder_GetDataOffset(GVF_Decoder me, GB_UINT32 nCode);
static int  GreyVectorFile_Decoder_CaheItem(GVF_Decoder me, GB_UINT32 nCode, GB_Outline outline);
static void GreyVectorFile_Decoder_ClearCache(GVF_Decoder me);
static GB_INT16 GreyVectorFile_Decoder_GetHoriOff(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize);

//-----------------------------------------------------------------------------
// Decoder
//-----------------------------------------------------------------------------
GB_Decoder GreyVectorFile_Decoder_New(GB_Loader loader, GB_Stream stream)
{
    GVF_Decoder decoder = (GVF_Decoder)GreyBit_Malloc(loader->gbMem,sizeof(GVF_DecoderRec));
    if(decoder)
    {
        decoder->gbDecoder.setparam     = GreyVectorFile_Decoder_SetParam;
        decoder->gbDecoder.getcount     = GreyVectorFile_Decoder_GetCount;
        decoder->gbDecoder.getheight    = GreyVectorFile_Decoder_GetHeight;
        decoder->gbDecoder.getwidth     = GreyVectorFile_Decoder_GetWidth;
        decoder->gbDecoder.getadvance   = GreyVectorFile_Decoder_GetAdvance;
        decoder->gbDecoder.decode       = GreyVectorFile_Decoder_Decode;
        decoder->gbDecoder.done         = GreyVectorFile_Decoder_Done;
        decoder->gbLibrary              = loader->gbLibrary;
        decoder->gbMem                  = loader->gbMem;
        decoder->gbStream               = stream;
        decoder->nCacheItem	            = 0;
        decoder->nItemCount             = 0;
        decoder->gbOffDataBits  = sizeof(GREYVECTORFILEHEADER)+sizeof(GREYVECTORINFOHEADER);
        GreyVectorFile_Decoder_Init(decoder);
    }
    return (GB_Decoder)decoder;
}

int GreyVectorFile_Decoder_SetParam(GB_Decoder decoder, GB_Param nParam, GB_UINT32 dwParam)
{
    GVF_Decoder me = (GVF_Decoder)decoder;
    
    switch(nParam){
    case GB_PARAM_CACHEITEM:
        if(dwParam)
        {
            // Cache Size只能设置一次
            if(me->gpGreyBits)
            {
                return -1;
            }
            me->nCacheItem = (GB_INT32)dwParam;
            me->gpGreyBits = (GB_Outline *)GreyBit_Malloc(me->gbMem, me->nCacheItem*sizeof(GB_Outline));
            me->nGreyBitsCount = 0;
        }
        break;
        
    default:
        return -1;
    }
    return 0;
}

GB_INT32 GreyVectorFile_Decoder_GetCount(GB_Decoder decoder)
{
    GVF_Decoder me = (GVF_Decoder)decoder;
    return me->nItemCount;
}

GB_INT32 GreyVectorFile_Decoder_GetHeight(GB_Decoder decoder)
{
    GVF_Decoder me = (GVF_Decoder)decoder;
    return 0;
}

GB_INT16 GreyVectorFile_Decoder_GetWidth(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize)
{
    GVF_Decoder me = (GVF_Decoder)decoder;
    GVF_Index WidthIdx;
    GB_INT32 UniIndex;
    GB_UINT16 nMinCode;
    GVF_Width nWidth;

    UniIndex = UnicodeSection_GetIndex((GB_UINT16)nCode);
    if(UniIndex >= UNICODE_SECTION_NUM)
    {
        return GB_WIDTH_DEFAULT;
    }
    
    WidthIdx = me->gbInfoHeader.gbiWidthSection.gbSectionOff[UniIndex];
    if(WidthIdx == 0)
    {
        return GB_WIDTH_DEFAULT;
    }

    WidthIdx--;
    if(me->gbWidthTable) // 已读入到内存中
    {
        UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
        nWidth = me->gbWidthTable[WidthIdx+(nCode-nMinCode)];
    }
    else
    {
        UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
        WidthIdx += (GVF_Index)(nCode-nMinCode);
        GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(WidthIdx*sizeof(GVF_Width)+me->gbOffDataBits+me->gbInfoHeader.gbiWidthTabOff));
        GreyBit_Stream_Read(me->gbStream, (GB_BYTE*)&nWidth, sizeof(GVF_Width));
    }
    return nWidth*nSize/me->gbInfoHeader.gbiHeight;
}

GB_INT16 GreyVectorFile_Decoder_GetAdvance(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize)
{
    GB_INT16 nAdvance;
    GB_INT16 nWidth = GreyVectorFile_Decoder_GetWidth(decoder, nCode, nSize);
    GB_INT16 nHoriOff = GreyVectorFile_Decoder_GetHoriOff(decoder, nCode, nSize);

    nAdvance = nWidth+nHoriOff;
    return (nAdvance>0)?nAdvance:0;
}

int	 GreyVectorFile_Decoder_Decode(GB_Decoder decoder, GB_UINT32 nCode, GB_Data pData, GB_INT16 nSize)
{
    GVF_Decoder me = (GVF_Decoder)decoder;
    GVF_Offset Offset = GreyVectorFile_Decoder_GetDataOffset(me, nCode);
    GB_Outline outline;
    GB_INT16 nWidth = GreyVectorFile_Decoder_GetWidth(decoder, nCode, nSize);
    GB_INT16 nHoriOff = GreyVectorFile_Decoder_GetHoriOff(decoder, nCode, nSize);
    GVF_Lenght Lenght;

    if(nWidth == GB_WIDTH_DEFAULT)
    {
        return -1;
    }
    
    if(IS_INRAM(Offset))
    {
        Offset = GET_INDEX(Offset);
        outline = me->gpGreyBits[Offset];
    }
    else
    {
        // 从文件中读取
        GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(Offset+me->gbOffDataBits+me->gbInfoHeader.gbiOffGreyBits));
        GreyBit_Stream_Read(me->gbStream, (GB_BYTE *)&Lenght, sizeof(GVF_Lenght));
        GreyBit_Stream_Read(me->gbStream, me->pBuff+sizeof(GVF_OutlineRec), Lenght);
        outline = GreyBitType_Outline_UpdateByGVF(me->gbOutline, GreyVector_Outline_FromData(me->pBuff));
        GreyVectorFile_Decoder_CaheItem(me, nCode, outline);
    }

    if(outline)
    {
        GreyBitType_Outline_Transform(me->gbOutline, outline, nSize, me->gbInfoHeader.gbiHeight);
        
        if(pData)
        {
            pData->format = GB_FORMAT_OUTLINE;
            pData->data   = me->gbOutline;
            pData->width  = nWidth;
            pData->horioff= nHoriOff;
        }
    }
    else
    {
        return -1;
    }
    return 0;
}

void GreyVectorFile_Decoder_Done(GB_Decoder decoder)
{
    GVF_Decoder me = (GVF_Decoder)decoder;
    GreyVectorFile_Decoder_ClearCache(me);
    GreyBit_Free(me->gbMem,decoder);
}

//-----------------------------------------------------------------------------
// Local function
//-----------------------------------------------------------------------------
static GVF_Offset GreyVectorFile_Decoder_GetDataOffset(GVF_Decoder me, GB_UINT32 nCode)
{
    GB_INT32 UniIndex;
    GVF_Index  SectionIndex;
    GB_UINT16 nMinCode;
    
    UniIndex = UnicodeSection_GetIndex((GB_UINT16)nCode);
    if(UniIndex >= UNICODE_SECTION_NUM)
    {
        return 0;
    }

    SectionIndex = me->gbInfoHeader.gbiIndexSection.gbSectionOff[UniIndex];
    if(SectionIndex == 0)
    {
        return 0;
    }
    
    SectionIndex--;
    if(me->gbOffsetTable) // 已读入到内存中
    {
        UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
        return me->gbOffsetTable[SectionIndex+(nCode-nMinCode)];
    }
    else
    {
        GVF_Offset nOffset;
        
        UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
        SectionIndex += (GVF_Index)(nCode-nMinCode);
        GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(SectionIndex*sizeof(GVF_Offset)+me->gbOffDataBits+me->gbInfoHeader.gbiOffsetTabOff));
        GreyBit_Stream_Read(me->gbStream, (GB_BYTE *)&nOffset, sizeof(GVF_Offset));
        return nOffset;
    }
    return 0;
}

static int GreyVectorFile_Decoder_CaheItem(GVF_Decoder me, GB_UINT32 nCode, GB_Outline outline)
{
    GB_INT32 UniIndex;
    GVF_Index SectionIndex;
    GB_UINT16 nMinCode;

    if(me->nGreyBitsCount >= me->nCacheItem || me->gbOffsetTable == 0)
    {
        return -1;
    }

    UniIndex = UnicodeSection_GetIndex((GB_UINT16)nCode);
    if(UniIndex >= UNICODE_SECTION_NUM)
    {
        return -1;
    }

    SectionIndex = me->gbInfoHeader.gbiIndexSection.gbSectionOff[UniIndex];
    if(SectionIndex == 0)
    {
        return -1;
    }
    
    me->gpGreyBits[me->nGreyBitsCount] = GreyBitType_Outline_Clone(me->gbLibrary, outline);

    // 标志Index Cache
    UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
    SectionIndex--;
    SectionIndex += (GVF_Index)(nCode-nMinCode);
    me->gbOffsetTable[SectionIndex] = SET_RAM(me->nGreyBitsCount);
    me->nGreyBitsCount++;
    return 0;
}

static void GreyVectorFile_Decoder_ClearCache(GVF_Decoder me)
{
    if(me->gbOutline)
    {
        GreyBitType_Outline_Done(me->gbLibrary, me->gbOutline);
    }

    if(me->pBuff)
    {
        GreyBit_Free(me->gbMem,me->pBuff);
    }

    if(me->gbWidthTable)
    {
        GreyBit_Free(me->gbMem,me->gbWidthTable);
    }

    if(me->gbHoriOffTable)
    {
        GreyBit_Free(me->gbMem,me->gbHoriOffTable);
    }

    if(me->gbOffsetTable)
    {
        GreyBit_Free(me->gbMem,me->gbOffsetTable);
    }

    if(me->gpGreyBits)
    {
        GB_INT32 i;
        for(i=0;i<me->nCacheItem;i++)
        {
            if(me->gpGreyBits[i])
            {
                GreyBitType_Outline_Done(me->gbLibrary, me->gpGreyBits[i]);
            }
        }
        GreyBit_Free(me->gbMem,me->gpGreyBits);
    }
}

static int GreyVectorFile_Decoder_Init(GVF_Decoder me)
{
    int nRet;
    int nDataSize;

    nRet = GreyVectorFile_Decoder_ReadHeader(me);
    if(nRet < 0)
    {
        return nRet;
    }

    // Cache Width
    nDataSize = me->gbInfoHeader.gbiHoriOffTabOff-me->gbInfoHeader.gbiWidthTabOff;
    me->gbWidthTable = (GVF_Width *)GreyBit_Malloc(me->gbMem, nDataSize);
    GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(me->gbOffDataBits+me->gbInfoHeader.gbiWidthTabOff));
    GreyBit_Stream_Read(me->gbStream, (GB_BYTE*)me->gbWidthTable, nDataSize);
    
    // Hori Offset
    nDataSize = me->gbInfoHeader.gbiOffsetTabOff-me->gbInfoHeader.gbiHoriOffTabOff;
    me->gbHoriOffTable = (GVF_Width *)GreyBit_Malloc(me->gbMem, nDataSize);
    GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(me->gbOffDataBits+me->gbInfoHeader.gbiHoriOffTabOff));
    GreyBit_Stream_Read(me->gbStream, (GB_BYTE*)me->gbHoriOffTable, nDataSize);
    
    // Cache Index
    nDataSize = me->gbInfoHeader.gbiOffGreyBits-me->gbInfoHeader.gbiOffsetTabOff;
    me->gbOffsetTable = (GVF_Offset *)GreyBit_Malloc(me->gbMem, nDataSize);
    GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(me->gbOffDataBits+me->gbInfoHeader.gbiOffsetTabOff));
    GreyBit_Stream_Read(me->gbStream, (GB_BYTE *)me->gbOffsetTable, nDataSize);
    return 0;
}

static int GreyVectorFile_Decoder_ReadHeader(GVF_Decoder me)
{
    GreyBit_Stream_Seek(me->gbStream, 0);
    if(GreyBit_Stream_Read(me->gbStream, (GB_BYTE *)&me->gbFileHeader, sizeof(GREYVECTORFILEHEADER)) != sizeof(GREYVECTORFILEHEADER))
    {
        return -1;
    }
    
    GreyBit_Stream_Read(me->gbStream, (GB_BYTE *)&me->gbInfoHeader, sizeof(GREYVECTORINFOHEADER));
    me->nItemCount = me->gbInfoHeader.gbiCount;
    GreyVectorFile_Decoder_InfoInit(me, me->gbInfoHeader.gbiWidth, me->gbInfoHeader.gbiHeight, me->gbInfoHeader.gbiMaxPoints, me->gbInfoHeader.gbiMaxContours);
    return 0;
}

static int GreyVectorFile_Decoder_InfoInit(GVF_Decoder me, GB_INT16 nMaxWidth, GB_INT16 nHeight, GB_INT16 nMaxPoints, GB_INT16 nMaxContours)
{
    me->gbOutline = GreyBitType_Outline_New(me->gbLibrary, nMaxContours, nMaxPoints);
    me->nBuffSize= sizeof(GVF_OutlineRec)+GreyVector_Outline_GetSizeEx((GB_BYTE)nMaxContours, (GB_BYTE)nMaxPoints);
    me->pBuff    = (GB_BYTE *)GreyBit_Malloc(me->gbMem, me->nBuffSize);
    return 0;
}

static GB_INT16 GreyVectorFile_Decoder_GetHoriOff(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize)
{
    GVF_Decoder me = (GVF_Decoder)decoder;
    GVF_Index   HoriOffIdx;
    GB_INT32    UniIndex;
    GB_UINT16   nMinCode;
    GVF_HoriOff nHoriOff;

    UniIndex = UnicodeSection_GetIndex((GB_UINT16)nCode);
    if(UniIndex >= UNICODE_SECTION_NUM)
    {
        return GB_HORIOFF_DEFAULT;
    }

    HoriOffIdx = me->gbInfoHeader.gbiWidthSection.gbSectionOff[UniIndex];
    if(HoriOffIdx == 0)
    {
        return GB_HORIOFF_DEFAULT;
    }

    HoriOffIdx--;
    if(me->gbHoriOffTable) // 已读入到内存中
    {
        UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
        nHoriOff = me->gbHoriOffTable[HoriOffIdx+(nCode-nMinCode)];
    }
    else
    {
        UnicodeSection_GetSectionInfo(UniIndex, &nMinCode, 0);
        HoriOffIdx += (GVF_Index)(nCode-nMinCode);
        GreyBit_Stream_Seek(me->gbStream, (GB_INT32)(HoriOffIdx*sizeof(GVF_HoriOff)+me->gbOffDataBits+me->gbInfoHeader.gbiHoriOffTabOff));
        GreyBit_Stream_Read(me->gbStream, (GB_BYTE*)&nHoriOff, sizeof(GVF_HoriOff));
    }
    nHoriOff = nHoriOff*nSize/me->gbInfoHeader.gbiHeight;
    return nHoriOff;
}

#endif

