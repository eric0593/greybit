#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"
#include "GreyBitRaster.h"

//-----------------------------------------------------------------------------
// Local Functions Declare
//-----------------------------------------------------------------------------
static int       GreyBitType_Layout_Bold(GB_Layout layout);
static int       GreyBitType_Layout_Italic(GB_Layout layout);
static void *    GreyBitType_Bitmap_SwitcBuffer(GB_Bitmap bitmap, void *pNewBuf);
static int       GreyBitType_Layout_ScaleBitmap(GB_Bitmap dst, GB_Bitmap src);

//-----------------------------------------------------------------------------
// Layout
//-----------------------------------------------------------------------------
GBHANDLE GreyBitType_Layout_New(GBHANDLE loader, GB_INT16 nSize, GB_INT16 nBitCount, GB_BOOL bBold, GB_BOOL bItalic)
{
    GB_Loader gbLoader = (GB_Loader)loader;
    GB_Layout me = (GB_Layout)GreyBit_Malloc(gbLoader->gbMem,sizeof(GB_LayoutRec));
	if(me)
	{
		me->gbLibrary       = gbLoader->gbLibrary;
		me->gbMem           = gbLoader->gbMem;
		me->gbStream        = gbLoader->gbStream;
		me->gbDecoder       = gbLoader->gbDecoder;
        me->dwCode          = (GB_UINT32)-1;
		me->nSize           = nSize;
		me->nBitCount       = nBitCount;
		me->bBold           = bBold;
		me->bItalic         = bItalic;
		me->gbBitmap        = GreyBitType_Bitmap_New(me->gbLibrary, nSize<<1, nSize, nBitCount, 0);
        if(nBitCount != 8)
        {
            me->gbBitmap8   = GreyBitType_Bitmap_New(me->gbLibrary, nSize<<1, nSize, 8, 0);
        }
        me->gbRaster        = GreyBit_Raster_New(me->gbLibrary, 0);
		me->nSwitchBufLen   = me->gbBitmap->pitch*me->gbBitmap->height;
		me->gbSwitchBuf     = (GB_BYTE *)GreyBit_Malloc(me->gbMem, me->nSwitchBufLen);
	}
	return me;
    
}

void GreyBitType_Layout_Done(GBHANDLE layout)
{
	GB_Layout me = (GB_Layout)layout;
	if(me->gbBitmap)
	{
		if(me->gbSwitchBuf)
		{
			GreyBit_Free(me->gbMem, me->gbSwitchBuf);
		}
		GreyBitType_Bitmap_Done(me->gbLibrary, me->gbBitmap);
	}
    if(me->gbBitmap8)
	{
		GreyBitType_Bitmap_Done(me->gbLibrary, me->gbBitmap8);
	}
    if(me->gbRaster)
	{
		GreyBit_Raster_Done(me->gbRaster);
	}
	GreyBit_Free(me->gbMem, me);
}

int GreyBitType_Layout_LoadChar(GBHANDLE layout, GB_UINT32 nCode, GB_Bitmap *pBmp)
{
	GB_Layout me = (GB_Layout)layout;
    GB_Bitmap bitmap;
    GB_DataRec data;
    int nRet;

	if(me->gbBitmap == 0)
	{
		return -1;
	}
    
    if(me->dwCode != nCode)
    {
        nRet = GreyBit_Decoder_Decode(me->gbDecoder, nCode, &data, me->nSize);
    	if(0 != nRet)
        {
            return -1;
        }

        if(data.format == GB_FORMAT_BITMAP)
        {
            bitmap = (GB_Bitmap)data.data;
        }
        else if(data.format == GB_FORMAT_OUTLINE)
        {
            if(me->gbBitmap8)
            {
                bitmap = me->gbBitmap8;
            }
            else
            {
                bitmap = me->gbBitmap;
            }
            bitmap->pitch   = bitmap->width = data.width;
            bitmap->horioff = data.horioff;
            GB_MEMSET(bitmap->buffer, 0, bitmap->pitch*bitmap->height);
            GreyBit_Raster_Render(me->gbRaster, bitmap, (GB_Outline)data.data);
        }
        else
        {
            return -1;
        }

        if(bitmap->bitcount == me->gbBitmap->bitcount && bitmap->height == me->gbBitmap->height)
        {
            me->gbBitmap->pitch     = bitmap->pitch;
            me->gbBitmap->width     = bitmap->width;
            me->gbBitmap->horioff   = bitmap->horioff;
            bitmap->buffer = (GB_BYTE *)GreyBitType_Bitmap_SwitcBuffer(me->gbBitmap, bitmap->buffer);
        }
        else
        {
            // Scale to me->gbBitmap from bitmap
            GreyBitType_Layout_ScaleBitmap(me->gbBitmap, bitmap);
        }
    	// Bold Glyph
    	if(me->bBold)
    	{
    		GreyBitType_Layout_Bold(me);
    	}
    	
    	// Italic Glyph
    	if(me->bItalic)
    	{
    		GreyBitType_Layout_Italic(me);
    	}

        // Set only success to Load
        me->dwCode = nCode;
    }

    if(pBmp)
	{
		*pBmp = me->gbBitmap;
	}
	return 0;
}

GB_INT32 GreyBitType_Layout_GetWidth(GBHANDLE layout, GB_UINT32 nCode)
{
	GB_Layout me = (GB_Layout)layout;
	return GreyBit_Decoder_GetAdvance(me->gbDecoder, nCode, me->nSize);
}

//-----------------------------------------------------------------------------
// Local Functions
//-----------------------------------------------------------------------------
static int GreyBitType_Layout_Bold(GB_Layout layout)
{
#ifdef ENABLE_BOLD
	GB_Layout me = (GB_Layout)layout;
	GB_Bitmap bitmap = me->gbBitmap;
	int x,y;
	GB_BYTE *pSrc = bitmap->buffer;
	GB_BYTE *pDst;
	GB_INT16 yMax = bitmap->height;
	GB_INT16 xMax;
	GB_INT16 nOff = bitmap->height>>5; // 1/16

	if(nOff == 0)
	{
		return 0;
	}
	else if(nOff > 4)
	{
		nOff = 4;
	}

	if(bitmap->bitcount == 8)
	{
		GB_INT16 nGrey;
		pDst = me->gbSwitchBuf+nOff;
		xMax = bitmap->pitch-nOff;

		GB_MEMCPY(me->gbSwitchBuf, bitmap->buffer, me->nSwitchBufLen);

		for(y=0;y<yMax;y++)
		{
			for(x=0;x<xMax;x++)
			{
				nGrey   = pDst[x]+pSrc[x];
				pDst[x] = (GB_BYTE)(nGrey>0xFF?0xFF:nGrey);
			}
			pSrc += bitmap->pitch;
			pDst += bitmap->pitch;
		}
	}
	else if(bitmap->bitcount == 1)
	{
		GB_BYTE bitMove;
		GB_INT16 nOff_R = 8-nOff;
		GB_BYTE OffMask = 0xFF>>nOff_R;
		pDst = me->gbSwitchBuf;
		xMax = bitmap->pitch;

		//GB_MEMCPY(me->gbSwitchBuf, bitmap->buffer, me->nSwitchBufLen);

		for(y=0;y<yMax;y++)
		{
			bitMove = 0;
			for(x=0;x<xMax;x++)
			{
				pDst[x] = (pSrc[x]>>nOff)|(bitMove<<nOff_R);
				bitMove = pSrc[x]&OffMask;
			}
			pSrc += bitmap->pitch;
			pDst += bitmap->pitch;
		}
	}
	else
	{
		// Not Support
		return -1;
	}
	me->gbSwitchBuf = (GB_BYTE *)GreyBitType_Bitmap_SwitcBuffer(bitmap, me->gbSwitchBuf);
#endif
	return 0;
}

static int GreyBitType_Layout_Italic(GB_Layout layout)
{
#ifdef ENABLE_ITALIC
	GB_Layout me = (GB_Layout)layout;
	GB_Bitmap bitmap = me->gbBitmap;
	int x,y;
	GB_BYTE *pSrc = bitmap->buffer;
	GB_BYTE *pDst;
	GB_INT16 yMax = bitmap->height;
	GB_INT16 xMax;
	GB_INT16 nOffMax = bitmap->height>>2; // 1/4
	GB_INT16 nHalfOffMax = nOffMax>>1;
	GB_INT16 nOff;

	if(nOffMax == 0)
	{
		return 0;
	}
	else if(nOffMax > 8)
	{
		nOffMax = 8;
	}

	if(bitmap->bitcount == 8)
	{
		GB_BYTE *pDstCurr;
		pDst = me->gbSwitchBuf;
		
		GB_MEMSET(me->gbSwitchBuf, 0, me->nSwitchBufLen);

		for(y=0;y<yMax;y++)
		{
			nOff = (y>>2)-nHalfOffMax;
			
			if(nOff < 0)
			{
				// 超出行头
				pDstCurr = pDst+nOff;
				xMax = bitmap->pitch;
				for(x=-nOff;x<xMax;x++)
				{
					pDstCurr[x] = pSrc[x];
				}
			}
			else
			{
				pDstCurr = pDst+nOff;
				xMax = bitmap->pitch-nOff;
				for(x=0;x<xMax;x++)
				{
					pDstCurr[x] = pSrc[x];
				}
			}
			
			pSrc += bitmap->pitch;
			pDst += bitmap->pitch;
		}
	}
	else if(bitmap->bitcount == 1)
	{
		GB_BYTE bitMove;
		GB_INT16 nOff_R;
		GB_BYTE OffMask;
		pDst = me->gbSwitchBuf;
		xMax = bitmap->pitch;

		//GB_MEMCPY(me->gbSwitchBuf, bitmap->buffer, me->nSwitchBufLen);

		for(y=0;y<yMax;y++)
		{
			nOff = (y>>2)-nHalfOffMax;
			bitMove = 0;
			if(nOff < 0)
			{
				nOff_R = 8+nOff;
				OffMask = 0xFF<<nOff_R;
				xMax--;
				for(x=0;x<xMax;x++)
				{
					bitMove = pSrc[x+1]&OffMask;
					pDst[x] = (pSrc[x]<<nOff)|(bitMove>>nOff_R);
				}
				pDst[x] = (pSrc[x]<<nOff);
			}
			else
			{
				nOff_R = 8-nOff;
				OffMask = 0xFF>>nOff_R;
				for(x=0;x<xMax;x++)
				{
					pDst[x] = (pSrc[x]>>nOff)|(bitMove<<nOff_R);
					bitMove = pSrc[x]&OffMask;
				}
			}
			pSrc += bitmap->pitch;
			pDst += bitmap->pitch;
		}
	}
	else
	{
		// Not Support
		return -1;
	}
	me->gbSwitchBuf = (GB_BYTE *)GreyBitType_Bitmap_SwitcBuffer(bitmap, me->gbSwitchBuf);
#endif
	return 0;
}

static void *GreyBitType_Bitmap_SwitcBuffer(GB_Bitmap bitmap, void *pNewBuf)
{
	void *pBuf = bitmap->buffer;
	bitmap->buffer = (GB_BYTE *)pNewBuf;
	return pBuf;
}


#define MATH_FACTOR_BIT         10
static void GreyBitType_Layout_BMP8Scale8(GB_BYTE *pBitsDst, GB_INT16 wWidthDst, GB_INT16 wHeightDst, GB_INT16 nPitchDst,
                                          GB_BYTE *pBitsSrc, GB_INT16 wWidthSrc, GB_INT16 wHeightSrc, GB_INT16 nPitchSrc)
{
    //第一步, 进行参数合法性检测
    //宽度缩放比
    GB_UINT32 fScalex =(wWidthSrc<<MATH_FACTOR_BIT) / wWidthDst;
    GB_UINT32 fScaley =(wHeightSrc<<MATH_FACTOR_BIT) / wHeightDst;
    
    //指向目标数据
    GB_BYTE* pbyDst = pBitsDst;
    GB_BYTE* pbySrc;
    int i,j;
    GB_UINT32 xx,yy;
    
    //完成变换
    for( i = 0; i < wHeightDst;i++)
    {
        //取整
        yy = (i * fScaley)>>MATH_FACTOR_BIT;
        pbySrc = pBitsSrc + yy * nPitchSrc;
        
        for( j = 0;j < wWidthDst;j++)
        {
            //取整
            xx = (j * fScalex)>>MATH_FACTOR_BIT;
            
            //获取数据
            *(pbyDst+j) = *(pbySrc + xx);
        }
        pbyDst += nPitchDst;
    }
}

static void GreyBitType_Layout_BMP1Scale1(GB_BYTE *pBitsDst, GB_INT16 wWidthDst, GB_INT16 wHeightDst, GB_INT16 nPitchDst,
                                          GB_BYTE *pBitsSrc, GB_INT16 wWidthSrc, GB_INT16 wHeightSrc, GB_INT16 nPitchSrc)
{
    //第一步, 进行参数合法性检测
    //宽度缩放比
    GB_UINT32 fScalex =(wWidthSrc<<MATH_FACTOR_BIT) / wWidthDst;
    GB_UINT32 fScaley =(wHeightSrc<<MATH_FACTOR_BIT) / wHeightDst;
    
    //指向目标数据
    GB_BYTE* pbyDst = pBitsDst;
    GB_BYTE* pbySrc;
    int i,j;
    GB_UINT32 xx,yy;
    
    //完成变换
    for( i = 0; i < wHeightDst;i++)
    {
        //取整
        for( j = 0;j < nPitchDst;j++)
        {
            *(pbyDst+j) = 0;
        }

        yy = (i * fScaley)>>MATH_FACTOR_BIT;
        pbySrc = pBitsSrc + yy * nPitchSrc;
        
        for( j = 0;j < wWidthDst;j++)
        {
            //取整
            xx = (j * fScalex)>>MATH_FACTOR_BIT;

            //获取数据
            *(pbyDst+(j>>3)) |= ((*(pbySrc + (xx>>3))>>( 7-(xx%8)))&0x1)<<(7-(j%8));
        }
        pbyDst += nPitchDst;
    }
}

static void GreyBitType_Layout_BMP8Scale1(GB_BYTE *pBitsDst, GB_INT16 wWidthDst, GB_INT16 wHeightDst, GB_INT16 nPitchDst,
                                          GB_BYTE *pBitsSrc, GB_INT16 wWidthSrc, GB_INT16 wHeightSrc, GB_INT16 nPitchSrc)
{
    //第一步, 进行参数合法性检测
    //宽度缩放比
    GB_UINT32 fScalex =(wWidthSrc<<MATH_FACTOR_BIT) / wWidthDst;
    GB_UINT32 fScaley =(wHeightSrc<<MATH_FACTOR_BIT) / wHeightDst;
    
    //指向目标数据
    GB_BYTE* pbyDst = pBitsDst;
    GB_BYTE* pbySrc;
    int i,j;
    GB_UINT32 xx,yy;
    
    //完成变换
    for( i = 0; i < wHeightDst;i++)
    {
        //取整
        for( j = 0;j < nPitchDst;j++)
        {
            *(pbyDst+j) = 0;
        }

        yy = (i * fScaley)>>MATH_FACTOR_BIT;
        pbySrc = pBitsSrc + yy * nPitchSrc;
        
        for( j = 0;j < wWidthDst;j++)
        {
            //取整
            xx = (j * fScalex)>>MATH_FACTOR_BIT;
            
            if((*(pbySrc + xx)) > BITMAP8TO1_SWITCH_VALUE)
            {
                //获取数据
                *(pbyDst+(j>>3)) |= 0x1<<(7-(j%8));
            }
        }
        pbyDst += nPitchDst;
    }
}

static void GreyBitType_Layout_BMP8To1(GB_BYTE *pBitsDst, GB_INT16 wWidthDst, GB_INT16 wHeightDst, GB_INT16 nPitchDst,
                                       GB_BYTE *pBitsSrc, GB_INT16 wWidthSrc, GB_INT16 wHeightSrc, GB_INT16 nPitchSrc)
{
    //指向目标数据
    GB_BYTE* pbyDst = pBitsDst;
    GB_BYTE* pbySrc = pBitsSrc;
    int i,j;
    
    //完成变换
    for( i = 0; i < wHeightDst;i++)
    {
        //取整
        for( j = 0;j < nPitchDst;j++)
        {
            *(pbyDst+j) = 0;
        }
        
        for( j = 0;j < wWidthDst;j++)
        {
            //获取数据
            if((*(pbySrc + j))>BITMAP8TO1_SWITCH_VALUE)
            {
                *(pbyDst+(j>>3)) |= 0x1<<(7-(j%8));
            }
        }
        pbyDst += nPitchDst;
        pbySrc += nPitchSrc;
    }
}

static void GreyBitType_Layout_BMP1Scale8(GB_BYTE *pBitsDst, GB_INT16 wWidthDst, GB_INT16 wHeightDst, GB_INT16 nPitchDst,
                                          GB_BYTE *pBitsSrc, GB_INT16 wWidthSrc, GB_INT16 wHeightSrc, GB_INT16 nPitchSrc)
{
    //第一步, 进行参数合法性检测
    //宽度缩放比
    GB_UINT32 fScalex =(wWidthSrc<<MATH_FACTOR_BIT) / wWidthDst;
    GB_UINT32 fScaley =(wHeightSrc<<MATH_FACTOR_BIT) / wHeightDst;
    
    //指向目标数据
    GB_BYTE* pbyDst = pBitsDst;
    GB_BYTE* pbySrc;
    int i,j;
    GB_UINT32 xx,yy;
    
    //完成变换
    for( i = 0; i < wHeightDst;i++)
    {
        //取整
        yy = (i * fScaley)>>MATH_FACTOR_BIT;
        pbySrc = pBitsSrc + yy * nPitchSrc;
        
        for( j = 0;j < wWidthDst;j++)
        {
            //取整
            xx = (j * fScalex)>>MATH_FACTOR_BIT;
            
            //获取数据
            *(pbyDst+j) = (((*(pbySrc + (xx>>3))>>( 7-(xx%8)))&0x1) == 0)?0:0xFF;
        }
        pbyDst += nPitchDst;
    }
}


static void GreyBitType_Layout_BMP1To8(GB_BYTE *pBitsDst, GB_INT16 wWidthDst, GB_INT16 wHeightDst, GB_INT16 nPitchDst,
                                       GB_BYTE *pBitsSrc, GB_INT16 wWidthSrc, GB_INT16 wHeightSrc, GB_INT16 nPitchSrc)
{
    //指向目标数据
    GB_BYTE* pbyDst = pBitsDst;
    GB_BYTE* pbySrc = pBitsSrc;
    int i,j;

    //完成变换
    for( i = 0; i < wHeightDst;i++)
    {
        for( j = 0;j < wWidthDst;j++)
        {
            //获取数据
            *(pbyDst+j) = (((*(pbySrc + (j>>3))>>( 7-(j%8)))&0x1) == 0)?0:0xFF;
        }
        pbyDst += nPitchDst;
        pbySrc += nPitchSrc;
    }
}

static int GreyBitType_Layout_ScaleBitmap(GB_Bitmap dst, GB_Bitmap src)
{
    if(dst->bitcount == src->bitcount)
    {
        if(dst->bitcount == 8)
        {
            dst->width      = (dst->height*src->width)/src->height;
            dst->horioff    = (dst->height*src->horioff)/src->height;
            dst->pitch      = dst->width;
            GreyBitType_Layout_BMP8Scale8(dst->buffer, dst->width, dst->height, dst->pitch, src->buffer, src->width, src->height, src->pitch);
        }
        else if(dst->bitcount == 1)
        {
            dst->width      = (dst->height*src->width)/src->height;
            dst->horioff    = (dst->height*src->horioff)/src->height;
            dst->pitch      = dst->width>>3;
            if(dst->pitch == 0)
            {
                dst->pitch = 1;
            }
            GreyBitType_Layout_BMP1Scale1(dst->buffer, dst->width, dst->height, dst->pitch, src->buffer, src->width, src->height, src->pitch);
        }
        else
        {
            // Not Support
        }
    }
    else
    {
        if(dst->bitcount == 8 && src->bitcount == 1)
        {
            dst->width      = (dst->height*src->width)/src->height;
            dst->horioff    = (dst->height*src->horioff)/src->height;
            dst->pitch      = dst->width;
            if(dst->height == src->height)
            {
                GreyBitType_Layout_BMP1To8(dst->buffer, dst->width, dst->height, dst->pitch, src->buffer, src->width, src->height, src->pitch);
            }
            else
            {
                GreyBitType_Layout_BMP1Scale8(dst->buffer, dst->width, dst->height, dst->pitch, src->buffer, src->width, src->height, src->pitch);
            }
        }
        else if(dst->bitcount == 1 && src->bitcount == 8)
        {
            dst->width      = (dst->height*src->width)/src->height;
            dst->horioff    = (dst->height*src->horioff)/src->height;
            dst->pitch      = dst->width>>3;
            if(dst->pitch == 0)
            {
                dst->pitch = 1;
            }
            if(dst->height == src->height)
            {
                GreyBitType_Layout_BMP8To1(dst->buffer, dst->width, dst->height, dst->pitch, src->buffer, src->width, src->height, src->pitch);
            }
            else
            {
                GreyBitType_Layout_BMP8Scale1(dst->buffer, dst->width, dst->height, dst->pitch, src->buffer, src->width, src->height, src->pitch);
            }
        }
        else
        {
            // Not Support
        }
    }
    return 0;
}
