#ifndef GREYVECTORFILE_H_
#define GREYVECTORFILE_H_
#include "GreyBitType.h"
#include "UnicodeSection.h"
#include "GreyBitType_Def.h"

#ifdef __cplusplus
extern "C"{
#endif

#define COMBINE_TAG
#ifdef COMBINE_TAG
#define TAG_MASK        0x1
#endif
#define MAX_COUNT       0x10000

// Compress
#define LEN_MASK        0x80
#define MAX_LEN()       (LEN_MASK-1)
#define GET_LEN(d)      (((d)&(~LEN_MASK))+1)
#define SET_LEN(d)      ((d)|LEN_MASK)
//#define SET_END()     (LEN_MASK)
#define IS_LEN(d)       ((d)&LEN_MASK)
//#define IS_END(d)     ((d) == LEN_MASK)

// RAM Index
#define RAM_MASK        0x80000000
#define IS_INRAM(d)     ((d)&RAM_MASK)
#define SET_RAM(d)      ((d)|RAM_MASK)
#define GET_INDEX(d)    ((d)&(~RAM_MASK))

// Unicode Section
typedef GB_INT8     GVF_Width;      // 宽度
typedef GB_INT8     GVF_HoriOff;    // 水平偏移
typedef GB_UINT32   GVF_Offset;     // 数据偏移
typedef GB_UINT16   GVF_Index;      // Section Index
typedef GB_UINT16   GVF_Lenght;     // DataLenght
typedef GB_BYTE     GVF_Pos;

typedef struct  _GVF_PointRec
{
    GVF_Pos     x;
    GVF_Pos     y;
} GVF_PointRec, *GVF_Point;

typedef struct  _GVF_OutlineRec
{
    GB_BYTE         n_contours;      /* number of contours in glyph        */
    GB_BYTE         n_points;        /* number of points in the glyph      */
    GVF_Point       points;          /* the outline's points               */
#ifndef COMBINE_TAG
    GB_BYTE        *tags;            /* the points flags                   */
#endif
    GB_BYTE        *contours;        /* the contour end points             */
} GVF_OutlineRec, *GVF_Outline;

// FileHeader+InfoHeader+SectionTable(Width)+SectionTable(Index)+Data(GreyBits)
typedef struct tagGREYVECTORFILEHEADER {
    char            gbfTag[4];    // gvtf
} GREYVECTORFILEHEADER, *PGREYVECTORFILEHEADER;

typedef struct tagSECTIONOINFO {
    GVF_Index       gbSectionOff[UNICODE_SECTION_NUM];        // Section Info 0为不可用
} SECTIONOINFO, *PSECTIONOINFO;

typedef struct tagGREYVECTORINFOHEADER {
    GB_UINT32       gbiSize;          // 结构体大小
    GB_UINT32       gbiCount;         // 包含文字个数
    GB_INT16        gbiMaxPoints;     // 字型中最大的Point数目
    GB_INT16        gbiMaxContours;   // 字型中最大的轮廓数目
    GB_INT16        gbiWidth;         // 最大宽度
    GB_INT16        gbiHeight;        // 字体高度
    GB_UINT32       gbiWidthTabOff;   // 宽度表在数据区的偏移
    GB_UINT32       gbiHoriOffTabOff; // 水平偏移表在数据区的偏移
    GB_UINT32       gbiOffsetTabOff;  // 检索表在数据区的偏移
    GB_UINT32       gbiOffGreyBits;   // 位图信息在数据区的偏移
    SECTIONOINFO    gbiWidthSection;  // 宽度Section信息
    SECTIONOINFO    gbiIndexSection;  // 字符偏移Section信息
} GREYVECTORINFOHEADER, *PGREYVECTORINFOHEADER;

// Decoder
typedef struct _GVF_DecoderRec{
    GB_DecoderRec           gbDecoder;
    GB_Library              gbLibrary;
    GB_Memory               gbMem;
    GB_Stream               gbStream;
    GB_Outline              gbOutline;
    GB_INT32                nCacheItem;
    GB_INT32                nItemCount;
    GB_BYTE                *pBuff;
    GB_INT32                nBuffSize;
    GB_UINT32               gbOffDataBits;  // 数据区相对于文件头的偏移
    GREYVECTORFILEHEADER    gbFileHeader;
    GREYVECTORINFOHEADER    gbInfoHeader;
    GVF_Width              *gbWidthTable;   // Width Cache      // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GVF_Width)，普通模式全部读入
    GVF_HoriOff            *gbHoriOffTable; // Hori Offset      // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GVF_HoriOff)，普通模式全部读入
    GVF_Offset             *gbOffsetTable;  // Index Cache      // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GVF_Index)，普通模式全部读入
    GB_Outline             *gpGreyBits;     // GreyBits Cache   // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GB_BYTE *)，普通模式按照nCacheItem缓存
    GB_INT32                nGreyBitsCount;
}GVF_DecoderRec,*GVF_Decoder;

extern GB_Decoder   GreyVectorFile_Decoder_New(GB_Loader loader, GB_Stream stream);
extern int          GreyVectorFile_Decoder_SetParam(GB_Decoder decoder, GB_Param nParam, GB_UINT32 dwParam);
extern GB_INT32     GreyVectorFile_Decoder_GetCount(GB_Decoder decoder);
extern GB_INT32     GreyVectorFile_Decoder_GetHeight(GB_Decoder decoder);
extern GB_INT16     GreyVectorFile_Decoder_GetWidth(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize);
extern GB_INT16     GreyVectorFile_Decoder_GetAdvance(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize);
extern int          GreyVectorFile_Decoder_Decode(GB_Decoder decoder, GB_UINT32 nCode, GB_Data pData, GB_INT16 nSize);
extern void         GreyVectorFile_Decoder_Done(GB_Decoder decoder);

#ifdef ENABLE_ENCODER
typedef struct _GVF_EncoderRec{
    GB_EncoderRec           gbEncoder;
    GB_Library              gbLibrary;
    GB_Memory               gbMem;
    GB_Stream               gbStream;
    GB_UINT16               gbHeight;
    GB_INT32                nCacheItem;
    GB_INT32                nItemCount;
    GB_UINT32               gbOffDataBits;  // 数据区相对于文件头的偏移
    GREYVECTORFILEHEADER    gbFileHeader;
    GREYVECTORINFOHEADER    gbInfoHeader;
    GVF_Width              *gbWidthTable;   // Width Cache      // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GVF_Width)，普通模式全部读入
    GVF_HoriOff            *gbHoriOffTable; // Hori Offset      // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GVF_HoriOff)，普通模式全部读入
    GVF_Offset             *gbOffsetTable;  // Index Cache      // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GVF_Index)，普通模式全部读入
    GB_Outline             *gpGreyBits;     // GreyBits Cache   // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GB_BYTE *)，普通模式按照nCacheItem缓存
    GVF_Lenght             *pnGreySize;
}GVF_EncoderRec,*GVF_Encoder;

extern GB_Encoder   GreyVectorFile_Encoder_New(GB_Creator creator, GB_Stream stream);
extern GB_INT32     GreyVectorFile_Encoder_GetCount(GB_Encoder encoder);
extern int          GreyVectorFile_Encoder_SetParam(GB_Encoder encoder, GB_Param nParam, GB_UINT32 dwParam);
extern int          GreyVectorFile_Encoder_Delete(GB_Encoder encoder, GB_UINT32 nCode);
extern int          GreyVectorFile_Encoder_Encode(GB_Encoder encoder, GB_UINT32 nCode, GB_Data pData);
extern int          GreyVectorFile_Encoder_Flush(GB_Encoder encoder);
extern void         GreyVectorFile_Encoder_Done(GB_Encoder encoder);
#endif // #ifdef ENABLE_ENCODER

extern GVF_Outline  GreyVector_Outline_New(GBHANDLE library, GB_BYTE n_contours, GB_BYTE n_points);
extern GVF_Outline  GreyVector_Outline_Clone(GBHANDLE library, GVF_Outline source);
extern GVF_Outline  GreyVector_Outline_FromData(GB_BYTE *pData);
extern GB_BYTE*     GreyVector_Outline_GetData(GVF_Outline outline);
extern GB_INT32     GreyVector_Outline_GetSize(GVF_Outline outline);
extern GB_INT32     GreyVector_Outline_GetSizeEx(GB_BYTE n_contours, GB_BYTE n_points);
extern void         GreyVector_Outline_Done(GBHANDLE library, GVF_Outline outline);
extern GVF_Outline  GreyVector_Outline_NewByGB(GBHANDLE library, GB_Outline source);
extern GB_Outline   GreyBitType_Outline_NewByGVF(GBHANDLE library, GVF_Outline source);
extern GB_Outline   GreyBitType_Outline_UpdateByGVF(GB_Outline outline, GVF_Outline source);

#ifdef __cplusplus
}
#endif 
#endif
