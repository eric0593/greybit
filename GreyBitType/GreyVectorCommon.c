#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyVectorFile.h"

//-----------------------------------------------------------------------------
// Outline
//-----------------------------------------------------------------------------
GVF_Outline GreyVector_Outline_New(GBHANDLE library, GB_BYTE n_contours, GB_BYTE n_points)
{
    GB_Library gbLib = (GB_Library)library;
#ifdef COMBINE_TAG
    GB_INT32 nSize = sizeof(GVF_OutlineRec)+sizeof(GB_BYTE)+sizeof(GB_BYTE)+n_contours*sizeof(GB_BYTE)+n_points*(sizeof(GVF_PointRec));
#else
    GB_INT32 nSize = sizeof(GVF_OutlineRec)+sizeof(GB_BYTE)+sizeof(GB_BYTE)+n_contours*sizeof(GB_BYTE)+n_points*(sizeof(GVF_PointRec)+sizeof(GB_BYTE));
#endif
    GVF_Outline outline = (GVF_Outline)GreyBit_Malloc(gbLib->gbMem, nSize);
    if(outline)
    {
        GB_BYTE *pBase = (GB_BYTE *)outline;

        outline->n_contours = n_contours;
        outline->n_points   = n_points;

        pBase += sizeof(GVF_OutlineRec);
        *pBase = n_contours;
        pBase += 1;
        *pBase = n_points;
        pBase += 1;
        outline->contours   = pBase;

        pBase += n_contours*sizeof(GB_BYTE);
        outline->points     = (GVF_Point)pBase;
#ifndef COMBINE_TAG
        pBase += n_points*sizeof(GVF_PointRec);
        outline->tags       = pBase;
#endif
    }
    return outline;
}

GVF_Outline GreyVector_Outline_Clone(GBHANDLE library, GVF_Outline source)
{
    GVF_Outline outline = GreyVector_Outline_New(library, source->n_contours, source->n_points);
    if(outline)
    {
        int i;
        for(i=0;i<outline->n_contours;i++)
        {
            outline->contours[i] = source->contours[i];
        }

        for(i=0;i<outline->n_points;i++)
        {
            outline->points[i].x = source->points[i].x;
            outline->points[i].y = source->points[i].y;
#ifndef COMBINE_TAG
            outline->tags[i]     = source->tags[i];
#endif
        }
    }
    return outline;
}
GB_BYTE *GreyVector_Outline_GetData(GVF_Outline outline)
{
    GB_BYTE *pBase = (GB_BYTE *)outline;
    return (pBase+sizeof(GVF_OutlineRec));
}

GB_INT32 GreyVector_Outline_GetSize(GVF_Outline outline)
{
    return GreyVector_Outline_GetSizeEx(outline->n_contours, outline->n_points);
}

GB_INT32 GreyVector_Outline_GetSizeEx(GB_BYTE n_contours, GB_BYTE n_points)
{
#ifdef COMBINE_TAG
    return (sizeof(GB_BYTE)+sizeof(GB_BYTE)+n_contours*sizeof(GB_BYTE)+n_points*(sizeof(GVF_PointRec)));
#else
    return (sizeof(GB_BYTE)+sizeof(GB_BYTE)+n_contours*sizeof(GB_BYTE)+n_points*(sizeof(GVF_PointRec)+sizeof(GB_BYTE)));
#endif
}

GVF_Outline GreyVector_Outline_FromData(GB_BYTE *pData)
{
    GVF_Outline outline;
    GB_INT32 offset = 0;

    outline = (GVF_Outline)pData;
    offset += sizeof(GVF_OutlineRec);
    outline->n_contours = pData[offset];
    offset += 1;
    outline->n_points   = pData[offset];
    offset += 1;
    outline->contours   = &pData[offset];
    offset += outline->n_contours;
    outline->points     = (GVF_Point)&pData[offset];
#ifndef COMBINE_TAG
    offset += outline->n_points*sizeof(GVF_PointRec);
    outline->tags       = &pData[offset];
#endif
    return outline;
}

void GreyVector_Outline_Done(GBHANDLE library, GVF_Outline outline)
{
    GB_Library gbLib = (GB_Library)library;
    //GreyBit_Free(gbLib->gbMem,outline->contours);
    //GreyBit_Free(gbLib->gbMem,outline->points);
    //GreyBit_Free(gbLib->gbMem,outline->tags);
    GreyBit_Free(gbLib->gbMem,outline);
}

GVF_Outline GreyVector_Outline_NewByGB(GBHANDLE library, GB_Outline source)
{
    GVF_Outline outline = GreyVector_Outline_New(library, (GB_BYTE)source->n_contours, (GB_BYTE)source->n_points);
    if(outline)
    {
        int i;
        for(i=0;i<outline->n_contours;i++)
        {
            outline->contours[i] = (GB_BYTE)source->contours[i];
        }

        for(i=0;i<outline->n_points;i++)
        {
            outline->points[i].x = (GVF_Pos)(source->points[i].x>>6);
            outline->points[i].y = (GVF_Pos)(source->points[i].y>>6);
#ifdef COMBINE_TAG
            outline->points[i].x = (outline->points[i].x<<1)|(source->tags[i]&TAG_MASK);
            outline->points[i].y = (outline->points[i].y<<1)|((source->tags[i]>>1)&TAG_MASK);
#else
            outline->tags[i]     = source->tags[i];
#endif
        }
    }
    return outline;
}

GB_Outline GreyBitType_Outline_NewByGVF(GBHANDLE library, GVF_Outline source)
{
    GB_Outline outline = GreyBitType_Outline_New(library, source->n_contours, source->n_points);
    if(outline)
    {
        int i;
        for(i=0;i<outline->n_contours;i++)
        {
            outline->contours[i] = source->contours[i];
        }

        for(i=0;i<outline->n_points;i++)
        {
            outline->points[i].x = (source->points[i].x>>1)<<6;
            outline->points[i].y = (source->points[i].y>>1)<<6;
#ifdef COMBINE_TAG
            outline->tags[i]     = (source->points[i].x&TAG_MASK)|((source->points[i].y&TAG_MASK)<<1);
#else
            outline->tags[i]     = source->tags[i];
#endif
        }
    }
    return outline;
}

GB_Outline GreyBitType_Outline_UpdateByGVF(GB_Outline outline, GVF_Outline source)
{
    int i;

    outline->n_contours = source->n_contours;
    outline->n_points   = source->n_points;
    for(i=0;i<outline->n_contours;i++)
    {
        outline->contours[i] = source->contours[i];
    }

    for(i=0;i<outline->n_points;i++)
    {
        outline->points[i].x = (source->points[i].x>>1)<<6;
        outline->points[i].y = (source->points[i].y>>1)<<6;
#ifdef COMBINE_TAG
        outline->tags[i]     = (source->points[i].x&TAG_MASK)|((source->points[i].y&TAG_MASK)<<1);
#else
        outline->tags[i]     = source->tags[i];
#endif
    }
    return outline;
}
