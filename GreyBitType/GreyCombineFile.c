#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"
#ifdef ENABLE_GREYCOMBINEFILE
#include "GreyCombineFile.h"

//-----------------------------------------------------------------------------
// Format
//-----------------------------------------------------------------------------
GB_BOOL GreyCombineFile_Probe(GB_Stream stream)
{
    GREYCOMBINEFILEHDR fileHeader;
    GreyBit_Stream_Seek(stream, 0);
    if(GreyBit_Stream_Read(stream, (GB_BYTE *)&fileHeader, sizeof(GREYCOMBINEFILEHDR)) == sizeof(GREYCOMBINEFILEHDR))
    {
        if( fileHeader.gbfTag[0] == 'g' 
          &&fileHeader.gbfTag[1] == 'c'
          &&fileHeader.gbfTag[2] == 't' 
          &&fileHeader.gbfTag[3] == 'f' )
        {
            return GB_TRUE;
        }
    }
    return GB_FALSE;
}

GB_Format GreyCombineFile_Format_New(GB_Library library)
{
    GB_Format format = (GB_Format)GreyBit_Malloc(library->gbMem,sizeof(GB_FormatRec));
    if(format)
    {
        format->next        = 0;
        format->tag[0]      = 'g';
        format->tag[1]      = 'c';
        format->tag[2]      = 'f';
        format->probe       = GreyCombineFile_Probe;
        format->decodernew  = GreyCombineFile_Decoder_New;
#ifdef ENABLE_ENCODER
        format->encodernew  = GreyCombineFile_Encoder_New;
#endif
    }
    return format;
}
#endif

