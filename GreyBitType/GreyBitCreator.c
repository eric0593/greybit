#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"

#ifdef ENABLE_ENCODER
//-----------------------------------------------------------------------------
// Local Functions Declare
//-----------------------------------------------------------------------------
static GB_Encoder  GreyBitType_Creator_Probe(GB_Library library, GB_Creator creator);

//-----------------------------------------------------------------------------
// Creator
//-----------------------------------------------------------------------------
GBHANDLE GreyBitType_Creator_New(GBHANDLE library, const char* filepathname)
{
    GB_Library gbLib = (GB_Library)library;
    GB_Creator me = (GB_Creator)GreyBit_Malloc(gbLib->gbMem,sizeof(GB_CreatorRec));
    if(me)
    {
        me->gbLibrary   = gbLib;
        me->gbMem       = gbLib->gbMem;
        me->gbStream    = GreyBit_Stream_New(filepathname, 1);
        
        if(me->gbStream)
        {
            me->gbEncoder  = GreyBitType_Creator_Probe(gbLib, me);
        }
        
        if(me->gbStream == 0 || me->gbEncoder == 0)
        {
            GreyBitType_Creator_Done(me);
            return 0;
        }
    }
    return me;
}

void GreyBitType_Creator_Done(GBHANDLE creator)
{
    GB_Creator me = (GB_Creator)creator;
    
    if(me->gbEncoder)
    {
        GreyBit_Encoder_Done(me->gbEncoder);
    }
    
    if(me->gbStream)
    {
        GreyBit_Stream_Done(me->gbStream);
    }
    GreyBit_Free(me->gbMem, me);
}


int GreyBitType_Creator_SetParam(GBHANDLE creator, GB_Param nParam, GB_UINT32 dwParam)
{
    GB_Creator me = (GB_Creator)creator;
    return GreyBit_Encoder_SetParam(me->gbEncoder, nParam, dwParam);
}

int GreyBitType_Creator_DelChar(GBHANDLE creator, GB_UINT32 nCode)
{
    GB_Creator me = (GB_Creator)creator;
    return GreyBit_Encoder_Delete(me->gbEncoder, nCode);
}

int GreyBitType_Creator_SaveChar(GBHANDLE creator, GB_UINT32 nCode, GB_Data pData)
{
    GB_Creator me = (GB_Creator)creator;
    return GreyBit_Encoder_Encode(me->gbEncoder, nCode, pData);
}

int GreyBitType_Creator_Flush(GBHANDLE creator)
{
    GB_Creator me = (GB_Creator)creator;
    return GreyBit_Encoder_Flush(me->gbEncoder);
}

//-----------------------------------------------------------------------------
// Local Functions
//-----------------------------------------------------------------------------
static GB_Encoder GreyBitType_Creator_Probe(GB_Library library, GB_Creator creator)
{
    GB_Format format = library->gbFormatHeader;
    GB_Encoder  encoder  = 0;
    while(format)
    {
        if(GreyBit_Format_Probe(format,creator->gbStream))
        {
            encoder = GreyBit_Format_EncoderNew(format, creator, creator->gbStream);
            break;
        }
        else
        {
            // 文件扩展名相同
            char *pExt = GB_STRRCHR(creator->gbStream->pfilename, '.');
            if(pExt)
            {
                pExt++;
                if(GB_STRCMP(pExt, format->tag) == 0)
                {
                    encoder = GreyBit_Format_EncoderNew(format, creator, creator->gbStream);
                    break;
                }
            }
        }
        format = format->next;
    }
    return encoder;
}
#endif
