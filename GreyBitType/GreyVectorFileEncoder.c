#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"

#ifdef ENABLE_GREYVECTORFILE
#ifdef ENABLE_ENCODER
#include "GreyVectorFile.h"

/*************************************************************************/
/*                                                                       */
/*                              Struct                                   */
/*                                                                       */
/*************************************************************************/
static int GreyVectorFile_Encoder_Init(GVF_Encoder me);
static int GreyVectorFile_Encoder_BuildAll(GVF_Encoder me);
static int GreyVectorFile_Encoder_WriteAll(GVF_Encoder me);
static int GreyVectorFile_Encoder_InfoInit(GVF_Encoder me, GB_INT16 nHeight);

static void GreyVectorFile_Encoder_ClearCache(GVF_Encoder me);

//-----------------------------------------------------------------------------
// Encoder
//-----------------------------------------------------------------------------
GB_Encoder GreyVectorFile_Encoder_New(GB_Creator creator, GB_Stream stream)
{
    GVF_Encoder codec = (GVF_Encoder)GreyBit_Malloc(creator->gbMem,sizeof(GVF_EncoderRec));
    if(codec)
    {
        codec->gbEncoder.getcount       = GreyVectorFile_Encoder_GetCount;
        codec->gbEncoder.setparam       = GreyVectorFile_Encoder_SetParam;
        codec->gbEncoder.remove         = GreyVectorFile_Encoder_Delete;
        codec->gbEncoder.encode         = GreyVectorFile_Encoder_Encode;
        codec->gbEncoder.flush          = GreyVectorFile_Encoder_Flush;
        codec->gbEncoder.done           = GreyVectorFile_Encoder_Done;
        codec->gbLibrary                = creator->gbLibrary;
        codec->gbMem                    = creator->gbMem;
        codec->gbStream                 = stream;
        codec->nCacheItem	            = 0;
        codec->nItemCount               = 0;
        codec->gbHeight                 = 127;
        codec->gbOffDataBits            = sizeof(GREYVECTORFILEHEADER)+sizeof(GREYVECTORINFOHEADER);
        GreyVectorFile_Encoder_Init(codec);
    }
    return (GB_Encoder)codec;
}

GB_INT32 GreyVectorFile_Encoder_GetCount(GB_Encoder codec)
{
    GVF_Encoder me = (GVF_Encoder)codec;
    return me->nItemCount;
}

int	GreyVectorFile_Encoder_SetParam(GB_Encoder codec, GB_Param nParam, GB_UINT32 dwParam)
{
    GVF_Encoder me = (GVF_Encoder)codec;

    switch(nParam){
    case GB_PARAM_HEIGHT:
        me->gbHeight = (GB_UINT16)dwParam;
        break;
        
    default:
        return -1;
    }
    
    GreyVectorFile_Encoder_InfoInit(me, me->gbHeight);
    return 0;
}

int	GreyVectorFile_Encoder_Delete(GB_Encoder codec, GB_UINT32 nCode)
{
    GVF_Encoder me = (GVF_Encoder)codec;

    me->gbOffsetTable[nCode] = 0;
    me->gbWidthTable[nCode] = GB_WIDTH_DEFAULT;
    me->gbHoriOffTable[nCode] = GB_HORIOFF_DEFAULT;
    if(me->gpGreyBits[nCode])
    {
        GreyBitType_Outline_Done(me->gbLibrary, me->gpGreyBits[nCode]);
        me->gpGreyBits[nCode] = 0;
        me->pnGreySize[nCode] = 0;
    }
    return 0;
}

int	GreyVectorFile_Encoder_Encode(GB_Encoder codec, GB_UINT32 nCode, GB_Data pData)
{
    GVF_Encoder me = (GVF_Encoder)codec;
    GB_Outline source;
    GB_Outline outline;
    GB_INT16   nWidth;
    
    if(!pData || pData->format != GB_FORMAT_OUTLINE)
    {
        return -1;
    }

    nWidth = pData->width;
    source = (GB_Outline)pData->data;
    if(source->n_points > 0xFF)
    {
        return -2;
    }
    
    if(me->gbInfoHeader.gbiWidth<nWidth)
    {
        me->gbInfoHeader.gbiWidth = nWidth;
    }

    if(me->gbInfoHeader.gbiMaxPoints<source->n_points)
    {
        me->gbInfoHeader.gbiMaxPoints = source->n_points;
    }

    if(me->gbInfoHeader.gbiMaxContours<source->n_contours)
    {
        me->gbInfoHeader.gbiMaxContours = source->n_contours;
    }

    outline = GreyBitType_Outline_Clone(me->gbLibrary, source);

    if(me->gpGreyBits[nCode])
    {
        GreyBitType_Outline_Done(me->gbLibrary, me->gpGreyBits[nCode]);
    }

    me->gpGreyBits[nCode]     = outline;
    me->pnGreySize[nCode]     = (GVF_Lenght)GreyVector_Outline_GetSizeEx((GB_BYTE)outline->n_contours, (GB_BYTE)outline->n_points);
    me->gbOffsetTable[nCode]  = SET_RAM(nCode);
    me->gbWidthTable[nCode]   = (GVF_Width)nWidth;
    me->gbHoriOffTable[nCode] = (GVF_HoriOff)pData->horioff;
    return 0;
}

int	GreyVectorFile_Encoder_Flush(GB_Encoder codec)
{
    GVF_Encoder me = (GVF_Encoder)codec;

    GreyVectorFile_Encoder_BuildAll(me);
    GreyVectorFile_Encoder_WriteAll(me);
    return 0;
}

void GreyVectorFile_Encoder_Done(GB_Encoder codec)
{
    GVF_Encoder me = (GVF_Encoder)codec;

    GreyVectorFile_Encoder_ClearCache(me);
    GreyBit_Free(me->gbMem,codec);
}

//-----------------------------------------------------------------------------
// Local function
//-----------------------------------------------------------------------------
static void GreyVectorFile_Encoder_ClearCache(GVF_Encoder me)
{
    if(me->gbWidthTable)
    {
        GreyBit_Free(me->gbMem,me->gbWidthTable);
    }
    
    if(me->gbHoriOffTable)
    {
        GreyBit_Free(me->gbMem,me->gbHoriOffTable);
    }

    if(me->gbOffsetTable)
    {
        GreyBit_Free(me->gbMem,me->gbOffsetTable);
    }

    if(me->pnGreySize)
    {
        GreyBit_Free(me->gbMem,me->pnGreySize);
    }

    if(me->gpGreyBits)
    {
        GB_INT32 i;
        for(i=0;i<me->nCacheItem;i++)
        {
            if(me->gpGreyBits[i])
            {
                GreyBitType_Outline_Done(me->gbLibrary, me->gpGreyBits[i]);
            }
        }
        GreyBit_Free(me->gbMem,me->gpGreyBits);
    }
}

static int GreyVectorFile_Encoder_Init(GVF_Encoder me)
{
    me->gbWidthTable   = (GVF_Width *)GreyBit_Malloc(me->gbMem, MAX_COUNT*sizeof(GVF_Width));
    me->gbHoriOffTable = (GVF_HoriOff *)GreyBit_Malloc(me->gbMem, MAX_COUNT*sizeof(GVF_HoriOff));
    me->gbOffsetTable  = (GVF_Offset *)GreyBit_Malloc(me->gbMem, MAX_COUNT*sizeof(GVF_Offset));
    me->gpGreyBits     = (GB_Outline *)GreyBit_Malloc(me->gbMem, MAX_COUNT*sizeof(GB_Outline));
    me->pnGreySize     = (GVF_Lenght *)GreyBit_Malloc(me->gbMem, MAX_COUNT*sizeof(GVF_Lenght));
    me->nCacheItem     = MAX_COUNT;
    GB_MEMSET(me->gbWidthTable,GB_WIDTH_DEFAULT,MAX_COUNT*sizeof(GVF_Width));
    GB_MEMSET(me->gbHoriOffTable,GB_HORIOFF_DEFAULT,MAX_COUNT*sizeof(GVF_HoriOff));
    return 0;
}

static int GreyVectorFile_Encoder_BuildAll(GVF_Encoder me)
{
    GB_INT32 nSection, nSectionLen;
    GB_UINT16 nMaxCode, nMinCode; 
    GB_INT32 nCode;
    GB_UINT32 nWidthTableSize = 0, nHoriOffTableSize = 0, nOffSetTableSize = 0, nGreyBitSize = 0, nCount = 0;
    GVF_Lenght nSize;
    // Width and Index Info
    for(nSection = 0; nSection < UNICODE_SECTION_NUM; nSection++)
    {
        UnicodeSection_GetSectionInfo(nSection, &nMinCode, &nMaxCode);
        nSectionLen = (nMaxCode-nMinCode+1);
        for(nCode = nMinCode; nCode <= nMaxCode; nCode++)
        {
            if(me->gbWidthTable[nCode] != GB_WIDTH_DEFAULT)
            {
                me->gbInfoHeader.gbiWidthSection.gbSectionOff[nSection] = (GVF_Index)(nWidthTableSize/sizeof(GVF_Width))+1;
                me->gbInfoHeader.gbiIndexSection.gbSectionOff[nSection] = (GVF_Index)(nOffSetTableSize/sizeof(GVF_Offset))+1;
                nWidthTableSize += nSectionLen*sizeof(GVF_Width);
                nHoriOffTableSize += nSectionLen*sizeof(GVF_HoriOff);
                nOffSetTableSize += nSectionLen*sizeof(GVF_Offset);
                break;
            }
        }
    }
    
    for(nCode = 0; nCode<MAX_COUNT; nCode++)
    {
        nSize = me->pnGreySize[nCode];
        if(nSize)
        {
            me->gbOffsetTable[nCode] = nGreyBitSize;
            nGreyBitSize += nSize+sizeof(GVF_Lenght);
            nCount++;
        }
    }

    me->gbInfoHeader.gbiCount = nCount;
    me->gbInfoHeader.gbiOffGreyBits  = nWidthTableSize+nHoriOffTableSize+nOffSetTableSize;
    me->gbInfoHeader.gbiOffsetTabOff = nWidthTableSize+nHoriOffTableSize;
    me->gbInfoHeader.gbiHoriOffTabOff= nWidthTableSize;
    me->gbInfoHeader.gbiWidthTabOff  = 0;
    me->gbInfoHeader.gbiSize         = sizeof(GREYVECTORINFOHEADER);
    GB_STRNCPY(me->gbFileHeader.gbfTag, "gvtf", GB_STRLEN("gvtf"));
    return 0;
}

static int GreyVectorFile_Encoder_WriteAll(GVF_Encoder me)
{
    GB_INT32 nSection, nSectionLen;
    GB_UINT16 nMaxCode, nMinCode; 
    GB_INT32 nCode;
    GB_BYTE *pData;
    GB_INT32 nDataSize;

    GreyBit_Stream_Seek(me->gbStream, 0);
    GreyBit_Stream_Write(me->gbStream, (GB_BYTE *)&me->gbFileHeader, sizeof(GREYVECTORFILEHEADER));
    GreyBit_Stream_Write(me->gbStream, (GB_BYTE *)&me->gbInfoHeader, sizeof(GREYVECTORINFOHEADER));

    // Width Info
    for(nSection = 0; nSection < UNICODE_SECTION_NUM; nSection++)
    {
        UnicodeSection_GetSectionInfo(nSection, &nMinCode, &nMaxCode);
        nSectionLen = (nMaxCode-nMinCode+1);
        
        if(me->gbInfoHeader.gbiWidthSection.gbSectionOff[nSection] != 0)
        {
            pData = &me->gbWidthTable[nMinCode];
            nDataSize = (GVF_Lenght)nSectionLen*sizeof(GVF_Width);
            GreyBit_Stream_Write(me->gbStream, pData, nDataSize);
        }
    }
    
    // Width Info
    for(nSection = 0; nSection < UNICODE_SECTION_NUM; nSection++)
    {
        UnicodeSection_GetSectionInfo(nSection, &nMinCode, &nMaxCode);
        nSectionLen = (nMaxCode-nMinCode+1);
        
        if(me->gbInfoHeader.gbiWidthSection.gbSectionOff[nSection] != 0)
        {
            pData = &me->gbHoriOffTable[nMinCode];
            nDataSize = (GVF_Lenght)nSectionLen*sizeof(GVF_HoriOff);
            GreyBit_Stream_Write(me->gbStream, pData, nDataSize);
        }
    }
    
    // Index Info
    for(nSection = 0; nSection < UNICODE_SECTION_NUM; nSection++)
    {
        UnicodeSection_GetSectionInfo(nSection, &nMinCode, &nMaxCode);
        nSectionLen = (nMaxCode-nMinCode+1);
        if(me->gbInfoHeader.gbiIndexSection.gbSectionOff[nSection] != 0)
        {
            pData = (GB_BYTE *)&me->gbOffsetTable[nMinCode];
            nDataSize = (GVF_Lenght)nSectionLen*sizeof(GVF_Offset);
            GreyBit_Stream_Write(me->gbStream, pData, nDataSize);
        }
    }
    
    // GeryBits
    for(nCode = 0; nCode<me->nCacheItem; nCode++)
    {
        nDataSize = me->pnGreySize[nCode];
        if(nDataSize)
        {
            GVF_Outline outline = GreyVector_Outline_NewByGB(me->gbLibrary, me->gpGreyBits[nCode]);
            // 写入数据的长度
            GreyBit_Stream_Write(me->gbStream, (GB_BYTE *)&nDataSize, sizeof(GVF_Lenght));
            pData = GreyVector_Outline_GetData(outline);
            GreyBit_Stream_Write(me->gbStream, pData, nDataSize);
            GreyVector_Outline_Done(me->gbLibrary, outline);
        }
    }
    return 0;
}

static int GreyVectorFile_Encoder_InfoInit(GVF_Encoder me, GB_INT16 nHeight)
{
    if(me->gbInfoHeader.gbiHeight == nHeight)
    {
        return 0;
    }
    else
    {
        // 清除原有信息，重新进行初始化
        int i;
        
        GB_MEMSET(me->gbWidthTable, GB_WIDTH_DEFAULT, MAX_COUNT*sizeof(GVF_Width));
        GB_MEMSET(me->gbHoriOffTable, GB_HORIOFF_DEFAULT, MAX_COUNT*sizeof(GVF_HoriOff));
        GB_MEMSET(me->gbOffsetTable, 0, MAX_COUNT*sizeof(GVF_Offset));
        for(i=0;i<me->nCacheItem;i++)
        {
            if(me->gpGreyBits[i])
            {
                GreyBitType_Outline_Done(me->gbLibrary, me->gpGreyBits[i]);
                me->gpGreyBits[i] = 0;
                me->pnGreySize[i] = 0;
            }
        }
    }
    me->gbInfoHeader.gbiHeight = nHeight;
    return 0;
}
#endif
#endif

