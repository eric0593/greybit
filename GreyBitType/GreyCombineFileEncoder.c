#include "GreyBitType.h"
#include "GreyBitSystem.h"
#include "GreyBitType_Def.h"

#ifdef ENABLE_GREYCOMBINEFILE
#ifdef ENABLE_ENCODER
#include "GreyCombineFile.h"

/*************************************************************************/
/*                                                                       */
/*                              Struct                                   */
/*                                                                       */
/*************************************************************************/
static int GreyCombineFile_Encoder_BuildAll(GCF_Encoder me);
static int GreyCombineFile_Encoder_WriteAll(GCF_Encoder me);

//-----------------------------------------------------------------------------
// Encoder
//-----------------------------------------------------------------------------
GB_Encoder GreyCombineFile_Encoder_New(GB_Creator creator, GB_Stream stream)
{
    GCF_Encoder codec = (GCF_Encoder)GreyBit_Malloc(creator->gbMem,sizeof(GCF_EncoderRec));
    if(codec)
    {
        codec->gbEncoder.getcount       = GreyCombineFile_Encoder_GetCount;
        codec->gbEncoder.setparam       = GreyCombineFile_Encoder_SetParam;
        codec->gbEncoder.remove         = GreyCombineFile_Encoder_Delete;
        codec->gbEncoder.encode         = GreyCombineFile_Encoder_Encode;
        codec->gbEncoder.flush          = GreyCombineFile_Encoder_Flush;
        codec->gbEncoder.done           = GreyCombineFile_Encoder_Done;
        codec->gbLibrary                = creator->gbLibrary;
        codec->gbMem                    = creator->gbMem;
        codec->gbStream                 = stream;
        GB_MEMSET(&codec->gbFileHeader, 0, sizeof(GREYCOMBINEFILEHDR));
        GB_MEMSET(codec->gbItemLoader, 0, sizeof(codec->gbItemLoader));
    }
    return (GB_Encoder)codec;
}

GB_INT32 GreyCombineFile_Encoder_GetCount(GB_Encoder codec)
{
    GCF_Encoder me = (GCF_Encoder)codec;
    int i;
    GB_INT32 nCount = 0;

    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        if(me->gbItemLoader[i])
        {
            nCount++;
        }
    }
    return nCount;
}

int	GreyCombineFile_Encoder_SetParam(GB_Encoder codec, GB_Param nParam, GB_UINT32 dwParam)
{
    return -1;
}

int	GreyCombineFile_Encoder_Delete(GB_Encoder codec, GB_UINT32 nCode)
{
    GCF_Encoder me = (GCF_Encoder)codec;
    if(nCode >= GCF_ITEM_MAX)
    {
        return -1;
    }
    
    me->gbFileHeader.gbfInfo[nCode].gbiHeight = 0;
    me->gbFileHeader.gbfInfo[nCode].gbiDataOff= 0;
    if(me->gbItemLoader[nCode])
    {
        GreyBitType_Loader_Done(me->gbItemLoader[nCode]);
        me->gbItemLoader[nCode] = 0;
    }
    return 0;
}

int	GreyCombineFile_Encoder_Encode(GB_Encoder codec, GB_UINT32 nCode, GB_Data pData)
{
    GCF_Encoder me = (GCF_Encoder)codec;
    
    if(!pData || pData->format != GB_FORMAT_STREAM)
    {
        return -1;
    }
    
    if(nCode >= GCF_ITEM_MAX)
    {
        return -1;
    }

    if(!pData->data)
    {
        return -1;
    }
    
    me->gbItemLoader[nCode] = (GB_Loader)pData->data;
    me->gbFileHeader.gbfInfo[nCode].gbiHeight = GreyBitType_Loader_GetHeight(me->gbItemLoader[nCode]);
    me->gbFileHeader.gbfInfo[nCode].gbiDataOff= 0;
    return 0;
}

int	GreyCombineFile_Encoder_Flush(GB_Encoder codec)
{
    GCF_Encoder me = (GCF_Encoder)codec;

    GreyCombineFile_Encoder_BuildAll(me);
    GreyCombineFile_Encoder_WriteAll(me);
    return 0;
}

void GreyCombineFile_Encoder_Done(GB_Encoder codec)
{
    GCF_Encoder me = (GCF_Encoder)codec;
    int i;
    
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        if(me->gbItemLoader[i])
        {
            GreyBitType_Loader_Done(me->gbItemLoader[i]);
            me->gbItemLoader[i] = 0;
        }
    }
    GreyBit_Free(me->gbMem,codec);
}

//-----------------------------------------------------------------------------
// Local function
//-----------------------------------------------------------------------------
static int GreyCombineFile_Encoder_BuildAll(GCF_Encoder me)
{
    GB_UINT32   dwOffset;
    int         i;

    dwOffset = sizeof(GREYCOMBINEFILEHDR);
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        if(me->gbItemLoader[i])
        {
            me->gbFileHeader.gbfInfo[i].gbiDataOff  = dwOffset;
            me->gbFileHeader.gbfInfo[i].gbiDataSize = me->gbItemLoader[i]->gbStream->size;
            dwOffset += me->gbItemLoader[i]->gbStream->size;
        }
    }
    GB_STRNCPY(me->gbFileHeader.gbfTag, "gctf", GB_STRLEN("gctf"));
    return 0;
}

static int GreyCombineFile_Encoder_WriteAll(GCF_Encoder me)
{
    GB_Stream pSrc;
    GB_BYTE *pData = me->gbBuffer;
    GB_INT32 nDataSize;
    int      i;
    
    GreyBit_Stream_Seek(me->gbStream, 0);
    GreyBit_Stream_Write(me->gbStream, (GB_BYTE *)&me->gbFileHeader, sizeof(GREYCOMBINEFILEHDR));
    
    for(i=0; i<GCF_ITEM_MAX; i++)
    {
        if(me->gbItemLoader[i])
        {
            pSrc = me->gbItemLoader[i]->gbStream;
            GreyBit_Stream_Seek(pSrc, 0);
            while(1)
            {
                nDataSize = GreyBit_Stream_Read(pSrc, pData, GCF_BUFF_SIZE);
                if(nDataSize > 0)
                {
                    GreyBit_Stream_Write(me->gbStream, pData, nDataSize);
                }
                else
                {
                    break;
                }
            }
        }
    }
    return 0;
}
#endif
#endif //#ifdef ENABLE_GREYCOMBINEFILE