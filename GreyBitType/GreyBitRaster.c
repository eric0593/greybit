/***************************************************************************/
/*                                                                         */
/*  GreyBitRaster.c                                                         */
/*                                                                         */
/*    A new `perfect' anti-aliasing renderer (body).                       */
/*                                                                         */
/***************************************************************************/
#include "GreyBitRaster.h"
#include "GreyBitType_Def.h"

#define GB_memset   GB_MEMSET

#define ErrRaster_Invalid_Mode      -2
#define ErrRaster_Invalid_Outline   -1
#define ErrRaster_Invalid_Argument  -3
#define ErrRaster_Memory_Overflow   -4

#define GB_DEFINE_OUTLINE_FUNCS( class_,               \
                                 move_to_, line_to_,   \
                                 conic_to_, cubic_to_, \
                                 shiGB_, delta_ )      \
          static const GB_Outline_Funcs class_ =       \
          {                                            \
            move_to_,                                  \
            line_to_,                                  \
            conic_to_,                                 \
            cubic_to_,                                 \
            shiGB_,                                    \
            delta_                                     \
         };
         
#ifndef GB_MEM_SET
#define GB_MEM_SET( d, s, c )  GB_memset( d, s, c )
#endif

#ifndef GB_MEM_ZERO
#define GB_MEM_ZERO( dest, count )  GB_MEM_SET( dest, 0, count )
#endif

  /* must be at least 6 bits! */
#define PIXEL_BITS  8

#define ONE_PIXEL       ( 1L << PIXEL_BITS )
#define PIXEL_MASK      ( -1L << PIXEL_BITS )
#define TRUNC( x )      ( (TCoord)( (x) >> PIXEL_BITS ) )
#define SUBPIXELS( x )  ( (TPos)(x) << PIXEL_BITS )
#define FLOOR( x )      ( (x) & -ONE_PIXEL )
#define CEILING( x )    ( ( (x) + ONE_PIXEL - 1 ) & -ONE_PIXEL )
#define ROUND( x )      ( ( (x) + (ONE_PIXEL>>1) ) & -ONE_PIXEL )
#define UPSCALE( x )    ( (x) << ( PIXEL_BITS - 6 ) )//( (x) << ( PIXEL_BITS - 6 ) ) ( (x) << PIXEL_BITS )
#define DOWNSCALE( x )  ( (x) >> ( PIXEL_BITS - 6 ) )//( (x) >> ( PIXEL_BITS - 6 ) ) ( (x) >> PIXEL_BITS )

/*************************************************************************/
/*                                                                       */
/*   TYPE DEFINITIONS                                                    */
/*                                                                       */

/* don't change the following types to GB_Int or GB_Pos, since we might */
/* need to define them to "float" or "double" when experimenting with   */
/* new algorithms                                                       */

typedef GB_INT32  TCoord;   /* integer scanline/pixel coordinate */
typedef GB_INT32  TPos;     /* sub-pixel coordinate              */
typedef GB_INT32  TArea;

/* maximal number of gray spans in a call to the span callback */
#define GB_MAX_GRAY_SPANS  32


typedef struct TCell_*  PCell;

typedef struct  TCell_
{
    TPos   x;     /* same with TWorker.ex */
    TCoord cover; /* same with TWorker.cover */
    TArea  area;
    PCell  next;
} TCell;

typedef struct  TWorker_
{
    TCoord  ex, ey;
    TPos    min_ex, max_ex;
    TPos    min_ey, max_ey;
    TPos    count_ex, count_ey;

    TArea   area;
    TCoord  cover;
    int     invalid;

    PCell   cells;
    int     max_cells;
    int     num_cells;

    TCoord  cx, cy;
    TPos    x,  y;

    TPos    last_ey;

    RST_Vector  bez_stack[32 * 3 + 1];
    int         lev_stack[32];

    GB_OutlineRec outline;
    GB_BitmapRec  target;
    GB_BBox     clip_box;

    GB_Span     gray_spans[GB_MAX_GRAY_SPANS];
    int         num_gray_spans;

    GB_Raster_Span_Func  render_span;
    void*                render_span_data;
    int                  span_y;

    int  band_size;
    int  band_shoot;
    int  conic_level;
    int  cubic_level;

    void*       buffer;
    GB_INT32    buffer_size;

    PCell*     ycells;
    TPos       ycount;

} TWorker, *PWorker;

typedef struct TRaster_
{
    GB_Memory   gbMem;
    void*       buffer;
    GB_INT32    buffer_size;
    int         band_size;
    PWorker     worker;
} TRaster, *PRaster;

  /*************************************************************************/
  /*                                                                       */
  /* Initialize the cells table.                                           */
  /*                                                                       */
  static void
  gray_init_cells( PWorker  worker, void*  buffer,
                   GB_INT32            byte_size )
  {
    worker->buffer      = buffer;
    worker->buffer_size = byte_size;

    worker->ycells      = (PCell*) buffer;
    worker->cells       = 0;
    worker->max_cells   = 0;
    worker->num_cells   = 0;
    worker->area        = 0;
    worker->cover       = 0;
    worker->invalid     = 1;
  }


  /*************************************************************************/
  /*                                                                       */
  /* Compute the outline bounding box.                                     */
  /*                                                                       */
  static void
  gray_compute_cbox( PWorker worker )
  {
    GB_Outline   outline = &worker->outline;
    GB_Vector*   vec     = outline->points;
    GB_Vector*   limit   = vec + outline->n_points;


    if ( outline->n_points <= 0 )
    {
      worker->min_ex = worker->max_ex = 0;
      worker->min_ey = worker->max_ey = 0;
      return;
    }

    worker->min_ex = worker->max_ex = vec->x;
    worker->min_ey = worker->max_ey = vec->y;

    vec++;

    for ( ; vec < limit; vec++ )
    {
      TPos  x = vec->x;
      TPos  y = vec->y;


      if ( x < worker->min_ex ) worker->min_ex = x;
      if ( x > worker->max_ex ) worker->max_ex = x;
      if ( y < worker->min_ey ) worker->min_ey = y;
      if ( y > worker->max_ey ) worker->max_ey = y;
    }

    /* truncate the bounding box to integer pixels */
    worker->min_ex = worker->min_ex >> 6;
    worker->min_ey = worker->min_ey >> 6;
    worker->max_ex = ( worker->max_ex + 63 ) >> 6;
    worker->max_ey = ( worker->max_ey + 63 ) >> 6;
  }

/*************************************************************************/
/*                                                                       */
/* Record the current cell in the table.                                 */
/*                                                                       */
static PCell gray_find_cell( PWorker worker, int *ret)
  {
    PCell  *pcell, cell;
    TPos    x = worker->ex;


    if ( x > worker->count_ex )
      x = worker->count_ex;

    pcell = &worker->ycells[worker->ey];
    for (;;)
    {
      cell = *pcell;
      if ( cell == 0 || cell->x > x )
        break;

      if ( cell->x == x )
        goto Exit;

      pcell = &cell->next;
    }

    if ( worker->num_cells >= worker->max_cells )
    {
        *ret = ErrRaster_Memory_Overflow;
        return cell;
    }
    //  GB_longjmp( worker->jump_buffer, 1 );

    cell        = worker->cells + worker->num_cells++;
    cell->x     = x;
    cell->area  = 0;
    cell->cover = 0;

    cell->next  = *pcell;
    *pcell      = cell;

  Exit:
    *ret = 0;
    return cell;
  }


static int gray_record_cell( PWorker worker )
{
    if ( !worker->invalid && ( worker->area | worker->cover ) )
    {
        int ret;
        PCell  cell = gray_find_cell( worker , &ret);
        if(ret != 0)
        {
            return ret;
        }
        
        cell->area  += worker->area;
        cell->cover += worker->cover;
    }
    return 0;
}


/*************************************************************************/
/*                                                                       */
/* Set the current cell to a new position.                               */
/*                                                                       */
static int gray_set_cell( PWorker  worker, TCoord  ex, TCoord  ey )
{
    /* Move the cell pointer to a new position.  We set the `invalid'      */
    /* flag to indicate that the cell isn't part of those we're interested */
    /* in during the render phase.  This means that:                       */
    /*                                                                     */
    /* . the new vertical position must be within min_ey..max_ey-1.        */
    /* . the new horizontal position must be strictly less than max_ex     */
    /*                                                                     */
    /* Note that if a cell is to the left of the clipping region, it is    */
    /* actually set to the (min_ex-1) horizontal position.                 */

    /* All cells that are on the left of the clipping region go to the */
    /* min_ex - 1 horizontal position.                                 */
    ey -= worker->min_ey;

    if ( ex > worker->max_ex )
        ex = worker->max_ex;

    ex -= worker->min_ex;
    if ( ex < 0 )
        ex = -1;

    /* are we moving to a different cell ? */
    if ( ex != worker->ex || ey != worker->ey )
    {
        /* record the current one if it is valid */
        if ( !worker->invalid )
        {
            int nRet = gray_record_cell(worker);
            if(0 != nRet)
            {
                return nRet;
            }
        }

        worker->area  = 0;
        worker->cover = 0;
    }

    worker->ex      = ex;
    worker->ey      = ey;
    worker->invalid = ( (unsigned)ey >= (unsigned)worker->count_ey ||
                              ex >= worker->count_ex           );
    return 0;
}

/*************************************************************************/
/*                                                                       */
/* Start a new contour at a given cell.                                  */
/*                                                                       */
static int gray_start_cell( PWorker  worker, TCoord  ex, TCoord  ey )
{
    if ( ex > worker->max_ex )
        ex = (TCoord)( worker->max_ex -1 );

    if ( ex < worker->min_ex )
        ex = (TCoord)( worker->min_ex - 1 );

    worker->area    = 0;
    worker->cover   = 0;
    worker->ex      = ex - worker->min_ex;
    worker->ey      = ey - worker->min_ey;
    worker->last_ey = SUBPIXELS( ey );
    worker->invalid = 0;

    return gray_set_cell( worker, ex, ey );
}


/*************************************************************************/
/*                                                                       */
/* Render a scanline as one or more cells.                               */
/*                                                                       */
static int gray_render_scanline( PWorker worker, 
                                 TCoord  ey,
                                 TPos    x1,
                                 TCoord  y1,
                                 TPos    x2,
                                 TCoord  y2 )
{
    TCoord  ex1, ex2, fx1, fx2, delta, mod, lift, rem;
    GB_INT32    p, first, dx;
    int     incr;


    dx = x2 - x1;

    ex1 = TRUNC( x1 );
    ex2 = TRUNC( x2 );
    fx1 = (TCoord)( x1 - SUBPIXELS( ex1 ) );
    fx2 = (TCoord)( x2 - SUBPIXELS( ex2 ) );

    /* trivial case.  Happens often */
    if ( y1 == y2 )
    {
        return gray_set_cell( worker, ex2, ey );
    }

    /* everything is located in a single cell.  That is easy! */
    /*                                                        */
    if ( ex1 == ex2 )
    {
        delta      = y2 - y1;
        worker->area  += (TArea)(( fx1 + fx2 ) * delta);
        worker->cover += delta;
        return 0;
    }

    /* ok, we'll have to render a run of adjacent cells on the same */
    /* scanline...                                                  */
    /*                                                              */
    p     = ( ONE_PIXEL - fx1 ) * ( y2 - y1 );
    first = ONE_PIXEL;
    incr  = 1;

    if ( dx < 0 )
    {
        p     = fx1 * ( y2 - y1 );
        first = 0;
        incr  = -1;
        dx    = -dx;
    }

    delta = (TCoord)( p / dx );
    mod   = (TCoord)( p % dx );
    if ( mod < 0 )
    {
        delta--;
        mod += (TCoord)dx;
    }

    worker->area  += (TArea)(( fx1 + first ) * delta);
    worker->cover += delta;

    ex1 += incr;
    if(0 != gray_set_cell( worker, ex1, ey ))
    {
        return -1;
    }
    
    y1  += delta;

    if ( ex1 != ex2 )
    {
        p    = ONE_PIXEL * ( y2 - y1 + delta );
        lift = (TCoord)( p / dx );
        rem  = (TCoord)( p % dx );
        if ( rem < 0 )
        {
            lift--;
            rem += (TCoord)dx;
        }

        mod -= (int)dx;

        while ( ex1 != ex2 )
        {
            delta = lift;
            mod  += rem;
            if ( mod >= 0 )
            {
              mod -= (TCoord)dx;
              delta++;
            }

            worker->area  += (TArea)(ONE_PIXEL * delta);
            worker->cover += delta;
            y1        += delta;
            ex1       += incr;
            if(0 != gray_set_cell( worker, ex1, ey ))
            {
                return -1;
            }
        }
    }

    delta      = y2 - y1;
    worker->area  += (TArea)(( fx2 + ONE_PIXEL - first ) * delta);
    worker->cover += delta;
    return 0;
}


/*************************************************************************/
/*                                                                       */
/* Render a given line as a series of scanlines.                         */
/*                                                                       */
static int gray_render_line( PWorker  worker, TPos  to_x, TPos  to_y )
{
    TCoord  ey1, ey2, fy1, fy2, mod;
    TPos    dx, dy, x, x2;
    GB_INT32    p, first;
    int     delta, rem, lift, incr;
    int     nRet = 0;

    ey1 = TRUNC( worker->last_ey );
    ey2 = TRUNC( to_y );     /* if (ey2 >= worker->max_ey) ey2 = worker->max_ey-1; */
    fy1 = (TCoord)( worker->y - worker->last_ey );
    fy2 = (TCoord)( to_y - SUBPIXELS( ey2 ) );

    dx = to_x - worker->x;
    dy = to_y - worker->y;

    /* XXX: we should do something about the trivial case where dx == 0, */
    /*      as it happens very often!                                    */

    /* perform vertical clipping */
    {
        TCoord  min, max;

        min = ey1;
        max = ey2;
        if ( ey1 > ey2 )
        {
            min = ey2;
            max = ey1;
        }
        if ( min >= worker->max_ey || max < worker->min_ey )
            goto End;
    }
    
    /* everything is on a single scanline */
    if ( ey1 == ey2 )
    {
        nRet = gray_render_scanline( worker, ey1, worker->x, fy1, to_x, fy2 );
        if(nRet != 0)
        {
            return nRet;
        }
        goto End;
    }

    /* vertical line - avoid calling gray_render_scanline */
    incr = 1;

    if ( dx == 0 )
    {
        TCoord  ex     = TRUNC( worker->x );
        TCoord  two_fx = (TCoord)( ( worker->x - SUBPIXELS( ex ) ) << 1 );
        TArea   area;

        first = ONE_PIXEL;
        if ( dy < 0 )
        {
            first = 0;
            incr  = -1;
        }

        delta      = (int)( first - fy1 );
        worker->area  += (TArea)two_fx * delta;
        worker->cover += delta;
        ey1       += incr;

        nRet = gray_set_cell( worker, ex, ey1 );
        if(nRet != 0)
        {
            return nRet;
        }

        delta = (int)( first + first - ONE_PIXEL );
        area  = (TArea)two_fx * delta;
        while ( ey1 != ey2 )
        {
            worker->area  += area;
            worker->cover += delta;
            ey1       += incr;

            nRet = gray_set_cell( worker, ex, ey1 );
            if(nRet != 0)
            {
                return nRet;
            }
        }

        delta      = (int)( fy2 - ONE_PIXEL + first );
        worker->area  += (TArea)two_fx * delta;
        worker->cover += delta;
        goto End;
    }

    /* ok, we have to render several scanlines */
    p     = ( ONE_PIXEL - fy1 ) * dx;
    first = ONE_PIXEL;
    incr  = 1;

    if ( dy < 0 )
    {
        p     = fy1 * dx;
        first = 0;
        incr  = -1;
        dy    = -dy;
    }

    delta = (int)( p / dy );
    mod   = (int)( p % dy );
    if ( mod < 0 )
    {
        delta--;
        mod += (TCoord)dy;
    }

    x = worker->x + delta;
    nRet = gray_render_scanline( worker, ey1, worker->x, fy1, x, (TCoord)first );
    if(nRet != 0)
    {
        return nRet;
    }
    
    ey1 += incr;
    nRet = gray_set_cell( worker, TRUNC( x ), ey1 );
    if(nRet != 0)
    {
        return nRet;
    }

    if ( ey1 != ey2 )
    {
        p     = ONE_PIXEL * dx;
        lift  = (int)( p / dy );
        rem   = (int)( p % dy );
        if ( rem < 0 )
        {
            lift--;
            rem += (int)dy;
        }
        mod -= (int)dy;

        while ( ey1 != ey2 )
        {
            delta = lift;
            mod  += rem;
            if ( mod >= 0 )
            {
                mod -= (int)dy;
                delta++;
            }

            x2 = x + delta;
            nRet = gray_render_scanline( worker, ey1, x,
                                       (TCoord)( ONE_PIXEL - first ), x2,
                                       (TCoord)first );
            if(nRet != 0)
            {
                return nRet;
            }
            x = x2;

            ey1 += incr;
            nRet = gray_set_cell( worker, TRUNC( x ), ey1 );
            if(nRet != 0)
            {
                return nRet;
            }
        }
    }

    nRet = gray_render_scanline( worker, ey1, x,
                                   (TCoord)( ONE_PIXEL - first ), to_x,
                                   fy2 );

  End:
    worker->x       = to_x;
    worker->y       = to_y;
    worker->last_ey = SUBPIXELS( ey2 );
    return nRet;
}


static void gray_split_conic( RST_Vector*  base )
{
    TPos  a, b;


    base[4].x = base[2].x;
    b = base[1].x;
    a = base[3].x = ( base[2].x + b ) / 2;
    b = base[1].x = ( base[0].x + b ) / 2;
    base[2].x = ( a + b ) / 2;

    base[4].y = base[2].y;
    b = base[1].y;
    a = base[3].y = ( base[2].y + b ) / 2;
    b = base[1].y = ( base[0].y + b ) / 2;
    base[2].y = ( a + b ) / 2;
}


static int gray_render_conic( PWorker  worker, const RST_Vector*  control, const RST_Vector*  to )
{
    TPos        dx, dy;
    int         top, level;
    int*        levels;
    RST_Vector*  arc;
    int         nRet = 0;


    dx = DOWNSCALE( worker->x ) + to->x - ( control->x << 1 );
    if ( dx < 0 )
        dx = -dx;
    dy = DOWNSCALE( worker->y ) + to->y - ( control->y << 1 );
    if ( dy < 0 )
        dy = -dy;
    if ( dx < dy )
        dx = dy;

    level = 1;
    dx = dx / worker->conic_level;
    while ( dx > 0 )
    {
        dx >>= 2;
        level++;
    }

    /* a shortcut to speed things up */
    if ( level <= 1 )
    {
        /* we compute the mid-point directly in order to avoid */
        /* calling gray_split_conic()                          */
        TPos  to_x, to_y, mid_x, mid_y;

        to_x  = UPSCALE( to->x );
        to_y  = UPSCALE( to->y );
        mid_x = ( worker->x + to_x + 2 * UPSCALE( control->x ) ) / 4;
        mid_y = ( worker->y + to_y + 2 * UPSCALE( control->y ) ) / 4;

        nRet = gray_render_line( worker, mid_x, mid_y );
        if(nRet != 0)
        {
            return nRet;
        }
        nRet = gray_render_line( worker, to_x, to_y );
        return nRet;
    }

    arc       = worker->bez_stack;
    levels    = worker->lev_stack;
    top       = 0;
    levels[0] = level;

    arc[0].x = UPSCALE( to->x );
    arc[0].y = UPSCALE( to->y );
    arc[1].x = UPSCALE( control->x );
    arc[1].y = UPSCALE( control->y );
    arc[2].x = worker->x;
    arc[2].y = worker->y;

    while ( top >= 0 )
    {
        level = levels[top];
        if ( level > 1 )
        {
            /* check that the arc crosses the current band */
            TPos  min, max, y;


            min = max = arc[0].y;

            y = arc[1].y;
            if ( y < min ) min = y;
            if ( y > max ) max = y;

            y = arc[2].y;
            if ( y < min ) min = y;
            if ( y > max ) max = y;

            if ( TRUNC( min ) >= worker->max_ey || TRUNC( max ) < worker->min_ey )
              goto Draw;

            gray_split_conic( arc );
            arc += 2;
            top++;
            levels[top] = levels[top - 1] = level - 1;
            continue;
        }

    Draw:
        {
            TPos  to_x, to_y, mid_x, mid_y;


            to_x  = arc[0].x;
            to_y  = arc[0].y;
            mid_x = ( worker->x + to_x + 2 * arc[1].x ) / 4;
            mid_y = ( worker->y + to_y + 2 * arc[1].y ) / 4;

            nRet = gray_render_line( worker, mid_x, mid_y );
            if(nRet != 0)
            {
                return nRet;
            }
            nRet = gray_render_line( worker, to_x, to_y );
            if(nRet != 0)
            {
                return nRet;
            }
            top--;
            arc -= 2;
        }
    }
    return nRet;
}


static void gray_split_cubic( RST_Vector*  base )
{
    TPos  a, b, c, d;


    base[6].x = base[3].x;
    c = base[1].x;
    d = base[2].x;
    base[1].x = a = ( base[0].x + c ) / 2;
    base[5].x = b = ( base[3].x + d ) / 2;
    c = ( c + d ) / 2;
    base[2].x = a = ( a + c ) / 2;
    base[4].x = b = ( b + c ) / 2;
    base[3].x = ( a + b ) / 2;

    base[6].y = base[3].y;
    c = base[1].y;
    d = base[2].y;
    base[1].y = a = ( base[0].y + c ) / 2;
    base[5].y = b = ( base[3].y + d ) / 2;
    c = ( c + d ) / 2;
    base[2].y = a = ( a + c ) / 2;
    base[4].y = b = ( b + c ) / 2;
    base[3].y = ( a + b ) / 2;
}


static int gray_render_cubic( PWorker  worker, 
                              const RST_Vector*  control1,
                              const RST_Vector*  control2,
                              const RST_Vector*  to )
{
    TPos        dx, dy, da, db;
    int         top, level;
    int*        levels;
    RST_Vector*  arc;
    int nRet = 0;


    dx = DOWNSCALE( worker->x ) + to->x - ( control1->x << 1 );
    if ( dx < 0 )
        dx = -dx;
    dy = DOWNSCALE( worker->y ) + to->y - ( control1->y << 1 );
    if ( dy < 0 )
        dy = -dy;
    if ( dx < dy )
        dx = dy;
    da = dx;

    dx = DOWNSCALE( worker->x ) + to->x - 3 * ( control1->x + control2->x );
    if ( dx < 0 )
        dx = -dx;
    dy = DOWNSCALE( worker->y ) + to->y - 3 * ( control1->x + control2->y );
    if ( dy < 0 )
        dy = -dy;
    if ( dx < dy )
        dx = dy;
    db = dx;

    level = 1;
    da    = da / worker->cubic_level;
    db    = db / worker->conic_level;
    while ( da > 0 || db > 0 )
    {
        da >>= 2;
        db >>= 3;
        level++;
    }

    if ( level <= 1 )
    {
        TPos   to_x, to_y, mid_x, mid_y;


        to_x  = UPSCALE( to->x );
        to_y  = UPSCALE( to->y );
        mid_x = ( worker->x + to_x +
                3 * UPSCALE( control1->x + control2->x ) ) / 8;
        mid_y = ( worker->y + to_y +
                3 * UPSCALE( control1->y + control2->y ) ) / 8;

        nRet = gray_render_line( worker, mid_x, mid_y );
        if(nRet != 0)
        {
            return nRet;
        }
        nRet = gray_render_line( worker, to_x, to_y );
        return nRet;
    }

    arc      = worker->bez_stack;
    arc[0].x = UPSCALE( to->x );
    arc[0].y = UPSCALE( to->y );
    arc[1].x = UPSCALE( control2->x );
    arc[1].y = UPSCALE( control2->y );
    arc[2].x = UPSCALE( control1->x );
    arc[2].y = UPSCALE( control1->y );
    arc[3].x = worker->x;
    arc[3].y = worker->y;

    levels    = worker->lev_stack;
    top       = 0;
    levels[0] = level;

    while ( top >= 0 )
    {
      level = levels[top];
      if ( level > 1 )
      {
        /* check that the arc crosses the current band */
        TPos  min, max, y;


        min = max = arc[0].y;
        y = arc[1].y;
        if ( y < min ) min = y;
        if ( y > max ) max = y;
        y = arc[2].y;
        if ( y < min ) min = y;
        if ( y > max ) max = y;
        y = arc[3].y;
        if ( y < min ) min = y;
        if ( y > max ) max = y;
        if ( TRUNC( min ) >= worker->max_ey || TRUNC( max ) < 0 )
          goto Draw;
        gray_split_cubic( arc );
        arc += 3;
        top ++;
        levels[top] = levels[top - 1] = level - 1;
        continue;
      }

    Draw:
      {
        TPos  to_x, to_y, mid_x, mid_y;


        to_x  = arc[0].x;
        to_y  = arc[0].y;
        mid_x = ( worker->x + to_x + 3 * ( arc[1].x + arc[2].x ) ) / 8;
        mid_y = ( worker->y + to_y + 3 * ( arc[1].y + arc[2].y ) ) / 8;

        nRet = gray_render_line( worker, mid_x, mid_y );
        if(nRet != 0)
        {
            return nRet;
        }
        nRet = gray_render_line( worker, to_x, to_y );
        if(nRet != 0)
        {
            return nRet;
        }
        top --;
        arc -= 3;
      }
    }

    return nRet;
}



static int gray_move_to( const RST_Vector*  to, PWorker worker )
{
    TPos  x, y;
    int error;

    /* record current cell, if any */
    error = gray_record_cell( worker );
    if(error != 0)
    {
        return error;
    }

    /* start to a new position */
    x = UPSCALE( to->x );
    y = UPSCALE( to->y );

    error = gray_start_cell( worker, TRUNC( x ), TRUNC( y ) );
    if(error != 0)
    {
        return error;
    }
    worker->x = x;
    worker->y = y;
    return 0;
}


static int gray_line_to( const RST_Vector*  to, PWorker worker )
{
    return gray_render_line( worker, UPSCALE( to->x ), UPSCALE( to->y ) );
}


static int gray_conic_to( const RST_Vector*  control,
                          const RST_Vector*  to,
                          PWorker           worker )
{
    return gray_render_conic( worker, control, to );
}


static int gray_cubic_to( const RST_Vector*  control1,
                          const RST_Vector*  control2,
                          const RST_Vector*  to,
                          PWorker           worker )
{
    return gray_render_cubic( worker, control1, control2, to );
}


static void gray_render_span( int             y,
                              int             count,
                              const GB_Span*  spans,
                              PWorker         worker )
{
    GB_BYTE*  p;
    GB_Bitmap       map = &worker->target;


    /* first of all, compute the scanline offset */
#if 1
    p = (GB_BYTE*)map->buffer + y * map->pitch;
#else
    p = (GB_BYTE*)map->buffer - y * map->pitch;
    if ( map->pitch >= 0 )
        p += ( map->height - 1 ) * map->pitch;
#endif
    for ( ; count > 0; count--, spans++ )
    {
        GB_BYTE  coverage = spans->coverage;
        
        if ( coverage )
        {
            if(coverage>0xF0)
            {
                coverage = 0xFF;
            }
            /* For small-spans it is faster to do it by ourselves than
            * calling `memset'.  This is mainly due to the cost of the
            * function call.
            */
            if ( spans->len >= 8 )
                GB_MEM_SET( p + spans->x, (GB_BYTE)coverage, spans->len );
            else
            {
                GB_BYTE*  q = p + spans->x;

                switch ( spans->len ){
                case 7: *q++ = (GB_BYTE)coverage;
                case 6: *q++ = (GB_BYTE)coverage;
                case 5: *q++ = (GB_BYTE)coverage;
                case 4: *q++ = (GB_BYTE)coverage;
                case 3: *q++ = (GB_BYTE)coverage;
                case 2: *q++ = (GB_BYTE)coverage;
                case 1: *q   = (GB_BYTE)coverage;
                default:
                    break;
                }
            }
        }
    }
}


static void gray_hline( PWorker  worker, TCoord  x,
                        TCoord  y,
                        TPos    area,
                        TCoord  acount )
{
    GB_Span*  span;
    int       count;
    int       coverage;


    /* compute the coverage line's coverage, depending on the    */
    /* outline fill rule                                         */
    /*                                                           */
    /* the coverage percentage is area/(PIXEL_BITS*PIXEL_BITS*2) */
    /*                                                           */
    coverage = (int)( area >> ( PIXEL_BITS * 2 + 1 - 8 ) );
                                                    /* use range 0..256 */
    if ( coverage < 0 )
        coverage = -coverage;

    /* normal non-zero winding rule */
    if ( coverage >= 256 )
        coverage = 255;

    y += (TCoord)worker->min_ey;
    x += (TCoord)worker->min_ex;

    /* GB_Span.x is a 16-bit GB_INT16, so limit our coordinates appropriately */
    if ( x >= 32767 )
        x = 32767;

    /* GB_Span.y is an integer, so limit our coordinates appropriately */
    //if ( y >= GB_INT_MAX )
    //  y = GB_INT_MAX;

    if ( coverage )
    {
      /* see whether we can add this span to the current list */
      count = worker->num_gray_spans;
      span  = worker->gray_spans + count - 1;
      if ( count > 0                          &&
           worker->span_y == y                    &&
           (int)span->x + span->len == (int)x &&
           span->coverage == coverage         )
      {
        span->len = (GB_UINT16)( span->len + acount );
        return;
      }

      if ( worker->span_y != y || count >= GB_MAX_GRAY_SPANS )
      {
        if ( worker->render_span && count > 0 )
          worker->render_span( worker->span_y, count, worker->gray_spans,
                           worker->render_span_data );

        worker->num_gray_spans = 0;
        worker->span_y         = (int)y;

        count = 0;
        span  = worker->gray_spans;
      }
      else
        span++;

      /* add a gray span to the current list */
      span->x        = (GB_INT16)x;
      span->len      = (GB_UINT16)acount;
      span->coverage = (GB_BYTE)coverage;

      worker->num_gray_spans++;
    }
}
  
static void gray_sweep( PWorker  worker, const GB_Bitmap target )
{
    int  yindex;
    
    if ( worker->num_cells == 0 )
      return;

    worker->num_gray_spans = 0;

    for ( yindex = 0; yindex < worker->ycount; yindex++ )
    {
      PCell   cell  = worker->ycells[yindex];
      TCoord  cover = 0;
      TCoord  x     = 0;


      for ( ; cell != 0; cell = cell->next )
      {
        TPos  area;


        if ( cell->x > x && cover != 0 )
          gray_hline( worker, x, yindex, cover * ( ONE_PIXEL * 2 ),
                      cell->x - x );

        cover += cell->cover;
        area   = cover * ( ONE_PIXEL * 2 ) - cell->area;

        if ( area != 0 && cell->x >= 0 )
          gray_hline( worker, cell->x, yindex, area, 1 );

        x = cell->x + 1;
      }

      if ( cover != 0 )
        gray_hline( worker, x, yindex, cover * ( ONE_PIXEL * 2 ),
                    worker->count_ex - x );
    }

    if ( worker->render_span && worker->num_gray_spans > 0 )
      worker->render_span( worker->span_y, worker->num_gray_spans,
                       worker->gray_spans, worker->render_span_data );
}

/*************************************************************************/
/*                                                                       */
/*  The following function should only compile in stand-alone mode,      */
/*  i.e., when building this component without the rest of FreeType.     */
/*                                                                       */
/*************************************************************************/

/*************************************************************************/
/*                                                                       */
/* <Function>                                                            */
/*    GB_Outline_Decompose                                               */
/*                                                                       */
/* <Description>                                                         */
/*    Walk over an outline's structure to decompose it into individual   */
/*    segments and Bézier arcs.  This function is also able to emit      */
/*    `move to' and `close to' operations to indicate the start and end  */
/*    of new contours in the outline.                                    */
/*                                                                       */
/* <Input>                                                               */
/*    outline        :: A pointer to the source target.                  */
/*                                                                       */
/*    func_interface :: A table of `emitters', i.e., function pointers   */
/*                      called during decomposition to indicate path     */
/*                      operations.                                      */
/*                                                                       */
/* <InOut>                                                               */
/*    user           :: A typeless pointer which is passed to each       */
/*                      emitter during the decomposition.  It can be     */
/*                      used to store the state during the               */
/*                      decomposition.                                   */
/*                                                                       */
/* <Return>                                                              */
/*    Error code.  0 means success.                                      */
/*                                                                       */
static int GB_Outline_Decompose( const GB_Outline         outline,
                                 const GB_Outline_Funcs*  func_interface,
                                 void*                    user )
  {
#undef SCALED
#define SCALED( x )  ( ( (x) << shift ) - delta )

    RST_Vector   v_last;
    RST_Vector   v_control;
    RST_Vector   v_start;

    GB_Vector*  point;
    GB_Vector*  limit;
    GB_BYTE*    tags;

    int         error;

    int   n;         /* index of contour in outline     */
    int   first;     /* index of first point in contour */
    GB_BYTE  tag;       /* current point's state           */

    int   shift;
    TPos  delta;


    if ( !outline || !func_interface )
      return ErrRaster_Invalid_Argument;

    shift = func_interface->shift;
    delta = func_interface->delta;
    first = 0;

    for ( n = 0; n < outline->n_contours; n++ )
    {
      int  last;  /* index of last point in contour */
      
      last  = outline->contours[n];
      if ( last < 0 )
        goto Invalid_Outline;
      limit = outline->points + last;

      v_start.x = SCALED( outline->points[first].x );
      v_start.y = SCALED( outline->points[first].y );

      v_last.x = SCALED( outline->points[last].x );
      v_last.y = SCALED( outline->points[last].y );

      v_control = v_start;

      point = outline->points + first;
      tags  = outline->tags   + first;
      tag   = GB_CURVE_TAG( tags[0] );

      /* A contour cannot start with a cubic control point! */
      if ( tag == GB_CURVE_TAG_CUBIC )
        goto Invalid_Outline;

      /* check first point to determine origin */
      if ( tag == GB_CURVE_TAG_CONIC )
      {
        /* first point is conic control.  Yes, this happens. */
        if ( GB_CURVE_TAG( outline->tags[last] ) == GB_CURVE_TAG_ON )
        {
          /* start at last point if it is on the curve */
          v_start = v_last;
          limit--;
        }
        else
        {
          /* if both first and last points are conic,         */
          /* start at their middle and record its position    */
          /* for closure                                      */
          v_start.x = ( v_start.x + v_last.x ) / 2;
          v_start.y = ( v_start.y + v_last.y ) / 2;

          v_last = v_start;
        }
        point--;
        tags--;
      }
      error = func_interface->move_to( &v_start, user );
      if ( error )
        goto Exit;

      while ( point < limit )
      {
        point++;
        tags++;

        tag = GB_CURVE_TAG( tags[0] );
        switch ( tag )
        {
        case GB_CURVE_TAG_ON:  /* emit a single line_to */
          {
            RST_Vector  vec;


            vec.x = SCALED( point->x );
            vec.y = SCALED( point->y );
            error = func_interface->line_to( &vec, user );
            if ( error )
              goto Exit;
            continue;
          }

        case GB_CURVE_TAG_CONIC:  /* consume conic arcs */
          v_control.x = SCALED( point->x );
          v_control.y = SCALED( point->y );

        Do_Conic:
          if ( point < limit )
          {
            RST_Vector  vec;
            RST_Vector  v_middle;


            point++;
            tags++;
            tag = GB_CURVE_TAG( tags[0] );

            vec.x = SCALED( point->x );
            vec.y = SCALED( point->y );

            if ( tag == GB_CURVE_TAG_ON )
            {
              error = func_interface->conic_to( &v_control, &vec, user );
              if ( error )
                goto Exit;
              continue;
            }

            if ( tag != GB_CURVE_TAG_CONIC )
              goto Invalid_Outline;

            v_middle.x = ( v_control.x + vec.x ) / 2;
            v_middle.y = ( v_control.y + vec.y ) / 2;
            error = func_interface->conic_to( &v_control, &v_middle, user );
            if ( error )
              goto Exit;

            v_control = vec;
            goto Do_Conic;
          }

          error = func_interface->conic_to( &v_control, &v_start, user );
          goto Close;

        default:  /* GB_CURVE_TAG_CUBIC */
          {
            RST_Vector  vec1, vec2;


            if ( point + 1 > limit                             ||
                 GB_CURVE_TAG( tags[1] ) != GB_CURVE_TAG_CUBIC )
              goto Invalid_Outline;

            point += 2;
            tags  += 2;

            vec1.x = SCALED( point[-2].x );
            vec1.y = SCALED( point[-2].y );

            vec2.x = SCALED( point[-1].x );
            vec2.y = SCALED( point[-1].y );

            if ( point <= limit )
            {
              RST_Vector  vec;


              vec.x = SCALED( point->x );
              vec.y = SCALED( point->y );
              error = func_interface->cubic_to( &vec1, &vec2, &vec, user );
              if ( error )
                goto Exit;
              continue;
            }
            error = func_interface->cubic_to( &vec1, &vec2, &v_start, user );
            goto Close;
          }
        }
      }
      error = func_interface->line_to( &v_start, user );

   Close:
      if ( error )
        goto Exit;

      first = last + 1;
    }

    return 0;

  Exit:
    return error;

  Invalid_Outline:
    return ErrRaster_Invalid_Outline;
  }

  typedef struct  TBand_
  {
    TPos  min, max;

  } TBand;

    GB_DEFINE_OUTLINE_FUNCS(func_interface,
      (GB_Outline_MoveTo_Func) gray_move_to,
      (GB_Outline_LineTo_Func) gray_line_to,
      (GB_Outline_ConicTo_Func)gray_conic_to,
      (GB_Outline_CubicTo_Func)gray_cubic_to,
      0,
      0
    )

static int gray_convert_glyph_inner( PWorker worker )
{
    volatile int  error = 0;
    error = GB_Outline_Decompose( &worker->outline, &func_interface, worker );
    if(gray_record_cell( worker ) != 0)
    {
        error = ErrRaster_Memory_Overflow;
    }
    
    return error;
}

  static int
  gray_convert_glyph( PWorker worker )
  {
    TBand            bands[40];
    TBand* volatile  band;
    int volatile     n, num_bands;
    TPos volatile    min, max, max_y;
    GB_BBox*         clip;


    /* Set up state in the raster object */
    gray_compute_cbox( worker );

    /* clip to target bitmap, exit if nothing to do */
    clip = &worker->clip_box;

    if ( worker->max_ex <= clip->xMin || worker->min_ex >= clip->xMax ||
         worker->max_ey <= clip->yMin || worker->min_ey >= clip->yMax )
      return 0;

    if ( worker->min_ex < clip->xMin ) worker->min_ex = clip->xMin;
    if ( worker->min_ey < clip->yMin ) worker->min_ey = clip->yMin;

    if ( worker->max_ex > clip->xMax ) worker->max_ex = clip->xMax;
    if ( worker->max_ey > clip->yMax ) worker->max_ey = clip->yMax;

    worker->count_ex = worker->max_ex - worker->min_ex;
    worker->count_ey = worker->max_ey - worker->min_ey;

    /* simple heuristic used to speed up the bezier decomposition -- see */
    /* the code in gray_render_conic() and gray_render_cubic() for more  */
    /* details                                                           */
    worker->conic_level = 32;
    worker->cubic_level = 16;

    {
      int  level = 0;


      if ( worker->count_ex > 24 || worker->count_ey > 24 )
        level++;
      if ( worker->count_ex > 120 || worker->count_ey > 120 )
        level++;

      worker->conic_level <<= level;
      worker->cubic_level <<= level;
    }

    /* set up vertical bands */
    num_bands = (int)( ( worker->max_ey - worker->min_ey ) / worker->band_size );
    if ( num_bands == 0 )
      num_bands = 1;
    if ( num_bands >= 39 )
      num_bands = 39;

    worker->band_shoot = 0;

    min   = worker->min_ey;
    max_y = worker->max_ey;

    for ( n = 0; n < num_bands; n++, min = max )
    {
      max = min + worker->band_size;
      if ( n == num_bands - 1 || max > max_y )
        max = max_y;

      bands[0].min = min;
      bands[0].max = max;
      band         = bands;

      while ( band >= bands )
      {
        TPos  bottom, top, middle;
        int   error;

        {
          PCell  cells_max;
          int    yindex;
          GB_INT32   cell_start, cell_end, cell_mod;


          worker->ycells = (PCell*)worker->buffer;
          worker->ycount = band->max - band->min;

          cell_start = sizeof ( PCell ) * worker->ycount;
          cell_mod   = cell_start % sizeof ( TCell );
          if ( cell_mod > 0 )
            cell_start += sizeof ( TCell ) - cell_mod;

          cell_end  = worker->buffer_size;
          cell_end -= cell_end % sizeof( TCell );

          cells_max = (PCell)( (GB_BYTE*)worker->buffer + cell_end );
          worker->cells = (PCell)( (GB_BYTE*)worker->buffer + cell_start );
          if ( worker->cells >= cells_max )
            goto ReduceBands;

          worker->max_cells = cells_max - worker->cells;
          if ( worker->max_cells < 2 )
            goto ReduceBands;

          for ( yindex = 0; yindex < worker->ycount; yindex++ )
            worker->ycells[yindex] = 0;
        }

        worker->num_cells = 0;
        worker->invalid   = 1;
        worker->min_ey    = band->min;
        worker->max_ey    = band->max;
        worker->count_ey  = band->max - band->min;

        error = gray_convert_glyph_inner( worker );

        if ( !error )
        {
          gray_sweep( worker, &worker->target );
          band--;
          continue;
        }
        else if ( error != ErrRaster_Memory_Overflow )
          return 1;

      ReduceBands:
        /* render pool overflow; we will reduce the render band by half */
        bottom = band->min;
        top    = band->max;
        middle = bottom + ( ( top - bottom ) >> 1 );

        /* This is too complex for a single scanline; there must */
        /* be some problems.                                     */
        if ( middle == bottom )
        {
          return 1;
        }

        if ( bottom-top >= worker->band_size )
          worker->band_shoot++;

        band[1].min = bottom;
        band[1].max = middle;
        band[0].min = middle;
        band[0].max = top;
        band++;
      }
    }

    if ( worker->band_shoot > 8 && worker->band_size > 16 )
      worker->band_size = worker->band_size / 2;

    return 0;
  }


int GreyBit_Raster_Render(GBHANDLE raster, GB_Bitmap tobitmap, GB_Outline fromoutline)
{
    PRaster me = (PRaster)raster;
    const GB_Outline  outline    = fromoutline;
    const GB_Bitmap   target_map = tobitmap;
    PWorker worker;

    if ( !outline )
        return ErrRaster_Invalid_Outline;
    
    /* return immediately if the outline is empty */
    if ( outline->n_points == 0 || outline->n_contours <= 0 )
        return 0;
    
    if ( !outline->contours || !outline->points )
        return ErrRaster_Invalid_Outline;

    if ( outline->n_points !=
           outline->contours[outline->n_contours - 1] + 1 )
        return ErrRaster_Invalid_Outline;
    
    worker = me->worker;

    if ( !target_map )
        return ErrRaster_Invalid_Argument;

    /* nothing to do */
    if ( !target_map->width || !target_map->height )
        return 0;

    if ( !target_map->buffer )
        return ErrRaster_Invalid_Argument;

    /* this version does not support monochrome rendering */

    /* compute clip box from target pixmap */
    worker->clip_box.xMin = 0;
    worker->clip_box.yMin = 0;
    worker->clip_box.xMax = target_map->width;
    worker->clip_box.yMax = target_map->height;

    gray_init_cells( worker, me->buffer, me->buffer_size );

    worker->outline        = *outline;
    worker->num_cells      = 0;
    worker->invalid        = 1;
    worker->band_size      = me->band_size;
    worker->num_gray_spans = 0;
    worker->target           = *target_map;
    worker->render_span      = (GB_Raster_Span_Func)gray_render_span;
    worker->render_span_data = worker;

    return gray_convert_glyph( worker );
}

GBHANDLE GreyBit_Raster_New(GBHANDLE library, int nPoolSize)
{
    GB_Library gbLib = (GB_Library)library;
    PRaster me;
    
    if(nPoolSize <= 0)
    {
        nPoolSize = DEFAULT_POOL_SIZE;
    }

    me = (PRaster)GreyBit_Malloc(gbLib->gbMem,(sizeof(TRaster)+nPoolSize));
    if(me)
    {
        GB_BYTE *pBase = (GB_BYTE *)me+sizeof(TRaster);
        me->gbMem = gbLib->gbMem;
        me->worker      = (PWorker)(pBase);
        me->buffer      = pBase +
                              ( ( sizeof ( TWorker ) + sizeof ( TCell ) - 1 ) &
                                ~( sizeof ( TCell ) - 1 ) );
        me->buffer_size = (GB_INT32)( ( pBase + nPoolSize ) -
                                    (GB_BYTE*)me->buffer ) &
                                      ~( sizeof ( TCell ) - 1 );
        me->band_size   = (int)( me->buffer_size/( sizeof ( TCell ) * 8 ) );
    }
    return me;
}

void GreyBit_Raster_Done(GBHANDLE raster)
{
    PRaster me = (PRaster)raster;
    GreyBit_Free(me->gbMem, me);
}
