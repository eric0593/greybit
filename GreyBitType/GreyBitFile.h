#ifndef GREYBITFILE_H_
#define GREYBITFILE_H_
#include "GreyBitType.h"
#include "UnicodeSection.h"
#include "GreyBitType_Def.h"

#ifdef __cplusplus
extern "C"{
#endif

#define MAX_COUNT       0x10000

// Compress
#define LEN_MASK        0x80
#define MAX_LEN()       (LEN_MASK-1)
#define GET_LEN(d)      (((d)&(~LEN_MASK))+1)
#define SET_LEN(d)      ((d)|LEN_MASK)
//#define SET_END()     (LEN_MASK)
#define IS_LEN(d)       ((d)&LEN_MASK)
//#define IS_END(d)     ((d) == LEN_MASK)

// RAM Index
#define RAM_MASK        0x80000000
#define IS_INRAM(d)     ((d)&RAM_MASK)
#define SET_RAM(d)      ((d)|RAM_MASK)
#define GET_INDEX(d)    ((d)&(~RAM_MASK))

// Unicode Section
typedef GB_INT8     GBF_Width;      // 宽度
typedef GB_INT8     GBF_HoriOff;    // 水平偏移
typedef GB_UINT32   GBF_Offset;     // 数据偏移
typedef GB_UINT16   GBF_Index;      // Section Index
typedef GB_UINT16   GBF_Lenght;     // DataLenght

// FileHeader+InfoHeader+SectionTable(Width)+SectionTable(Index)+Data(GreyBits)
typedef struct tagGREYBITFILEHEADER {
    char            gbfTag[4];    // gbtf
} GREYBITFILEHEADER, *PGREYBITFILEHEADER;

typedef struct tagSECTIONOINFO {
    GBF_Index         gbSectionOff[UNICODE_SECTION_NUM];        // Section Info 0为不可用
} SECTIONOINFO, *PSECTIONOINFO;

typedef struct tagGREYBITINFOHEADER {
    GB_UINT32       gbiSize;            // 结构体大小
    GB_UINT32       gbiCount;           // 包含文字个数
    GB_INT16        gbiBitCount;        // Bit像素深度
    GB_INT16        gbiCompression;     // 数据是否压缩
    GB_INT16        gbiWidth;           // 最大宽度
    GB_INT16        gbiHeight;          // 字体高度
    GB_UINT32       gbiWidthTabOff;     // 宽度表在数据区的偏移
    GB_UINT32       gbiHoriOffTabOff;   // 水平偏移表在数据区的偏移
    GB_UINT32       gbiOffsetTabOff;    // 检索表在数据区的偏移
    GB_UINT32       gbiOffGreyBits;     // 位图信息在数据区的偏移
    SECTIONOINFO    gbiWidthSection;    // 宽度Section信息
    SECTIONOINFO    gbiIndexSection;    // 字符偏移Section信息
} GREYBITINFOHEADER, *PGREYBITINFOHEADER;

// Decoder
typedef struct _GBF_DecoderRec{
    GB_DecoderRec       gbDecoder;
    GB_Library          gbLibrary;
    GB_Memory           gbMem;
    GB_Stream           gbStream;
    GB_Bitmap           gbBitmap;
    GB_INT32            nCacheItem;
    GB_INT32            nItemCount;
    GB_BYTE            *pBuff;
    GB_INT32            nBuffSize;
    GB_UINT32           gbOffDataBits;  // 数据区相对于文件头的偏移
    GREYBITFILEHEADER   gbFileHeader;
    GREYBITINFOHEADER   gbInfoHeader;
    GBF_Width          *gbWidthTable;   // Width Cache      // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GBF_Width)，普通模式全部读入
    GBF_HoriOff        *gbHoriOffTable; // Hori Offset      // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GBF_HoriOff)，普通模式全部读入
    GBF_Offset         *gbOffsetTable;  // Index Cache      // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GBF_Index)，普通模式全部读入
    GB_BYTE           **gpGreyBits;     // GreyBits Cache   // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GB_BYTE *)，普通模式按照nCacheItem缓存
    GB_INT32            nGreyBitsCount;
    GBF_Lenght         *pnGreySize;
}GBF_DecoderRec,*GBF_Decoder;

extern GB_Decoder   GreyBitFile_Decoder_New(GB_Loader loader, GB_Stream stream);
extern int          GreyBitFile_Decoder_SetParam(GB_Decoder decoder, GB_Param nParam, GB_UINT32 dwParam);
extern GB_INT32     GreyBitFile_Decoder_GetCount(GB_Decoder decoder);
extern GB_INT32     GreyBitFile_Decoder_GetHeight(GB_Decoder decoder);
extern GB_INT16     GreyBitFile_Decoder_GetWidth(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize);
extern GB_INT16     GreyBitFile_Decoder_GetAdvance(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize);
extern int          GreyBitFile_Decoder_Decode(GB_Decoder decoder, GB_UINT32 nCode, GB_Data pData, GB_INT16 nSize);
extern void         GreyBitFile_Decoder_Done(GB_Decoder decoder);

#ifdef ENABLE_ENCODER
typedef struct _GBF_EncoderRec{
    GB_EncoderRec       gbEncoder;
    GB_Library          gbLibrary;
    GB_Memory           gbMem;
    GB_Stream           gbStream;
    GB_UINT16           gbHeight;
    GB_BYTE             gbBitCount;
    GB_BOOL             gbCompress;
    GB_INT32            nCacheItem;
    GB_INT32            nItemCount;
    GB_UINT32           gbOffDataBits;  // 数据区相对于文件头的偏移
    GREYBITFILEHEADER   gbFileHeader;
    GREYBITINFOHEADER   gbInfoHeader;
    GBF_Width          *gbWidthTable;   // Width Cache      // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GBF_Width)，普通模式全部读入
    GBF_HoriOff        *gbHoriOffTable; // Hori Offset      // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GBF_HoriOff)，普通模式全部读入
    GBF_Offset         *gbOffsetTable;  // Index Cache      // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GBF_Index)，普通模式全部读入
    GB_BYTE           **gpGreyBits;     // GreyBits Cache   // 编辑模式将分配最大的空间，MAX_COUNT*sizeof(GB_BYTE *)，普通模式按照nCacheItem缓存
    GBF_Lenght         *pnGreySize;
}GBF_EncoderRec,*GBF_Encoder;

extern GB_Encoder   GreyBitFile_Encoder_New(GB_Creator creator, GB_Stream stream);
extern GB_INT32     GreyBitFile_Encoder_GetCount(GB_Encoder encoder);
extern int          GreyBitFile_Encoder_SetParam(GB_Encoder encoder, GB_Param nParam, GB_UINT32 dwParam);
extern int          GreyBitFile_Encoder_Delete(GB_Encoder encoder, GB_UINT32 nCode);
extern int          GreyBitFile_Encoder_Encode(GB_Encoder encoder, GB_UINT32 nCode, GB_Data pData);
extern int          GreyBitFile_Encoder_Flush(GB_Encoder encoder);
extern void         GreyBitFile_Encoder_Done(GB_Encoder encoder);
#endif
#ifdef __cplusplus
}
#endif 
#endif