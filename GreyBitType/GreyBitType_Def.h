#ifndef GREYBITTYPE_DEF_H_
#define GREYBITTYPE_DEF_H_
#include "GreyBitType.h"
#include "GreyBitSystem.h"

#ifdef __cplusplus
extern "C"{
#endif

typedef struct _GB_FormatRec*   GB_Format;
typedef struct _GB_DecoderRec*  GB_Decoder;
typedef struct _GB_EncoderRec*  GB_Encoder;

typedef struct _GB_LibraryRec{
    GB_Memory       gbMem;
    GB_Format       gbFormatHeader;
}GB_LibraryRec,*GB_Library;

typedef struct _GB_LoaderRec{
    GB_Library      gbLibrary;
    GB_Memory       gbMem;
    GB_Stream       gbStream;
    GB_Decoder      gbDecoder;
}GB_LoaderRec,*GB_Loader;

typedef struct _GB_LayoutRec{
    GB_Library      gbLibrary;
    GB_Memory       gbMem;
    GB_Stream       gbStream;
    GB_Decoder      gbDecoder;
    GB_Bitmap       gbBitmap;
    GB_Bitmap       gbBitmap8;
    GBHANDLE        gbRaster;
    GB_UINT32       dwCode;
    GB_BYTE        *gbSwitchBuf;
    GB_INT32        nSwitchBufLen;
    GB_INT16        nWidth;
    GB_INT16        nHeight;
    GB_INT16        nSize;
    GB_INT16        nBitCount;
    GB_INT16        bItalic;
    GB_INT16        bBold;
}GB_LayoutRec,*GB_Layout;

#ifdef ENABLE_ENCODER
typedef struct _GB_CreatorRec{
    GB_Library      gbLibrary;
    GB_Memory       gbMem;
    GB_Stream       gbStream;
    GB_Encoder      gbEncoder;
}GB_CreatorRec,*GB_Creator;
#endif

typedef GB_BOOL     (*GB_PROBE)(GB_Stream stream);
typedef GB_Decoder  (*GB_DECODER_NEW)(GB_Loader loader, GB_Stream stream);
typedef int         (*GB_DECODER_SETCACHE)(GB_Decoder decoder, GB_INT32 nCacheItem);
typedef int         (*GB_DECODER_SETPARAM)(GB_Decoder decoder, GB_Param nParam, GB_UINT32 dwParam);
typedef GB_INT32    (*GB_DECODER_GETCOUNT)(GB_Decoder decoder);
typedef GB_INT32    (*GB_DECODER_GETHEIGHT)(GB_Decoder decoder);
typedef GB_INT16    (*GB_DECODER_GETWIDTH)(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize);
typedef GB_INT16    (*GB_DECODER_GETADVANCE)(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize);
typedef int         (*GB_DECODER_DECODE)(GB_Decoder decoder, GB_UINT32 nCode, GB_Data pData, GB_INT16 nSize);
typedef void        (*GB_DECODER_DONE)(GB_Decoder decoder);

#ifdef ENABLE_ENCODER
typedef GB_Encoder  (*GB_ENCODER_NEW)(GB_Creator creator, GB_Stream stream);
typedef GB_INT32    (*GB_ENCODER_GETCOUNT)(GB_Encoder encoder);
typedef int         (*GB_ENCODER_SETPARAM)(GB_Encoder encoder, GB_Param nParam, GB_UINT32 dwParam);
typedef int         (*GB_ENCODER_DELETE)(GB_Encoder encoder, GB_UINT32 nCode);
typedef int         (*GB_ENCODER_ENCODE)(GB_Encoder encoder, GB_UINT32 nCode, GB_Data pData);
typedef int         (*GB_ENCODER_FLUSH)(GB_Encoder encoder);
typedef void        (*GB_ENCODER_DONE)(GB_Encoder encoder);
#endif

typedef struct _GB_FormatRec{
    GB_Format       next;
    char            tag[4]; // �ļ���չ��
    GB_PROBE        probe;
    GB_DECODER_NEW  decodernew;
#ifdef ENABLE_ENCODER
    GB_ENCODER_NEW  encodernew;
#endif
}GB_FormatRec;

extern GB_Decoder   GreyBit_Format_DecoderNew(GB_Format format, GB_Loader loader, GB_Stream stream);
#ifdef ENABLE_ENCODER
extern GB_Encoder   GreyBit_Format_EncoderNew(GB_Format format, GB_Creator creator, GB_Stream stream);
#endif
extern int          GreyBit_Format_Probe(GB_Format format, GB_Stream stream);
extern void         GreyBit_Format_Done(GB_Format format, GB_Memory mem);

// Decoder
typedef struct _GB_DecoderRec{
    GB_DECODER_SETPARAM     setparam;
    GB_DECODER_GETCOUNT     getcount;
    GB_DECODER_GETHEIGHT    getheight;
    GB_DECODER_GETWIDTH     getwidth;
    GB_DECODER_GETADVANCE   getadvance;
    GB_DECODER_DECODE       decode;
    GB_DECODER_DONE         done;
}GB_DecoderRec;

extern int          GreyBit_Decoder_SetParam(GB_Decoder decoder, GB_Param nParam, GB_UINT32 dwParam);
extern GB_INT32     GreyBit_Decoder_GetCount(GB_Decoder decoder);
extern GB_INT32     GreyBit_Decoder_GetHeight(GB_Decoder decoder);
extern GB_INT32     GreyBit_Decoder_GetWidth(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize);
extern GB_INT32     GreyBit_Decoder_GetAdvance(GB_Decoder decoder, GB_UINT32 nCode, GB_INT16 nSize);
extern int          GreyBit_Decoder_Decode(GB_Decoder decoder, GB_UINT32 nCode, GB_Data pData, GB_INT16 nSize);
extern void         GreyBit_Decoder_Done(GB_Decoder decoder);

#ifdef ENABLE_ENCODER
// Encoder
typedef struct _GB_EncoderRec{
    GB_ENCODER_GETCOUNT     getcount;
    GB_ENCODER_SETPARAM     setparam;
    GB_ENCODER_DELETE       remove;
    GB_ENCODER_ENCODE       encode;
    GB_ENCODER_FLUSH        flush;
    GB_ENCODER_DONE         done;
}GB_EncoderRec;

extern GB_INT32     GreyBit_Encoder_GetCount(GB_Encoder encoder);
extern int          GreyBit_Encoder_SetParam(GB_Encoder encoder, GB_Param nParam, GB_UINT32 dwParam);
extern int          GreyBit_Encoder_Delete(GB_Encoder encoder, GB_UINT32 nCode);
extern int          GreyBit_Encoder_Encode(GB_Encoder encoder, GB_UINT32 nCode, GB_Data pData);
extern int          GreyBit_Encoder_Flush(GB_Encoder encoder);
extern void         GreyBit_Encoder_Done(GB_Encoder encoder);
#endif

// System function
extern void *       GreyBit_Malloc_Sys(GB_INT32 size);
//extern void         GreyBit_Free_Sys(void *p);
#ifdef __cplusplus
}
#endif 
#endif //GREYBITTYPE_DEF_H_