#pragma once
#include <ft2build.h>
#include FT_FREETYPE_H
#include "GreyBitViewer.h"
#include "GreyBitType.h"

class FreeTypeFontEngine : public GreyBitViewer
{
private:
	FT_Library  m_FTlibrary;   /* handle to library     */
	FT_Face     m_FTface;      /* handle to face object */
	TCHAR *     m_pFileNamePtr;
	int         m_nSize;
    int         m_nScale;
	int         m_nWidth;
	int         m_nHeight;
    int         m_nHoriOff;
	int         m_nBitsLen;
	byte       *m_BitsBuf;
	int         m_nBufLen;
    GBHANDLE    m_GBlibrary;
    GB_Outline  m_Outline;

private:
    int GetDisstance(int x1, int y1, int x2, int y2);

public:
	FreeTypeFontEngine(void);
	~FreeTypeFontEngine(void);

public:
	void SetFontSize(int nSize, int nScale);
	bool SetFile(TCHAR *pName);
	bool IsCodeAvaliable(int nCode);
	int LoadGlyph(int nCode,char bShowOutline = 0);
	int GetWidth(void)		{return m_nWidth;};
	int GetHeight(void)		{return m_nHeight;};
	int GetPitch(void)		{return m_nWidth;};
    int GetHoriOff(void)    {return m_nHoriOff;};
	int GetBitsLen(void)	{return m_nBitsLen;};
	byte *GetBits(void)		{return m_BitsBuf;};
    void *GetOutline(void)  {return m_Outline;};
    void Destroy(void)      {delete this;};
};
