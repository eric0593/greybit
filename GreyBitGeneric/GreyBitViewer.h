#pragma once
#include "windows.h"

class GreyBitViewer
{
public:
	virtual void SetFontSize(int nSize, int nScale)= 0;
	virtual bool SetFile(TCHAR *pName)= 0;
	virtual bool IsCodeAvaliable(int nCode) = 0;
	virtual int LoadGlyph(int nCode,char bShowOutline = 0)= 0;
	virtual int GetWidth(void)= 0;
	virtual int GetHeight(void)= 0;
    virtual int GetHoriOff(void)= 0;
	virtual int GetPitch(void)= 0;
	virtual int GetBitsLen(void)= 0;
	virtual byte *GetBits(void)= 0;
    virtual void *GetOutline(void) = 0;
    virtual void Destroy(void)= 0;
};

class GreyBitEditor
{
public:
    virtual bool SetFile(TCHAR *pName)= 0;
	virtual int PrepareCreate(int nHeight, int nBitCount, bool bCompress)=0;
    virtual int SetBits(int nCode, byte *pBuff, int nWidth, int nBitCount, int nHoriOff)=0;
    virtual int SetOutline(int nCode, byte *pBuff, int nWidth, int nHoriOff)=0;
    virtual int FinishCreate(void)=0;
    virtual void Destroy(void)= 0;
};

class GreyBitFactory
{
public:
	GreyBitFactory(void);
	~GreyBitFactory(void);

public:
    GreyBitViewer *CreateViewer(TCHAR *pName);
    GreyBitEditor *CreateEditor(TCHAR *pName);
    void DestroyViewer(GreyBitViewer *pViewer);
    void DestroyEditor(GreyBitEditor *pEditor);
};