#include "GreyBitViewer.h"
#include "FreeTypeFontEngine.h"
#include "GreyBitFontEngine.h"

GreyBitFactory::GreyBitFactory(void)
{
}

GreyBitFactory::~GreyBitFactory(void)
{
}

GreyBitViewer *GreyBitFactory::CreateViewer(TCHAR *pName)
{
    GreyBitViewFontEngine *pGreyBitViewFontEngine;
    pGreyBitViewFontEngine = new GreyBitViewFontEngine();
    if(pGreyBitViewFontEngine->SetFile(pName))
    {
        return pGreyBitViewFontEngine;
    }
    else
    {
        delete pGreyBitViewFontEngine;
    }
    
    FreeTypeFontEngine *pFreeTypeFontEngine;
    pFreeTypeFontEngine = new FreeTypeFontEngine();
    if(pFreeTypeFontEngine->SetFile(pName))
    {
        return pFreeTypeFontEngine;
    }
    else
    {
        delete pFreeTypeFontEngine;
    }
    return NULL;
}

GreyBitEditor *GreyBitFactory::CreateEditor(TCHAR *pName)
{
    GreyBitEditor *pEditor;
    pEditor = new GreyBitFontEngine();
    if(pEditor->SetFile(pName))
    {
        return pEditor;
    }
    else
    {
        delete pEditor;
    }
    return NULL;
}

void GreyBitFactory::DestroyViewer(GreyBitViewer *pViewer)
{
    if(pViewer)
    {
        pViewer->Destroy();
    }
}

void GreyBitFactory::DestroyEditor(GreyBitEditor *pEditor)
{
    if(pEditor)
    {
        pEditor->Destroy();
    }
}