#! /usr/bin/env perl

#############################################################################
#
#                             G E T D E P
#
# GENERAL DESCRIPTION
#   Process the output of the C preprocessor into a nice list of dependencies.
#   The -M option on tcc works poorly, and can't handle an assembly file.
#   The first argument is the name of our object file.
#   The second object is the name of our source (needed for <stdin>
#   replacement).
#
# INVOCATION
#   perl getdep.pl file.o file.c
#
# Copyright (c) 1998, 1999 by QUALCOMM Incorporated.  All Rights Reserved.
#############################################################################
#
#                        EDIT HISTORY FOR FILE
#
# $Header:   L:/src/asw/MSM6050/vcs/getdep.plv   1.0   23 Oct 2001 15:28:08   donb  $
#
# when       who     what, where, why
# --------   ---     --------------------------------------------------------
# 08/22/02   kjm     Do NOT output a line for the source file itself to avoid
#                    screwing up implicit searching using vpath.
# 08/16/02   kjm     List the .dep file on the left side of the dependencies
# 11/13/01   kjm     Better handling of paths outside build directory and
#                    spaces in paths and filenames.
# 07/13/01   jmk     Modified #line pattern matching string to accept leading
#                    whitespace as well
# 02/21/00    mt     Changes for GNU make - put source as first dependency.
# 09/15/00    mt     Change algorithm to remove dependencies on files that
#                    are not in the directory.  Previous algorithm did not
#                    always remove them.  Also, we now only want to remove
#                    dependencies that are .h files.  This is because for
#                    internal RAM builds we have dependencies that are
#                    source files located in TARGETDIR.
# 06/20/00   jcw     Changed TARGET to TARGETDIR since they can be different
#                    in MSM5000 builds
# 10/13/98   dlb     Initial version.
#
#############################################################################

die "Usage: perl getdep.pl file.o file.c .dep\n"
    unless $#ARGV == 2;

$object = $ARGV[0];
$source = $ARGV[1];
$ext    = $ARGV[2];
$targvar = 'OBJSDIR';
#$armdirvar = 'ARMHOME';

# The object is probably of the form 'XXnnnn/name.o'.  Fix this to be
# '$($targvar)/name.o'.
$object =~ s#^[A-Z0-9a-z]+[\\\/]#\$\($targvar\)\/#;
$depobject = $object;
$object = '"' . $object . '"' if $object =~ /[ ]/;
$depobject =~ s/\.o$/$ext/;
$depobject =~ s/\.opp$/$ext/;
$depobject =~ s/\.osm$/$ext/;
$depobject = '"' . $depobject . '"' if $depobject =~ /[ ]/;
$object .= ' ' . $depobject;

$curdir = `cd`;
chomp $curdir;
#$curdir =~ tr/A-Z\\/a-z\//;
$curdir =~ tr/\\/\//;
#$curdir .= "\/";
#Remove "build"
$curdir = substr($curdir,0,(length($curdir)-5));

#if (defined($ENV{$armdirvar})) {
#  $armdir = $ENV{$armdirvar};
#  $armdir =~ tr/\\/\//;
#  $armdir .= "\/";
  #print "armdir = $armdir\n";
#}
%deps = ();

# Guarantee a line for the source file
$deps{$source} = 1;

while (<STDIN>) {
  next unless (/^\s*\#line\s+\d+\s+\"(.*)\"/);

  # Fix up a few names.
  $name = $1;

  # Hack workaround for bogus qct source that has #include "file.h " so
  # strip trailing spaces off the name inside quotes
  $name = $1 if $name =~ /^(.*?)[ \t]+$/;

  $name =~ s/\<stdin\>/$source/;

  $name =~ s/\\/\//g;
  $name =~ s/\/\//\//g;
  #$name =~ tr/A-Z/a-z/;
  
  # Change any paths to subdirectories to be relative paths
  if (length($name) > length($curdir)
      && substr(lc($name),0,length($curdir)) eq lc($curdir)) {
    $name = "..\/".substr($name,length($curdir),length($name)-length($curdir));
    #print "1. ", $name, "\n";
    $deps{$name} = 1;
  }
  elsif ($name !~ /[\\\/]/
         || ($name =~ /^[^.:\\\/]/ && $name !~ /^[A-Za-z]\:/)
         || $name =~ /^\.\.?[\\\/]/) {
    #print "2. ", $name, "\n";
    $deps{$name} = 1;
  }
  else {
    # Not a relative path and not a subdirectory
    # Go ahead and allow it unless ARMHOME is defined and
    # $name is a subdirectory of ARMHOME
    #print "3. ", $name, "\n";
    #$deps{$name} = 1;
    #unless (defined($armdir) && length($name) > length($armdir)
    #    && substr(lc($name),0,length($armdir)) eq lc($armdir));
  }
}

# Tricky stuff.  Need to remove any dependencies on .h files that are not
# present in this directory.  This indicates that it probably came
# from a -I.
#
# This is needed due to stupidity in the ARM compiler to not output
# paths on the #line directives.

# Print out the dependencies, with the C source being the first.

$source = '"' . $source . '"' if $source =~ /[ ]/;

# If all the implicit rules are set up properly, the correct .c will
# be automatically added to the list of prerequisites.  Listing it
# explicitly can cause failure of vpath to locate the correct location
# of a moved .c source file as it will prefer the explicitly mentioned
# .c location instead.  In fact, it doesn't matter whether or not the .dep
# file lists the .c as a prerequisite, only that the prerequisite is listed
# explicitly somewhere in the makefile.  This causes the .dep file to also
# use the wrong prerequisite rather than do a new vpath search. :(
# It's possible to work around this with a rather clever (complicated)
# rule for building .dep files, but why do that when this solution is so much
# simpler?

#print "$object : $source\n";

for $name (sort keys %deps) {

  # To really remove .h files that are not in this directory, we must
  # strip $name of its path.  This is because statements such as
  # #include "string.h" preprocesses to
  # #include "c:\apps\ads100\INCLUDE\string.h" and the -f $name test
  # fails because $name becomes "c:\apps\ads100\INCLUDE\string.h" and this
  # file exists.  This happens in cases where source files use
  # #include "string.h" instead of #include <string.h> or in *_.c sources
  # (preprocessed files generated by csplit.pl) where the
  # #include <string.h> in the original source file preprocesses to
  # #include "string.h" which in turn preprocesses to
  # #include "c:\apps\ads100\INCLUDE\string.h."
  #$name =~ s/.*\/(\w+\.h)/$1/i;

  next unless -f $name;
  $name = '"' . $name . '"' if $name =~ /[ ]/;
  #next if $name =~ /[ ]/;
  print "$object : $name\n" unless $name eq $source;
}

###########################################################################
# End of Perl script.
###########################################################################
