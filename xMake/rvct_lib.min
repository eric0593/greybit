#============================================================================
#  Name:
#    rvct_lib.min
#
#  Description:
#    Makefile to build the $(TARGET) module.
#
#   The following nmake targets are available in this makefile:
#
#     all           - make .elf and .mod image files (default)
#     clean         - delete object directory and image files
#     filename.o    - make object file
#     filename.mix  - make C and ASM mix file
#
#   The above targets can be made with the following command:
#
#     make [target]
#
#  Assumptions:
#    1. The ARM RVCT30 or higher tools are installed
#    2. The run environment of make toos is exist in this OS
#
#============================================================================
ifndef OBJSDIR
	$(error OBJSDIR must be defined)
endif
ifndef TARGETDIR
	$(error TARGETDIR must be defined)
endif
ifndef TARGET
	$(error TARGET must be defined)
endif

#-------------------------------------------------------------------------------
# Target file name and type definitions
#-------------------------------------------------------------------------------

EXETYPE    = elf#                # Target image file format

#-------------------------------------------------------------------------------
# Target compile time symbol definitions
#-------------------------------------------------------------------------------

ARMASM     = -D_ARM_ASM_#
PRJDEF     = $(addprefix -D, $(PRJ_DEFINE))#

#-------------------------------------------------------------------------------
# Software tool and environment definitions
#-------------------------------------------------------------------------------
COMPILERBIN  = $(RVCT30BIN)#
COMPILERBIN := $(subst \,/,$(COMPILERBIN))#

CC      = $(COMPILERBIN)/armcc#       # ARM ADS ARM 32-bit inst. set ANSI C compiler
CPP     = $(COMPILERBIN)/armcpp#      # ARM ADS ARM 32-bit inst. set ANSI C compiler
ASM     = $(COMPILERBIN)/armasm#      # ARM ADS assembler
AR      = $(COMPILERBIN)/armar#       # ARM Library maker
LD      = $(COMPILERBIN)/armlink#     # ARM ADS linker
HEXTOOL = elf2mod#     # ARM ADS utility to create hex file from image

OBJ_CMD    = -o#                 # Command line option to specify output filename

#-------------------------------------------------------------------------------
# Processor architecture options
#-------------------------------------------------------------------------------

CPU = --split_sections --cpu ARM926EJ-S#             # target processor

#-------------------------------------------------------------------------------
# ARM Procedure Call Standard (APCS) options
#-------------------------------------------------------------------------------
APCS = --apcs /ropi/norwpi/interwork

#-------------------------------------------------------------------------------
# Compiler output options
#-------------------------------------------------------------------------------

OUT = -c#                        # Object file output only

#-------------------------------------------------------------------------------
# Compiler/assembler debug options
#-------------------------------------------------------------------------------

DBG = -g#                        # Enable debug

#-------------------------------------------------------------------------------
# Compiler optimization options
#-------------------------------------------------------------------------------

OPT = -Ospace -O2 --enum_is_int --apcs=/adsabi --diag_suppress=C3011 --bss_threshold=0#           # Full compiler optimization for space
ASMOPT = --apcs=/adsabi --diag_suppress=A1658

#-------------------------------------------------------------------------------
# Compiler code generation options
#-------------------------------------------------------------------------------

END = --littleend#                # Compile for little endian memory architecture
ZA  = --feedback unused.txt#

CODE = $(END) $(ZA)#

#-------------------------------------------------------------------------------
# Compiler pragma emulation options
#-------------------------------------------------------------------------------

PCH = --pch --pch_dir $(OBJSDIR)#

#-------------------------------------------------------------------------------
# Include file search path options
#-------------------------------------------------------------------------------

INC = $(addprefix -I, $(INC_PATH))#

#-------------------------------------------------------------------------------
# HEXTOOL options
#-------------------------------------------------------------------------------

BINFORMAT = #-bin#
OUTPUT    = --output#

#-------------------------------------------------------------------------------
# Compiler flag definitions
#-------------------------------------------------------------------------------

CFLAGS  = $(OUT) $(INC) $(CPU) $(APCS) $(CODE) $(PCH) $(DBG) $(OPT) $(PRJDEF) /MTd /RTC1 /Z7 /GX /GS /Od /W3  /c
AFLAGS  = $(CPU) $(APCS) $(ASMOPT) $(INC)

#--------------------------------------------------------------------------
# Cpp code inference rules
#----------------------------------------------------------------------------
$(CPP_OBJS):$(OBJSDIR)/%.opp:%.cpp $(OBJSDIR)/exist $(TARGETDIR)/exist
	@echo -------------------cpp--------------------------------------------
	@echo OBJECT $(@F)
	$(CPP) $(CFLAGS) $(OBJ_FILE) $<
	@echo ---------------------------------------------------------------

#--------------------------------------------------------------------------
# C code inference rules
#----------------------------------------------------------------------------
$(C_OBJS):$(OBJSDIR)/%.o:%.c $(OBJSDIR)/exist $(TARGETDIR)/exist
	@echo ----------------------cc-----------------------------------------
	@echo OBJECT $(@F)
	$(CC) $(CFLAGS) $(OBJ_FILE) $<
	@echo ---------------------------------------------------------------

#-------------------------------------------------------------------------------
# Assembly code inference rules
#-------------------------------------------------------------------------------
$(A_OBJS):$(OBJSDIR)/%.osm:%.s $(OBJSDIR)/exist $(TARGETDIR)/exist
	@echo ---------------------------------------------------------------
	@echo OBJECT $(@F)
	$(CC) -E $(AFLAGS) $(ARMASM) $< | asm - > $(OBJSDIR)/$*.i
	$(ASM) $(AFLAGS) $(OBJ_FILE) $(OBJSDIR)/$*.i
	@echo ---------------------------------------------------------------

#-------------------------------------------------------------------------------
# Mixed source/assembly inference rule
#-------------------------------------------------------------------------------

.PHONY : dummyoutofdate

dummyoutofdate :

%.mix : %.c dummyoutofdate
	@echo ---------------------------------------------------------------
	@echo OBJECT $(@F)
	$(CC) -S -fs $(CFLAGS) $(INC) $(OBJ_FILE) $<
	@echo ---------------------------------------------------------------

%.mix : %.cpp dummyoutofdate
	@echo ---------------------------------------------------------------
	@echo OBJECT $(@F)
	$(CPP) -S -fs $(CFLAGS) $(INC) $(OBJ_FILE) $<
	@echo ---------------------------------------------------------------
	
#-------------------------------------------------------------------------------
# Depend file inference rules
#-------------------------------------------------------------------------------
$(OBJSDIR)/%.dep:%.c $(OBJSDIR)/exist $(TARGETDIR)/exist
	@echo ---------------------------------------------------------------
	$(CC) -E $(CFLAGS) $< > $(OBJSDIR)/$*.dep_
	getdeps $(basename $@).o $< .dep < $(OBJSDIR)/$*.dep_ > $(OBJSDIR)/$*.dep
	@rm -f $(OBJSDIR)/$*.dep_
	@echo ---------------------------------------------------------------
	
$(OBJSDIR)/%.depp:%.cpp $(OBJSDIR)/exist $(TARGETDIR)/exist
	@echo ---------------------------------------------------------------
	$(CPP) -E $(CFLAGS) $< > $(OBJSDIR)/$*.depp_
	getdeps $(basename $@).opp $< .depp < $(OBJSDIR)/$*.depp_ > $(OBJSDIR)/$*.depp
	@rm -f $(OBJSDIR)/$*.depp_
	@echo ---------------------------------------------------------------
	
$(OBJSDIR)/%.depa:%.s $(OBJSDIR)/exist $(TARGETDIR)/exist
	@echo ---------------------------------------------------------------
	$(CC) -E $(CFLAGS) $< > $(OBJSDIR)/$*.depa_
	getdeps $(basename $@).osm $< .depa < $(OBJSDIR)/$*.depa_ > $(OBJSDIR)/$*.depa
	@rm -f $(OBJSDIR)/$*.depa_
	@echo ---------------------------------------------------------------
	
#----------------------------------------------------------------------------
# Lib file targets
#----------------------------------------------------------------------------

$(TARGETDIR)/$(TARGET).$(MODULE) : $(OBJSDIR)/exist $(TARGETDIR)/exist $(OBJS) $(LIBS)
	@echo ---------------------------------------------------------------
	@echo TARGET $@
	$(AR) --create $(TARGETDIR)/$(TARGET).$(MODULE) $(OBJS) $(LIBS)

#============================================================================