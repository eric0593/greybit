#include <stdafx.h>
#include "CDib.h"

#define MATH_FACTOR_BIT 10

CDib::CDib()
{
    m_palDIB=NULL;
    m_hDIB=NULL;
}

CDib::~CDib()
{
    DeleteDIB();
}

void CDib::DeleteDIB()
{
    if (m_hDIB != NULL)
        ::GlobalFree((HGLOBAL) m_hDIB);
    
    if (m_palDIB != NULL)
        delete m_palDIB;
}

//从文件中载入DIB
BOOL CDib::Load(LPCTSTR lpszFileName)
{
    HDIB hDIB;
    CFile file;
    CFileException fe;
    
    if (!file.Open(lpszFileName, CFile::modeRead|CFile::shareDenyWrite, &fe))
    {
        AfxMessageBox(fe.m_cause);
        return FALSE;
    }
    TRY
    {
        hDIB = ::ReadDIBFile(file, &m_bmfHeader);
    }
    CATCH (CFileException, eLoad)
    {
        file.Abort(); 
        return FALSE;
    }
    END_CATCH
    
    DeleteDIB(); //清除旧位图
    
    m_hDIB=hDIB;
    m_palDIB = new CPalette;
    
    if (::CreateDIBPalette(m_hDIB, m_palDIB) == NULL)
    {
        // DIB有可能没有调色板
        delete m_palDIB;
        m_palDIB = NULL;
    }
    return TRUE;
}

//从资源中载入DIB
BOOL CDib::Load(UINT nID)
{
    HINSTANCE hResInst = AfxGetResourceHandle();
    HRSRC hFindRes;
    HDIB hDIB;
    LPSTR pDIB;
    LPSTR pRes;
    HGLOBAL hRes;
    
    //搜寻指定的资源
    hFindRes = ::FindResource(hResInst, MAKEINTRESOURCE(nID), RT_BITMAP);
    if (hFindRes == NULL) return FALSE;
    
    hRes = ::LoadResource(hResInst, hFindRes); //载入位图资源
    if (hRes == NULL) return FALSE;
    
    DWORD dwSize=::SizeofResource(hResInst,hFindRes);
        
    hDIB = (HDIB) ::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, dwSize);
    if (hDIB == NULL) return FALSE;
    
    pDIB = (LPSTR)::GlobalLock((HGLOBAL)hDIB);
    pRes = (LPSTR) ::LockResource(hRes);
    memcpy(pDIB, pRes, dwSize); //把hRes中的内容复制hDIB中
    ::GlobalUnlock((HGLOBAL) hDIB);
        
    DeleteDIB();
    memset(&m_bmfHeader, 0, sizeof(BITMAPFILEHEADER));
    m_hDIB=hDIB;
    m_palDIB = new CPalette;
    if (::CreateDIBPalette(m_hDIB, m_palDIB) == NULL)
    {
        // DIB有可能没有调色板
        delete m_palDIB;
        m_palDIB = NULL;
    }
    return TRUE;
}

BOOL CDib::Create(int nWidth, int nHeight, int nBitCount, LPSTR pBits)
{
    WORD nPalette;
    DWORD nSize, nHdrSize, nPaleteSize, nDIBBitSize;
    HDIB hDIB;
    LPBITMAPINFOHEADER pHDR;
    PPALETTEENTRY pPal;
    LPSTR pDIB;
    int nPitch = WIDTHBYTES(nBitCount*nWidth);
    int nTrueHeight = nHeight>0?nHeight:-nHeight;
    
    switch(nBitCount){
    case 1:
        nPalette = 2;
        break;
    case 8:
        nPalette = 256;
        break;
    case 24:
        nPalette = 0;
        break;
    default:
        return FALSE;
    }

    nHdrSize    = sizeof(BITMAPINFOHEADER);
    nPaleteSize = nPalette*sizeof(PALETTEENTRY);
    nDIBBitSize = nPitch*nTrueHeight;
    nSize = nHdrSize+nPaleteSize+nDIBBitSize;

    hDIB = (HDIB) ::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, nSize);
    if (hDIB == NULL) return FALSE;
    
    pDIB = (LPSTR)::GlobalLock((HGLOBAL)hDIB);
    pHDR = (LPBITMAPINFOHEADER)pDIB;
    pHDR->biSize            = nHdrSize;
    pHDR->biWidth           = nWidth;
    pHDR->biHeight          = nHeight;
    pHDR->biPlanes          = 1;
    pHDR->biBitCount        = nBitCount;
    pHDR->biCompression     = 0;
    pHDR->biSizeImage       = 0;
    pHDR->biXPelsPerMeter   = 0;
    pHDR->biYPelsPerMeter   = 0;
    pHDR->biClrUsed         = nPalette;
    pHDR->biClrImportant    = 0;

    pDIB += nHdrSize;
    pPal = (PPALETTEENTRY)pDIB;
    for(int i=0; i<nPalette; i++)
    {
        pPal[i].peBlue = pPal[i].peGreen = pPal[i].peRed  = 0xFF - 0xFF*i/(nPalette-1);
        pPal[i].peFlags = 0;
    }
    pDIB += nPaleteSize;
    if(pBits)
    {
        memcpy(pDIB, pBits, nDIBBitSize); //把Bits中的内容复制hDIB中
    }
    
    ::GlobalUnlock((HGLOBAL) hDIB);

    DeleteDIB();
    memset(&m_bmfHeader, 0, sizeof(BITMAPFILEHEADER));
    m_hDIB=hDIB;
    m_palDIB = new CPalette;
    if (::CreateDIBPalette(m_hDIB, m_palDIB) == NULL)
    {
        // DIB有可能没有调色板
        delete m_palDIB;
        m_palDIB = NULL;
    }
    return TRUE;
}

BOOL CDib::Save(LPCTSTR lpszFileName, int nWidth, int nHeight, int nPitch, int nBitCount, LPSTR pBits)
{
    WORD nPalette;
    DWORD nSize, nHdrSize, nPaleteSize, nDIBBitSize;
    HDIB hDIB;
    LPBITMAPINFOHEADER pHDR;
    PPALETTEENTRY pPal;
    LPSTR pDIB;
    int nDstPitch = WIDTHBYTES(nBitCount*nWidth);
    int nTrueHeight = nHeight>0?nHeight:-nHeight;

    CFile file;
    CFileException fe;
    
    if (!file.Open(lpszFileName, CFile::modeWrite|CFile::modeCreate, &fe))
    {
        AfxMessageBox(fe.m_cause);
        return FALSE;
    }

    switch(nBitCount){
    case 1:
        nPalette = 2;
        break;
    case 8:
        nPalette = 256;
        break;
    case 24:
        nPalette = 0;
        break;
    default:
        return FALSE;
    }

    nHdrSize    = sizeof(BITMAPINFOHEADER);
    nPaleteSize = nPalette*sizeof(PALETTEENTRY);
    nDIBBitSize = nDstPitch*nTrueHeight;
    nSize = nHdrSize+nPaleteSize+nDIBBitSize;

    hDIB = (HDIB) ::GlobalAlloc(GMEM_MOVEABLE | GMEM_ZEROINIT, nSize);
    if (hDIB == NULL) return FALSE;
    
    pDIB = (LPSTR)::GlobalLock((HGLOBAL)hDIB);
    pHDR = (LPBITMAPINFOHEADER)pDIB;
    pHDR->biSize            = nHdrSize;
    pHDR->biWidth           = nWidth;
    pHDR->biHeight          = nTrueHeight;
    pHDR->biPlanes          = 1;
    pHDR->biBitCount        = nBitCount;
    pHDR->biCompression     = 0;
    pHDR->biSizeImage       = 0;
    pHDR->biXPelsPerMeter   = 0;
    pHDR->biYPelsPerMeter   = 0;
    pHDR->biClrUsed         = nPalette;
    pHDR->biClrImportant    = 0;

    pDIB += nHdrSize;
    pPal = (PPALETTEENTRY)pDIB;
    for(int i=0; i<nPalette; i++)
    {
        pPal[i].peBlue = pPal[i].peGreen = pPal[i].peRed  = 0xFF - 0xFF*i/(nPalette-1);
        pPal[i].peFlags = 0;
    }
    pDIB += nPaleteSize;
    
    LPSTR pDst, pSrc;
    
    if(nHeight > 0)
    {
        pDst = (LPSTR)pDIB;
        pSrc = (LPSTR)pBits;
        for(int y=0;y<nTrueHeight;y++)
        {
            memcpy(pDst, pSrc, nPitch); //把Bits中的内容复制hDIB中
            pSrc += nPitch;
            pDst += nDstPitch;
        }
    }
    else
    {
        pDst = (LPSTR)pDIB;
        pSrc = (LPSTR)pBits+(nTrueHeight-1)*nPitch;
        for(int y=0;y<nTrueHeight;y++)
        {
            memcpy(pDst, pSrc, nPitch); //把Bits中的内容复制hDIB中
            pSrc -= nPitch;
            pDst += nDstPitch;
        }
    }
    
    BOOL bRet = ::SaveDIB(hDIB, file);
    ::GlobalUnlock((HGLOBAL) hDIB);
    ::GlobalFree((HGLOBAL) hDIB);
    return bRet;
}

int CDib::Width()
{
    if(m_hDIB==NULL) return 0;
    LPSTR lpDIB = (LPSTR) ::GlobalLock((HGLOBAL) m_hDIB);
    int cxDIB = (int) ::DIBWidth(lpDIB); // Size of DIB - x
    ::GlobalUnlock((HGLOBAL) m_hDIB);
    return cxDIB;

}

int CDib::Height()
{
    if(m_hDIB==NULL) return 0;
    LPSTR lpDIB = (LPSTR) ::GlobalLock((HGLOBAL) m_hDIB);
    int cyDIB = (int) ::DIBHeight(lpDIB); // Size of DIB - y
    ::GlobalUnlock((HGLOBAL) m_hDIB);
    return cyDIB;
}

int CDib::BitCount()
{
    if(m_hDIB==NULL) return 0;
    LPSTR lpDIB = (LPSTR) ::GlobalLock((HGLOBAL) m_hDIB);
    int BitCount = (int) ::DIBBitCount(lpDIB); // Size of DIB - x
    ::GlobalUnlock((HGLOBAL) m_hDIB);
    return BitCount;
}

int CDib::Pitch()
{
    return WIDTHBYTES(BitCount()*Width());
}

LPSTR CDib::Bits()
{
    if(m_hDIB==NULL) return 0;
    LPSTR lpDIB = (LPSTR) ::GlobalLock((HGLOBAL) m_hDIB);
    LPSTR Bits = NULL;
    if(m_bmfHeader.bfOffBits ==0)
    {
        Bits = ::FindDIBBits(lpDIB); // Size of DIB - x
    }
    else
    {
        Bits = lpDIB + m_bmfHeader.bfOffBits - sizeof(BITMAPFILEHEADER);
    }
    ::GlobalUnlock((HGLOBAL) m_hDIB);
    return Bits;
}

PPALETTEENTRY CDib::PaletteBits()
{
    if(m_hDIB==NULL) return 0;

    if(BitCount()<=8)
    {
        LPSTR lpDIB = (LPSTR) ::GlobalLock((HGLOBAL) m_hDIB);
        PPALETTEENTRY Bits = (PPALETTEENTRY)::FindPalletteBits(lpDIB); // Size of DIB - x
        ::GlobalUnlock((HGLOBAL) m_hDIB);
        return Bits;
    }
    return 0;
}
//显示DIB，该函数具有缩放功能
//参数x和y说明了目的矩形的左上角坐标，cx和cy说明了目的矩形的尺寸
//cx和cy若有一个为0则该函数按DIB的实际大小绘制，cx和cy的缺省值是0
BOOL CDib::Draw(CDC *pDC, int x, int y, int cx, int cy)
{

    if(m_hDIB==NULL) return FALSE;
    CRect rDIB,rDest;
    
    rDest.left=x;
    rDest.top=x;
    
    if(cx==0||cy==0)
    {
        cx=Width();
        cy=Height();
    }
    
    rDest.right=rDest.left+cx;
    rDest.bottom=rDest.top+cy;
    rDIB.left=rDIB.top=0;
    rDIB.right=Width();
    rDIB.bottom=Height();
    return ::PaintDIB(pDC->GetSafeHdc(),&rDest,m_hDIB,&rDIB,m_palDIB);
}


static BOOL DIB1_StretchBlt1(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Mask1;
            int Mask2;
            BYTE Curr1;
            BYTE Curr2;

            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);
            Mask1 = 0x80>>(xd%8);
            Mask2 = 0x80>>(xs%8);
            Curr1 = pDst[xd>>3]&(~Mask1);
            Curr2 = pSrc[xs>>3]&Mask2;
            Curr1 |= (Curr2<<(7-(xs%8)))>>(xd%8);
            pDst[xd>>3] = Curr1;
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB1_StretchBlt8(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Mask1 = 0x80>>(xd%8);
            BYTE Curr1 = pDst[xd>>3]&(~Mask1);
            BYTE Curr2;

            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);
            Curr2 = pSrc[xs];
            Curr1 |= (Curr2&0x80)>>(xd%8);
            pDst[xd>>3] = Curr1;
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB1_StretchBlt24(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Off;
            int Mask1;
            BYTE Curr1;
            BYTE Curr2;

            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);
            Off = xs*3;
            Mask1 = 0x80>>(xd%8);
            Curr1 = pDst[xd>>3]&(~Mask1);
            Curr2 = ~RGB2GRAY(pSrc[Off],pSrc[Off+1],pSrc[Off+2]);
            Curr1 |= (Curr2&0x80)>>(xd%8);
            pDst[xd>>3] = Curr1;
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB1_StretchBlt32(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Off;
            int Mask1;
            BYTE Curr1;
            BYTE Curr2;

            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);
            Off = xs*4;
            Mask1 = 0x80>>(xd%8);
            Curr1 = pDst[xd>>3]&(~Mask1);
            Curr2 = ~RGB2GRAY(pSrc[Off],pSrc[Off+1],pSrc[Off+2]);
            Curr1 |= (Curr2&0x80)>>(xd%8);
            pDst[xd>>3] = Curr1;
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB8_StretchBlt1(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Mask2;
            BYTE Curr2;

            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);
            Mask2 = 0x80>>(xs%8);
            Curr2 = pSrc[xs>>3]&Mask2;
            pDst[xd] = Curr2?0xFF:0;
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB8_StretchBlt8(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);
            pDst[xd] = pSrc[xs];
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB8_StretchBlt24(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Off;
            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);
            Off = xs*3;
            pDst[xd] = ~RGB2GRAY(pSrc[Off],pSrc[Off+1],pSrc[Off+2]);
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB8_StretchBlt32(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Off;
            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);
            Off = xs*4;
            pDst[xd] = ~RGB2GRAY(pSrc[Off],pSrc[Off+1],pSrc[Off+2]);
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB24_StretchBlt1(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight, PPALETTEENTRY pPal)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Off = xd*3;
            int Mask2;
            BYTE Curr2;

            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);

            Mask2 = 0x80>>(xs%8);
            Curr2 = pSrc[xs>>3]&Mask2;
            if(Curr2)
            {
                pDst[Off]   = pPal[1].peRed;
                pDst[Off+1] = pPal[1].peGreen;
                pDst[Off+2] = pPal[1].peBlue;
            }
            else
            {
                pDst[Off]   = pPal[0].peRed;
                pDst[Off+1] = pPal[0].peGreen;
                pDst[Off+2] = pPal[0].peBlue;
            }
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB24_StretchBlt8(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight, PPALETTEENTRY pPal)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Off = xd*3;
            BYTE Curr2;

            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);
            Curr2 = pSrc[xs];

            pDst[Off]   = pPal[Curr2].peRed;
            pDst[Off+1] = pPal[Curr2].peGreen;
            pDst[Off+2] = pPal[Curr2].peBlue;
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB24_StretchBlt24(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Off1 = xd*3;
            int Off2;

            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);

            Off2 = xs*3;
            pDst[Off1]   = pSrc[Off2];
            pDst[Off1+1] = pSrc[Off2+1];
            pDst[Off1+2] = pSrc[Off2+2];
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB24_StretchBlt32(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Off1 = xd*3;
            int Off2;

            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);

            Off2 = xs*4;
            pDst[Off1]   = pSrc[Off2];
            pDst[Off1+1] = pSrc[Off2+1];
            pDst[Off1+2] = pSrc[Off2+2];
        }
        pDst += nDstPitch;
    }
    return TRUE;
}


static BOOL DIB32_StretchBlt1(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight, PPALETTEENTRY pPal)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Off = xd*4;
            int Mask2;
            BYTE Curr2;

            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);

            Mask2 = 0x80>>(xs%8);
            Curr2 = pSrc[xs>>3]&Mask2;
            if(Curr2)
            {
                pDst[Off]   = pPal[1].peRed;
                pDst[Off+1] = pPal[1].peGreen;
                pDst[Off+2] = pPal[1].peBlue;
                pDst[Off+3] = pPal[1].peFlags;
            }
            else
            {
                pDst[Off]   = pPal[0].peRed;
                pDst[Off+1] = pPal[0].peGreen;
                pDst[Off+2] = pPal[0].peBlue;
                pDst[Off+3] = pPal[0].peFlags;
            }
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB32_StretchBlt8(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight, PPALETTEENTRY pPal)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Off = xd*4;
            BYTE Curr2;

            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);
            Curr2 = pSrc[xs];

            pDst[Off]   = pPal[Curr2].peRed;
            pDst[Off+1] = pPal[Curr2].peGreen;
            pDst[Off+2] = pPal[Curr2].peBlue;
            pDst[Off+3] = pPal[Curr2].peFlags;
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB32_StretchBlt24(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Off1 = xd*4;
            int Off2;

            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);

            Off2 = xs*3;
            pDst[Off1]   = pSrc[Off2];
            pDst[Off1+1] = pSrc[Off2+1];
            pDst[Off1+2] = pSrc[Off2+2];
            pDst[Off1+3] = 0;
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

static BOOL DIB32_StretchBlt32(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, int nSrcWidth, int nSrcHeight)
{
    int x,y;
    int xd;
    int xs,ys;
    int fScalex =(nSrcWidth<<MATH_FACTOR_BIT) / nWidth;
    int fScaley =(nSrcHeight<<MATH_FACTOR_BIT) / nHeight;
    LPSTR pSrcHeader = pSrc;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;

        ys = (y * fScaley)>>MATH_FACTOR_BIT;
        pSrc = pSrcHeader + ys * nSrcPitch;

        for(x=0; x<nWidth; x++,xd++)
        {
            int Off1 = xd*4;
            int Off2;

            xs = xSrc + ((x * fScalex)>>MATH_FACTOR_BIT);

            Off2 = xs*4;
            pDst[Off1]   = pSrc[Off2];
            pDst[Off1+1] = pSrc[Off2+1];
            pDst[Off1+2] = pSrc[Off2+2];
            pDst[Off1+3] = pSrc[Off2+3];
        }
        pDst += nDstPitch;
    }
    return TRUE;
}

BOOL CDib::StretchBlt(int x, int y, int nWidth, int nHeight, CDib &dibSrc, int xSrc, int ySrc, int nSrcWidth, int nSrcHeight)
{
    LPSTR pDst;
    int nDstPitch;
    LPSTR pSrc;
    int nSrcPitch;
    int nSrcTrueHeight = dibSrc.Height() > 0 ? dibSrc.Height(): -dibSrc.Height();
    int nDstTrueHeight = Height() > 0 ? Height(): -Height();

    if(x>=Width() || xSrc>=dibSrc.Width())
    {
        return FALSE;
    }

    if(y>=nDstTrueHeight || ySrc>=nSrcTrueHeight)
    {
        return FALSE;
    }
    
    if(nWidth > Width()-x)
    {
        nWidth = Width()-x;
    }

    if(nSrcWidth < 0 || nSrcWidth > dibSrc.Width()-xSrc)
    {
        nSrcWidth = dibSrc.Width()-xSrc;
    }

    if(nHeight > nDstTrueHeight-y)
    {
        nHeight = nDstTrueHeight-y;
    }

    if(nSrcHeight < 0 || nSrcHeight > nSrcTrueHeight-ySrc)
    {
        nSrcHeight = nSrcTrueHeight-ySrc;
    }
    
    nSrcPitch = WIDTHBYTES(dibSrc.BitCount()*dibSrc.Width());
    nDstPitch = WIDTHBYTES(BitCount()*Width());
    if(Height() > 0)
    {
        pDst = Bits()+(nDstTrueHeight-y-1)*nDstPitch;
        nDstPitch = -nDstPitch;
    }
    else
    {
        pDst = Bits()+(y*nDstPitch);
    }

    if(dibSrc.Height() > 0)
    {
        pSrc = dibSrc.Bits()+(nSrcTrueHeight-ySrc-1)*nSrcPitch;
        nSrcPitch = -nSrcPitch;
    }
    else
    {
        pSrc = dibSrc.Bits()+(ySrc*nSrcPitch);
    }

    switch(BitCount()){
    case 1:
        switch(dibSrc.BitCount()){
        case 1:
            return ::DIB1_StretchBlt1(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight);
        case 8:
            return ::DIB1_StretchBlt8(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight);
        case 24:
            return ::DIB1_StretchBlt24(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight);
        case 32:
            return ::DIB1_StretchBlt32(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight);
        default:
            return FALSE;
        }
        break;
    case 8:
        switch(dibSrc.BitCount()){
        case 1:
            return ::DIB8_StretchBlt1(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight);
        case 8:
            return ::DIB8_StretchBlt8(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight);
        case 24:
            return ::DIB8_StretchBlt24(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight);
        case 32:
            return ::DIB8_StretchBlt32(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight);
        default:
            return FALSE;
        }
        break;
    case 24:
        switch(dibSrc.BitCount()){
        case 1:
            return ::DIB24_StretchBlt1(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight, dibSrc.PaletteBits());
        case 8:
            return ::DIB24_StretchBlt8(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight, dibSrc.PaletteBits());
        case 24:
            return ::DIB24_StretchBlt24(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight);
        case 32:
            return ::DIB24_StretchBlt24(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight);
        default:
            return FALSE;
        }
        break;
    case 32:
        switch(dibSrc.BitCount()){
        case 1:
            return ::DIB32_StretchBlt1(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight, dibSrc.PaletteBits());
        case 8:
            return ::DIB32_StretchBlt8(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight, dibSrc.PaletteBits());
        case 24:
            return ::DIB32_StretchBlt24(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight);
        case 32:
            return ::DIB32_StretchBlt24(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, nSrcWidth, nSrcHeight);
        default:
            return FALSE;
        }
        break;
    default:
        return FALSE;
    }
    return TRUE;
}

static BOOL DIB1_BitBlt1(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Mask1 = 0x80>>(xd%8);
            int Mask2 = 0x80>>(xs%8);
            BYTE Curr1 = pDst[xd>>3]&(~Mask1);
            BYTE Curr2 = pSrc[xs>>3]&Mask2;
            Curr1 |= (Curr2<<(7-(xs%8)))>>(xd%8);
            pDst[xd>>3] = Curr1;
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB1_BitBlt8(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Mask1 = 0x80>>(xd%8);
            BYTE Curr1 = pDst[xd>>3]&(~Mask1);
            BYTE Curr2 = pSrc[xs];
            Curr1 |= (Curr2&0x80)>>(xd%8);
            pDst[xd>>3] = Curr1;
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB1_BitBlt24(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Off = xs*3;
            int Mask1 = 0x80>>(xd%8);
            BYTE Curr1 = pDst[xd>>3]&(~Mask1);
            BYTE Curr2 = ~RGB2GRAY(pSrc[Off],pSrc[Off+1],pSrc[Off+2]);
            Curr1 |= (Curr2&0x80)>>(xd%8);
            pDst[xd>>3] = Curr1;
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB1_BitBlt32(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Off = xs*4;
            int Mask1 = 0x80>>(xd%8);
            BYTE Curr1 = pDst[xd>>3]&(~Mask1);
            BYTE Curr2 = ~RGB2GRAY(pSrc[Off],pSrc[Off+1],pSrc[Off+2]);
            Curr1 |= (Curr2&0x80)>>(xd%8);
            pDst[xd>>3] = Curr1;
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB8_BitBlt1(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Mask2 = 0x80>>(xs%8);
            BYTE Curr2 = pSrc[xs>>3]&Mask2;
            pDst[xd] = Curr2?0xFF:0;
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB8_BitBlt8(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            pDst[xd] = pSrc[xs];
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB8_BitBlt24(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Off = xs*3;
            pDst[xd] = ~RGB2GRAY(pSrc[Off],pSrc[Off+1],pSrc[Off+2]);
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB8_BitBlt32(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Off = xs*4;
            pDst[xd] = ~RGB2GRAY(pSrc[Off],pSrc[Off+1],pSrc[Off+2]);
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB24_BitBlt1(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, PPALETTEENTRY pPal)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Off = xd*3;
            int Mask2 = 0x80>>(xs%8);
            BYTE Curr2 = pSrc[xs>>3]&Mask2;
            if(Curr2)
            {
                pDst[Off]   = pPal[1].peRed;
                pDst[Off+1] = pPal[1].peGreen;
                pDst[Off+2] = pPal[1].peBlue;
            }
            else
            {
                pDst[Off]   = pPal[0].peRed;
                pDst[Off+1] = pPal[0].peGreen;
                pDst[Off+2] = pPal[0].peBlue;
            }
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB24_BitBlt8(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, PPALETTEENTRY pPal)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Off = xd*3;
            BYTE Curr2 = pSrc[xs];
            pDst[Off]   = pPal[Curr2].peRed;
            pDst[Off+1] = pPal[Curr2].peGreen;
            pDst[Off+2] = pPal[Curr2].peBlue;
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB24_BitBlt24(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Off1 = xd*3;
            int Off2 = xs*3;
            pDst[Off1]   = pSrc[Off2];
            pDst[Off1+1] = pSrc[Off2+1];
            pDst[Off1+2] = pSrc[Off2+2];
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB24_BitBlt32(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Off1 = xd*3;
            int Off2 = xs*4;
            pDst[Off1]   = pSrc[Off2];
            pDst[Off1+1] = pSrc[Off2+1];
            pDst[Off1+2] = pSrc[Off2+2];
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB32_BitBlt1(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, PPALETTEENTRY pPal)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Off = xd*4;
            int Mask2 = 0x80>>(xs%8);
            BYTE Curr2 = pSrc[xs>>3]&Mask2;
            if(Curr2)
            {
                pDst[Off]   = pPal[1].peRed;
                pDst[Off+1] = pPal[1].peGreen;
                pDst[Off+2] = pPal[1].peBlue;
                pDst[Off+3] = pPal[1].peFlags;
            }
            else
            {
                pDst[Off]   = pPal[0].peRed;
                pDst[Off+1] = pPal[0].peGreen;
                pDst[Off+2] = pPal[0].peBlue;
                pDst[Off+3] = pPal[0].peFlags;
            }
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB32_BitBlt8(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight, PPALETTEENTRY pPal)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Off = xd*4;
            BYTE Curr2 = pSrc[xs];
            pDst[Off]   = pPal[Curr2].peRed;
            pDst[Off+1] = pPal[Curr2].peGreen;
            pDst[Off+2] = pPal[Curr2].peBlue;
            pDst[Off+3] = pPal[Curr2].peFlags;
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB32_BitBlt24(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Off1 = xd*4;
            int Off2 = xs*3;
            pDst[Off1]   = pSrc[Off2];
            pDst[Off1+1] = pSrc[Off2+1];
            pDst[Off1+2] = pSrc[Off2+2];
            pDst[Off1+3] = 0;
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

static BOOL DIB32_BitBlt32(LPSTR pDst, int xDst, int nDstPitch, LPSTR pSrc, int xSrc, int nSrcPitch, int nWidth, int nHeight)
{
    int x,y;
    int xd;
    int xs;

    for(y=0; y<nHeight; y++)
    {
        xd = xDst;
        xs = xSrc;
        for(x=0; x<nWidth; x++,xd++,xs++)
        {
            int Off1 = xd*4;
            int Off2 = xs*4;
            pDst[Off1]   = pSrc[Off2];
            pDst[Off1+1] = pSrc[Off2+1];
            pDst[Off1+2] = pSrc[Off2+2];
            pDst[Off1+3] = pSrc[Off2+3];
        }
        pDst += nDstPitch;
        pSrc += nSrcPitch;
    }
    return TRUE;
}

BOOL CDib::BitBlt(int x, int y, int nWidth, int nHeight, CDib &dibSrc, int xSrc, int ySrc)
{
    LPSTR pDst;
    int nDstPitch;
    LPSTR pSrc;
    int nSrcPitch;
    int nSrcTrueHeight = dibSrc.Height() > 0 ? dibSrc.Height(): -dibSrc.Height();
    int nDstTrueHeight = Height() > 0 ? Height(): -Height();

    if(x>=Width() || xSrc>=dibSrc.Width())
    {
        return FALSE;
    }

    if(y>=nDstTrueHeight || ySrc>=nSrcTrueHeight)
    {
        return FALSE;
    }
    
    if(nWidth > Width()-x)
    {
        nWidth = Width()-x;
    }

    if(nWidth > dibSrc.Width()-xSrc)
    {
        nWidth = dibSrc.Width()-xSrc;
    }

    if(nHeight > nDstTrueHeight-y)
    {
        nHeight = nDstTrueHeight-y;
    }

    if(nHeight > nSrcTrueHeight-ySrc)
    {
        nHeight = nSrcTrueHeight-ySrc;
    }
    
    nSrcPitch = WIDTHBYTES(dibSrc.BitCount()*dibSrc.Width());
    nDstPitch = WIDTHBYTES(BitCount()*Width());
    if(Height() > 0)
    {
        pDst = Bits()+(nDstTrueHeight-y-1)*nDstPitch;
        nDstPitch = -nDstPitch;
    }
    else
    {
        pDst = Bits()+(y*nDstPitch);
    }

    if(dibSrc.Height() > 0)
    {
        pSrc = dibSrc.Bits()+(nSrcTrueHeight-ySrc-1)*nSrcPitch;
        nSrcPitch = -nSrcPitch;
    }
    else
    {
        pSrc = dibSrc.Bits()+(ySrc*nSrcPitch);
    }

    switch(BitCount()){
    case 1:
        switch(dibSrc.BitCount()){
        case 1:
            return ::DIB1_BitBlt1(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight);
        case 8:
            return ::DIB1_BitBlt8(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight);
        case 24:
            return ::DIB1_BitBlt24(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight);
        case 32:
            return ::DIB1_BitBlt32(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight);
        default:
            return FALSE;
        }
        break;
    case 8:
        switch(dibSrc.BitCount()){
        case 1:
            return ::DIB8_BitBlt1(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight);
        case 8:
            return ::DIB8_BitBlt8(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight);
        case 24:
            return ::DIB8_BitBlt24(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight);
        case 32:
            return ::DIB8_BitBlt32(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight);
        default:
            return FALSE;
        }
        break;
    case 24:
        switch(dibSrc.BitCount()){
        case 1:
            return ::DIB24_BitBlt1(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, dibSrc.PaletteBits());
        case 8:
            return ::DIB24_BitBlt8(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, dibSrc.PaletteBits());
        case 24:
            return ::DIB24_BitBlt24(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight);
        case 32:
            return ::DIB24_BitBlt32(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight);
        default:
            return FALSE;
        }
        break;
    case 32:
        switch(dibSrc.BitCount()){
        case 1:
            return ::DIB32_BitBlt1(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, dibSrc.PaletteBits());
        case 8:
            return ::DIB32_BitBlt8(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight, dibSrc.PaletteBits());
        case 24:
            return ::DIB32_BitBlt24(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight);
        case 32:
            return ::DIB32_BitBlt32(pDst, x, nDstPitch, pSrc, xSrc, nSrcPitch, nWidth, nHeight);
        default:
            return FALSE;
        }
        break;
    default:
        return FALSE;
    }
    return TRUE;
}