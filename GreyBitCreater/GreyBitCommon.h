#pragma once

#define MAX_CHAR_CODE		0xFFFF
#define MAX_SLIDER_RANGE	256

#define CODE_ST_IDX			0x0F
#define CODE_ST_BMP			0x10
#define CODE_ST_ON   		0x80

#define WM_MY_UPDATEINFO	(WM_USER+101) 
