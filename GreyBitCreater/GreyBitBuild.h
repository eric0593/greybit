#pragma once
#include "GreyBitCommon.h"
#include "FreeTypeFontEngine.h"
#include "GreyBitFontEngine.h"

class GreyBitBuild
{
public:
	GreyBitBuild(void);
	~GreyBitBuild(void);

public:
	void Start(void);
	void Stop(void);
	void SetBmpPath(CString &szPath)				{m_BmpPath = szPath;};
	void SetOutputName(CString &szPath)				{m_OutputName = szPath;};
	void SetSize(int nSize)							{m_nSize = nSize;};
    void SetScale(int nScale)						{m_nScale = nScale;};
	void SetCodeTable(byte *pTable, int nTotal)		{memcpy(m_wAllCode, pTable, sizeof(m_wAllCode)); m_nTotalNum = nTotal;};
    void SetCodeHoriOff(int *pHoriOff)		        {memcpy(m_nCodeHoriOff, pHoriOff, sizeof(m_nCodeHoriOff));};
	void SetInputFile(CString &szPath, int nIdx)	{m_InputFile[nIdx] = szPath; m_bInputSet[nIdx] = true;};
	void SetCompress(bool bCompress)				{m_bCompress = bCompress;};
	void SetGrey(bool bGrey)						{m_bGrey = bGrey;};
    void SetVector(bool bVector)					{m_bVector = bVector;};
	void SetCWnd(CWnd *pCWnd)						{m_CurrWnd = pCWnd;};
	int  GetBuildNum(void)							{return m_nCurrNum;};
	bool IsBuilding(void)							{return m_bBuilding;};
	bool CheckInputFileAvaliable(int *pIdx);

public:
	int GetCodeInputIdx(TCHAR nCode)				{return (m_wAllCode[nCode]&CODE_ST_IDX);};
	CString &GetInputFileByIdx(int nIdx)			{return m_InputFile[nIdx];};
	CString &GetInputFileByCode(TCHAR nCode)		{return GetInputFileByIdx(GetCodeInputIdx(nCode));};
	void ResetInput(void);
	void UpdateInfo(void);

public:
	int				m_nSize;
    int             m_nScale;
	byte			m_wAllCode[MAX_CHAR_CODE+1];
    int 			m_nCodeHoriOff[MAX_CHAR_CODE+1];
	int				m_nTotalNum;
	int				m_nCurrNum;
    bool            m_bVector;
	bool			m_bCompress;
	bool			m_bGrey;
	bool			m_bBuilding;
	CString			m_BmpPath;
	CString			m_OutputName;
	CString			m_InputFile[CODE_ST_IDX+1];
	bool			m_bInputSet[CODE_ST_IDX+1];
	CWinThread	   *m_pThread;
	CWnd           *m_CurrWnd;
    byte           *m_pBuf;
};
