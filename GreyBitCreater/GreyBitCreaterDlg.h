// GreyBitCreaterDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "GreyBitCommon.h"
#include "GreyBitBuild.h"
#include "GreyBitViewer.h"
#include "UnicodeSection.h"
#include "CListCtrlEx.h"

// CGreyBitCreaterDlg 对话框
class CGreyBitCreaterDlg : public CDialog
{
// 构造
public:
	CGreyBitCreaterDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_GREYBITCREATER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
private:
	int				m_nCodeStart;
	int				m_nPreviewCode;
	int				m_nPercent;
	byte			m_wAllCode[MAX_CHAR_CODE+1];
    int  			m_nCodeHoriOff[MAX_CHAR_CODE+1];
	int				m_nTotalNum;
	int				m_nCurrNum;
    int             m_InputComboSel;
	CString			m_BmpPath;
	CString			m_OutputName;
    GreyBitViewer  *m_pViewer[CODE_ST_IDX+1];
    int             m_nCurrViewer;
    GB_UINT16       m_CodeTable[CODE_ST_IDX+1][UNICODE_SECTION_NUM];
    byte            m_FileIndex[UNICODE_SECTION_NUM];
	CString			m_InputFile[CODE_ST_IDX+1];
    int             m_InputFileNum;
	GreyBitBuild	m_GreyBitBuild;
    GreyBitFactory  m_GreyBitFactory;
    CStringArray    m_ArraySrc;

public:
	// Init
	void InitInput(void);
	void InitCharset(void);
	void InitOutput(void);
	void InitPreview(void);
	void InitBuild(void);
	void InitCharsetList(void);
	void InitSizeCombox(void);
	
	// Get Parameter
	int GetFontSize(void);
    int GetFontScale(void);
    int GetIndexByName(CString &szName);
	CString &GetOutputName(void);
    void GetFileName(CString &szPath, CString &szName);

	// Update
	void UpdatePercent(int nPercent);
	void UpdateProgressNum(int nCurr, int nTotal);
	void UpdateBuildInfo(void);
	void UpdatePreviewCode(void);
	void UpdateCodeFontInfo(int nWidth, int nHeight, int nHoriOff);
	void UpdateCodeBitmap(int nCode, int nIndex);
	void UpdateCharsetList(void);
    void UpdateFontViewer(void);

	// Parameter Check
	bool IsSizeAvaliable(void);
	bool IsCodeAvaliable(void);
	bool IsFromBmpCode(TCHAR nCode)		{return ((m_wAllCode[nCode]&CODE_ST_BMP)!=0);};
	bool IsFromBuildCode(TCHAR nCode)	{return (m_wAllCode[nCode]!=0);};
	bool IsCodeAvaliableInSrc(int nCode, int nIndex = -1, int *pIdxOut = NULL);
	bool IsBuilding(void)				{return m_GreyBitBuild.IsBuilding();};
	
	// Input Parse
	int ParseAllCode(void);
	int ParseCharsetList(void);
	int ParseBmpCode(void);
	int ParseTxtCode(void);

	// Build
	void StartBuild(void);
	void StopBuild(void);

protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	// 字符大小控件
	CComboBox m_SizeComboBox;
	// 字符编码列表列表控件
	CListCtrlEx m_CodeListCtrl;
	// 预览区域
	CStatic m_PreviewStatic;
	// 字符宽度
	CStatic m_WidthStatic;
	// 字符高度
	CStatic m_HeightStatic;
    // 水平偏移
    CStatic m_HOffStatic;
	// 当前CODE
	CEdit m_CodeEdit;
	// Code滑动条
	CSliderCtrl m_CodeSlider;
	// 进度条
	CProgressCtrl m_ProgressBar;
	// 百分比
	CStatic m_PercentStatic;
	// 全部进度指示
	CStatic m_TotalStatic;
	// 使用BMP文件
	CButton m_BmpCheck;
	// 使用TXT文件
	CButton m_TxtCheck;
	// 全选
	CButton m_SelectAllCheck;
	// 灰度类型
	CButton m_GreyRadio;
	// 压缩选项
	CButton m_CompressRadio;
	// 输入文件
	CComboBox m_InputFileCombo;
	// Bmp Path
	CComboBox m_InputBmpCombo;
	// Txt file
	CComboBox m_InputTxtCombo;
	// 输出路径
	CComboBox m_OutputPathCombo;
	// Build Button
	CButton m_BuildButton;
    // 输出格式
    CButton m_VectorRadio;
    // 显示矢量坐标
    CButton m_OutlineCheck;


    afx_msg LRESULT OnMyUpdateInfo(WPARAM wParam, LPARAM lParam); 
    afx_msg LRESULT OnMyShowCtrl(WPARAM wParam, LPARAM lParam); 
    afx_msg LRESULT OnItemChanged(WPARAM wParam, LPARAM lParam); 
	afx_msg void OnBnClickedButtonInputOpen();
	afx_msg void OnBnClickedButtonInputTxt();
	afx_msg void OnBnClickedButtonInputBmp();
	afx_msg void OnBnClickedButtonOutputOpen();
	afx_msg void OnBnClickedBuild();
	afx_msg void OnBnClickedCheckCharsetAll();
	afx_msg void OnCbnEditchangeComboSize();
	afx_msg void OnEnChangeEditCode();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnBnClickedButtonGoto();
    afx_msg void OnCbnCloseupComboInput();
    afx_msg void OnCbnDropdownComboInput();
    afx_msg void OnBnClickedRadioVect();
    afx_msg void OnBnClickedCheckOutline();
    afx_msg void OnBnClickedRadioBitmap();
//    afx_msg void OnLvnItemchangedListCharset(NMHDR *pNMHDR, LRESULT *pResult);
    afx_msg void OnBnClickedRadioGrey();
    afx_msg void OnBnClickedRadioMono();
    afx_msg void OnBnClickedButtonSave();
    CEdit m_ScaleEdit;
    afx_msg void OnBnClickedButtonGcf();
};
