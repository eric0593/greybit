#if !defined MYDIB
#define MYDIB

#include "dibapi.h"

#define RGB2GRAY(r,g,b) (((b)*117 + (g)*601 + (r)*306) >> 10)

class CDib
{
public:
    CDib();
    ~CDib();

protected:
    HDIB m_hDIB;
    CPalette* m_palDIB;
    BITMAPFILEHEADER m_bmfHeader;
    
public:
    BOOL Create(int nWidth, int nHeight, int nBitCount, LPSTR pBits=NULL);
    BOOL Load(LPCTSTR lpszFileName);
    BOOL Load(UINT nResID);
    BOOL Save(LPCTSTR lpszFileName, int nWidth, int nHeight, int nPitch, int nBitCount, LPSTR pBits);
    CPalette* GetPalette() const { return m_palDIB; }
    BOOL Draw(CDC *pDC, int x, int y, int cx=0, int cy=0);
    int Width();
    int Height();
    int BitCount();
    int Pitch();
    LPSTR Bits();
    PPALETTEENTRY PaletteBits();
    void DeleteDIB();

    // Blt
    BOOL StretchBlt(int x, int y, int nWidth, int nHeight, CDib &dibSrc, int xSrc = 0, int ySrc = 0, int nSrcWidth = -1, int nSrcHeight = -1);
    BOOL BitBlt(int x, int y, int nWidth, int nHeight, CDib &dibSrc, int xSrc = 0, int ySrc = 0);
};
#endif

